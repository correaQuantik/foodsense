<?php



ob_start("ob_gzhandler");



// Inclui os defines
require_once(dirname(__FILE__)."/app/_main/defines.php");

// Inclui a classe Route
require_once(FOLDER_MAIN."route.php");
// Inclui a classe Control
require_once(FOLDER_MAIN."control.php");
// Inclui a classe SQL
require_once(FOLDER_CLASS."sql.php");



// Prepara o caminho que o usuário está acessando
$path = isset($_GET["path"]) ? explode("/", $_GET["path"]) : array();



// Inicia o tratamento das rotas
$_r = new Route($path);



/*
	Adiciona rotas no seguintes valores
	URL, path do control, nome da classe

	ATENÇÃO: A primeira rota adicionada é usada como a padrão no redirect para 404!
*/
$_r->Add("Home", "front/home", "Home");
$_r->Add("Sobre", "front/sobre", "Sobre");
$_r->Add("Produtos", "front/produtos", "Produtos");
$_r->Add("Montar", "front/montar", "Montar");
$_r->Add("QualSeuEstilo", "front/qualSeuEstilo", "QualSeuEstilo");
$_r->Add("Diferenciais", "front/diferenciais", "Diferenciais");
$_r->Add("Parceiros", "front/parceiros", "Parceiros");
$_r->Add("Blog/[^/]+/[0-9]+", "front/blog/listar", "BlogListar");
$_r->Add("Blog/[^/]+/[0-9]+/[0-9]+", "front/blog/ver", "BlogVer");
$_r->Add("Contato", "front/contato", "Contato");

$_r->Add("Download/[0-9]+", "front/download", "Download");

// Painel
$_r->Add("Painel", "painel/home", "PainelHome");
$_r->Add("Painel/Login", "painel/login", "PainelLogin");
$_r->Add("Painel/Sair", "painel/sair", "PainelSair");
$_r->Add("Painel/Home", "painel/home", "PainelHome");
$_r->Add("Painel/Termos", "painel/termos", "PainelTermos");
$_r->Add("Painel/Perfil", "painel/perfil", "PainelPerfil");
// Usuarios
$_r->Add("Painel/Usuarios", "painel/usuarios/listar", "PainelUsuariosListar");
$_r->Add("Painel/Usuarios/Novo", "painel/usuarios/novo", "PainelUsuariosNovo");
$_r->Add("Painel/Usuarios/[0-9]+", "painel/usuarios/ver", "PainelUsuariosVer");
$_r->Add("Painel/Usuarios/[0-9]+/Editar", "painel/usuarios/editar", "PainelUsuariosEditar");
$_r->Add("Painel/Usuarios/[0-9]+/Apagar", "painel/usuarios/apagar", "PainelUsuariosApagar");
// Postagem
$_r->Add("Painel/Postagem/Nova", "painel/postagem/nova", "PainelPostagemNova");
$_r->Add("Painel/Postagem/Listar", "painel/postagem/listar", "PainelPostagemListar");
$_r->Add("Painel/Postagem/[0-9]+", "painel/postagem/ver", "PainelPostagemVer");
$_r->Add("Painel/Postagem/[0-9]+/Comentarios", "painel/postagem/comentarios", "PainelPostagemComentarios");
$_r->Add("Painel/Postagem/Categoria/Nova", "painel/postagem/categoria/nova", "PainelPostagemCategoriaNova");
$_r->Add("Painel/Postagem/Categoria/Listar", "painel/postagem/categoria/listar", "PainelPostagemCategoriaListar");
$_r->Add("Painel/Postagem/Categoria/[0-9]+", "painel/postagem/categoria/ver", "PainelPostagemCategoriaVer");
$_r->Add("Painel/Postagem/TAGs/Listar", "painel/postagem/tags/listar", "PainelPostagemTAGsListar");



// Ajax
$_r->Add("Ajax/Login", "ajax/login", "AjaxLogin");
$_r->Add("Ajax/Termos", "ajax/termos", "AjaxTermos");
$_r->Add("Ajax/Perfil", "ajax/perfil", "AjaxPerfil");
// Facebook
$_r->Add("Ajax/Front/Postagem", "ajax/front/postagem", "AjaxFrontPostagem");
// Usuarios
$_r->Add("Ajax/Usuario/Novo", "ajax/usuario/novo", "AjaxUsuarioNovo");
$_r->Add("Ajax/Usuario/Novo/[0-9]+", "ajax/usuario/novo", "AjaxUsuarioNovo");
$_r->Add("Ajax/Usuario/Editar/[0-9]+", "ajax/usuario/editar", "AjaxUsuarioEditar");
$_r->Add("Ajax/Usuario/Apagar/[0-9]+", "ajax/usuario/apagar", "AjaxUsuarioApagar");
// Postagem
$_r->Add("Ajax/Postagem/Nova", "ajax/postagem/nova", "AjaxPostagemNova");
$_r->Add("Ajax/Postagem/Comentario", "ajax/postagem/comentario", "AjaxPostagemComentario");
$_r->Add("Ajax/Postagem/Nova/[0-9]+", "ajax/postagem/nova", "AjaxPostagemNova");
$_r->Add("Ajax/Postagem/Editar/[0-9]+", "ajax/postagem/editar", "AjaxPostagemEditar");
$_r->Add("Ajax/Postagem/Apagar/[0-9]+", "ajax/postagem/apagar", "AjaxPostagemApagar");
$_r->Add("Ajax/Postagem/Categoria/Nova", "ajax/postagem/categoria/nova", "AjaxPostagemCategoriaNova");
$_r->Add("Ajax/Postagem/Categoria/Editar/[0-9]+", "ajax/postagem/categoria/editar", "AjaxPostagemCategoriaEditar");
$_r->Add("Ajax/Postagem/Categoria/Apagar/[0-9]+", "ajax/postagem/categoria/apagar", "AjaxPostagemCategoriaApagar");
$_r->Add("Ajax/Postagem/TAG/Apagar/[0-9]+", "ajax/postagem/tag/apagar", "AjaxPostagemTAGApagar");
// Datatable
$_r->Add("Ajax/Datatable/Usuarios", "ajax/datatable/usuarios", "AjaxDatatableUsuarios");
$_r->Add("Ajax/Datatable/Postagem", "ajax/datatable/postagem/postagens", "AjaxDatatablePostagem");
$_r->Add("Ajax/Datatable/Postagem/TAGs", "ajax/datatable/postagem/tags", "AjaxDatatablePostagemTAGs");
$_r->Add("Ajax/Datatable/Postagem/Categorias", "ajax/datatable/postagem/categorias", "AjaxDatatablePostagemCategorias");



// Conta administradora
// $_r->Add("Painel/Admin", "admin", "Admin");



// Style e JS
$_r->Add("(css|js)/(geral|home|login|painel)/[A-Za-z0-9.-]+", "extra/control", "Extra");



// Inicia as rotas
$_r->Init();



?>