<?php

require_once(dirname(__FILE__)."/app/_main/defines.php");
require_once(FOLDER_CLASS."Facebook/autoload.php");
require_once(FOLDER_CLASS."facebook.php");
require_once(FOLDER_CLASS."sql.php");

// Inicia a sessão
session_start();

$sql = new SQL();
$facebook = new Facebook($sql);

$ans = $facebook->RedirectFacebook();

if(is_array($ans))
{
	if($ans[0] == "success")
	{
		$user = $facebook->GetUserData(false);

		if(!!$user)
		{
			$exist = $sql->count("facebook", array(array("email", $user->email)));

			if(!!!$exist)
			{
				$insert = $sql->insert("facebook", array(array("idfacebook", $user->id), array("chave", $ans[1]), array("nome", $user->name), array("email", $user->email), array("criado", $sql->date())));
			}
			else
			{
				$update = $sql->update("facebook", array(array("chave", $ans[1]), array("nome", $user->name), array("email", $user->email)), array(array("email", $user->email)));
			}

			if(isset($_SESSION["url"]))
			{
				header("Location: ".$_SESSION["url"]);
				exit();
			}
			else
			{
				header("Location: ".URL);
				exit();
			}
		}
		else
		{
			session_destroy();
		}
	}
}

header("Location: ".URL);
exit();

?>