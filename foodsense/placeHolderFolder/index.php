<?php

	require_once "main.class.php";
	require_once "contentAdder.php";

?>

<!DOCTYPE html>
<html>
<head>
	<title>FoodSense - Produtos</title>

	<!--Configuracao-->
	<meta charset="utf-8">

	<!--Estilos-->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<!--Fontes-->
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
	<link href="MainStyle.css" rel="stylesheet"> 
</head>
<body>

	<!--Conteudo-->
	<section class="col-md-offset-2 col-md-8 ProductContainer">
		<form method="POST">
			<div class="productHeader col-md-12">
				<div class="col-md-6">
					<select id="comboType">
						<?php
							foreach ($combos as $key => $value) {
								echo "<option value='".$key."'>Smart ".$value->name."</option>";
							}
						?>
					</select>
				</div>
				<div class="col-md-6"><img src="prato.jpg" class="FoodPhoto"></div>
			</div>
			<div class="col-md-12 Lined">
				<div class="col-md-4 VerticalList">
					<div>
						Opcional:
					</div>
					<div>
						<?php
							foreach ($combos as $key => $value) {
								foreach ($value->dishContents as $subKey => $subValue) {

									echo "<div class='item col-md-5 hiddenItem ".$key." content'>";
									echo "<input type='checkbox' name='".$subKey."' class='guarni'>";
									if ($subValue->mediumSizeCal == $subValue->bigSizeCal) {
										echo "<span>".$subValue->name." - ".$subValue->mediumSizeCal."kcal</span>";
									}
									else {
										echo "<span>".$subValue->name." - ".$subValue->mediumSizeCal."kcal/".$subValue->bigSizeCal."kcal</span>";
									}

									if (!is_null($subValue->descrip)) {
										echo "<br>";
										echo "<span class='itemDescrip'>(".$subValue->descrip.")</span>";
									}

									echo "</div>";
								}
							}
						?>
					</div>
				</div>
				<div class="col-md-8 VerticalList">
					<div class="col-md-12 Lined">
						<div>
							Escolha +3 igredientes para a salada do seu combo
						</div>
						<div>
							<?php
								foreach ($igredientes as $key => $value) {
									echo "<div class='item col-md-5'><input type='checkbox' name='".$key."' class='igrediente'><span>".$value->name." - ".$value->cal."kcal </span></div>";
								}
							?>
						</div>
					</div>
					<div class="col-md-12 Lined">
						<div>
							Escolha 1 molho para a sua salada
						</div>
						<div>
							<?php
								foreach ($molhos as $key => $value) {
									echo "<div class='item col-md-5'><input type='checkbox' name='".$key."' class='molho'><span>Molho ".$value->name." - ".$value->cal."kcal</span></div>";
								}
							?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="item col-md-5"><input type="radio" value="mSize" name="dishSize"><span>Tamanho Médio - R$13,99</span></div>
				<div class="item col-md-5"><input type="radio" value="bSize" name="dishSize"><span>Tamanho Grande - R$15,99</span></div>
			</div>
			<div class="col-md-12">
				Total de calorias com salada e molho: <span class="Calories"></span>
			</div>
			<div class="col-md-12">
				<div class="item col-md-5"><input type="checkbox" name=""><span>Desejo colocar esse combo no carrinho de compra</span></div>
				<input type="submit">
			</div>
		</form>
	</section>


	<!--scripts-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script type="text/javascript">
		<?php
			echo "//Todos os molhos

			";

			foreach ($molhos as $key => $value) {
				echo "var ".$key." = ".$value->cal.";";
			}

			echo "
			//Todos os igredientes

			";

			foreach ($igredientes as $key => $value) {
				echo "var ".$key." = ".$value->cal.";";
			}

			echo "
			//Todos os igredientes

			";

			foreach ($combos as $key => $value) {
				foreach ($value->dishContents as $subKey => $subValue) {
					echo "var ".$subKey."M = ".$subValue->mediumSizeCal.";";
					echo "var ".$subKey."B = ".$subValue->bigSizeCal.";";
				}
			}
		?>
	</script>
	<script src="mainJs.js"></script>
</body>
</html>