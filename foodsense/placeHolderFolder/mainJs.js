$(document).ready(function () {

	//Zerar todos os checkbox
	$("input[type=checkbox]").attr("checked",false);

	//Quando atualizar o select, atualizar as opções
	$('#comboType').change(function() {

		UpdateGuarnSelect();

	});
	UpdateGuarnSelect();

	//Quando o usuario interagir com a checkbox de molho
	$(".molho").change(function() {
		UpdateMolho();
	});

	//Quando o usuario interagir com a checkbox de igrediente
	$(".igrediente").change(function() {
		UpdateIgrediente();
	});

	//Atualizar os valores de caloria
	$(".igrediente,.molho,.guarni,input:radio").change(function() {
		UpdateCal();
	});

	$("input:radio[value='mSize']").attr("checked",true);
});

function UpdateGuarnSelect () {
	var $contents = $(".content");

	for (var count = 0; count < $contents.length; count++) {
		if (!$($contents[count]).hasClass($("#comboType").val())) {
			$($contents[count]).fadeOut("fast",function() { $($contents[count]).addClass("hiddenItem"); });
		} else {
			$($contents[count]).removeClass("hiddenItem");
			$($contents[count]).fadeIn();
		}
	}

	$(".guarni").prop("checked",false);

	UpdateCal();
}

function UpdateMolho () {
	if ($(".molho:checkbox:checked").length > 1) {
		alert("Por favor, escolha apenas um molho");
	}
}

function UpdateIgrediente () {
	if ($(".igrediente:checkbox:checked").length > 3) {
		alert("Por favor, escolha no máximo 3 igredientes");
	}
}

var TotalCaloria = 0;

function UpdateCal () {
	TotalCaloria = 0;

	var $caloriesGroup = $(".molho:checkbox:checked,.igrediente:checkbox:checked");

	for (var count = 0; count < $caloriesGroup.length; count++) {
		TotalCaloria += window[$($caloriesGroup[count]).attr("name")];
	}

	$caloriesGroup = $(".guarni:checkbox:checked");

	if ($("input:radio:checked[value='mSize']").length > 0) {
		for (var count = 0; count < $caloriesGroup.length; count++) {
			TotalCaloria += window[$($caloriesGroup[count]).attr("name")+"M"];
		}
	}
	if ($("input:radio:checked[value='bSize']").length > 0) {
		for (var count = 0; count < $caloriesGroup.length; count++) {
			TotalCaloria += window[$($caloriesGroup[count]).attr("name")+"B"];
		}
	}

	$(".Calories").text(TotalCaloria + "kcal");
}