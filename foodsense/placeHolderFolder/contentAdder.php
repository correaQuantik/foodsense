<?php

	//Adiciona os molhos
	$molhos["mostarda"] = new Molho("Mostarda",20);
	$molhos["pesto"] = new Molho("Pesto",22);
	$molhos["guacamole"] = new Molho("Guacamole",15);
	$molhos["iogurte"] = new Molho("Iogurte",12);

	//Adiciona os igredientes
	$igredientes["agriao"] = new igredient("Agrião",2);
	$igredientes["brocoli"] = new igredient("Brócoli",8);
	$igredientes["mixAlfaces"] = new igredient("Mix de Alfaces",2);
	$igredientes["repolho"] = new igredient("Repolho",5);
	$igredientes["azeitonaPicada"] = new igredient("Azeitona Picada",9);
	$igredientes["beterrabaCozida"] = new igredient("Beterraba Cozida",6);
	$igredientes["cenouraCozida"] = new igredient("Cenoura Cozida",7);
	$igredientes["cenouraCrua"] = new igredient("Cenoura Crua",6);
	$igredientes["couveFlor"] = new igredient("Couve-flor",4);
	$igredientes["ervilhaFresca"] = new igredient("Ervilha Fresca",9);
	$igredientes["feijaoFradinho"] = new igredient("Feijão Fradinho",8);
	$igredientes["lentilha"] = new igredient("Lentilha",6);
	$igredientes["pepino"] = new igredient("Pepino",4);
	$igredientes["tomate"] = new igredient("Tomate",5);

	//Adiciona os combos
	$preGuarn["frangoFatiado"] = new DishContent("Filé de Frango fatiado",110);
	$preGuarn["croutonsIntegral"] = new DishContent("Croûtons Integral",10);
	$combos["salad"] = new Dish("Salad",$preGuarn);
	unset($preGuarn);

	$preGuarn["fileMignon"] = new DishContent("Filé Mignon", 50);
	$preGuarn["batataPalito"] = new DishContent("Batata Palito Assada", 20);
	$combos["kids"] = new Dish("Kids",$preGuarn);
	unset($preGuarn);

	$preGuarn["legumes"] = new DishContent("Smart Baby Legumes", 30,"Cenoura, pimentão, abobrinha e batata doce");
	$preGuarn["carne"] = new DishContent("Smart Baby Carne", 38,"Cenoura, pimentão, abobrinha e batata doce");
	$preGuarn["frango"] = new DishContent("Smart Baby Frango", 32,"Cenoura, pimentão, abobrinha e batata doce");
	$combos["baby"] = new Dish("Baby",$preGuarn);
	unset($preGuarn);

	$preGuarn["arroz"] = new DishContent("Arroz", 12, null, 18);
	$preGuarn["arrozIntegral"] = new DishContent("Arroz Integral", 12, null, 18);
	$preGuarn["arrozIntegralGrega"] = new DishContent("Arroz Integral à Grega", 15, "Cenoura, pimentão, abobrinha e batata doce", 19);
	$preGuarn["batataRustica"] = new DishContent("Batata Rústica", 18, "Cenoura, pimentão, abobrinha e batata doce", 22);
	$preGuarn["batataDoceRustica"] = new DishContent("Batata Doce Rústica", 18, "Cenoura, pimentão, abobrinha e batata doce", 22);
	$preGuarn["batataPalitoAssada"] = new DishContent("Batata Palito Assada", 18, null, 22);
	$preGuarn["berinjelaEmpanadaAssada"] = new DishContent("Berinjela Empanada Assada", 14, null, 17);
	$preGuarn["farofaGrao"] = new DishContent("Farofa de grão de Bico", 22, "Cenoura, pimentão, abobrinha e batata doce", 27);
	$preGuarn["feijao"] = new DishContent("Feijão", 18, null, 22);
	$preGuarn["legumesGrelhados"] = new DishContent("Legumes Grelhados", 9, "Cenoura, pimentão, abobrinha e batata doce", 12);
	$preGuarn["pureBatataDoce"] = new DishContent("Purê de Batata Doce", 18, null, 22);
	$preGuarn["pureBatata"] = new DishContent("Purê de Batata", 18, null, 22);
	$combos["sense"] = new Dish("Sense",$preGuarn);
	unset($preGuarn);

	$preGuarn["fileFrangoGrelhado"] = new DishContent("Filé de Frango Grelhado", 112, null, 153);
	$preGuarn["pureBatataDoce"] = new DishContent("Purê de Batata Doce", 112, null, 153);
	$preGuarn["ovoCozido"] = new DishContent("Ovo Cozido", 112, null, 153);
	$combos["training"] = new Dish("Training",$preGuarn);
	unset($preGuarn);

	$preGuarn["hamburguerVegano"] = new DishContent("Hamburguer Vegano", 112, null, 153);
	$preGuarn["arrozIntegral"] = new DishContent("Arroz Integral", 112, null, 153);
	$preGuarn["legumesGrelhados"] = new DishContent("Legumes Grelhados", 112, null, 153);
	$combos["veg"] = new Dish("Veg",$preGuarn);
	unset($preGuarn);
?>