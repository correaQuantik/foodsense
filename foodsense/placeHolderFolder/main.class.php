<?php

	class Dish {

		public $name;

		public $dishContents;

		function __Construct ($pName, $pContent) {
			$this->name = $pName;
			$this->dishContents = $pContent;
		}
	}

	class DishContent {

		public $name;
		public $descrip;

		public $mediumSizeCal;
		public $bigSizeCal;

		function __Construct ($pName, $pMediumCal, $pDescrip = null, $pBigCal = null) {
			$this->name = $pName;
			$this->descrip = $pDescrip;

			$this->mediumSizeCal = $pMediumCal;

			if (is_null($pBigCal)) {
				$this->bigSizeCal = $pMediumCal;
			} else {
				$this->bigSizeCal = $pBigCal;
			}
		}
	}

	class igredient {

		public $name;

		public $cal;

		function __Construct ($pName, $pCal) {
			$this->name = $pName;
			$this->cal = $pCal;
		}
	}

	class Molho {

		public $name;

		public $cal;

		function __Construct ($pName, $pCal) {
			$this->name = $pName;
			$this->cal = $pCal;
		}
	}

?>