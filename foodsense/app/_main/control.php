<?php

class Control
{
	protected $route;
	protected $sql;

	protected $view;
	protected $class;
	protected $replace = true;

	protected $header = "";
	protected $footer = "";

	protected $headers = Array();

	// Construtor da classe
	public function __construct($route, $view, $class)
	{
		$this->setRoute($route);
		$this->sql = new SQL();

		$this->setView(FOLDER_VIEW.$view.".php");
		$this->setClass($class);
	}

	// Ações a ser realizadas
	// Deve ser sobescrevida no Control da página
	public function doActions()
	{
	}

	// Carrega classes extras
	public function loadClass($class)
	{
		require_once(FOLDER_CLASS.$class.".php");
	}

	// Route
	public function setRoute($route)
	{
		$this->route = $route;
	}
	public function getRoute($id = null)
	{
		if(is_null($id))
		{
			return $this->route;
		}
		else
		{
			$path = $this->route->getPath($id);
			return isset($path) ? $path : null;
		}
	}

	// SQL
	public function getSQL()
	{
		return $this->sql;
	}

	// View
	public function setView($view)
	{
		$this->view = $view;
	}
	public function getView()
	{
		return $this->view;
	}

	// Class
	public function setClass($class)
	{
		$this->class = $class;
	}
	public function getClass()
	{
		return $this->class;
	}

	// Replace
	public function setReplace($replace)
	{
		$this->replace = $replace;
	}
	public function getReplace()
	{
		return $this->replace;
	}

	// Header
	public function setHeader($file)
	{
		$this->header = $this->getFile(FOLDER_INCLUDE.$file.".php");
	}
	public function getHeader()
	{
		return $this->header;
	}

	// Footer
	public function setFooter($file)
	{
		$this->footer = $this->getFile(FOLDER_INCLUDE.$file.".php");
	}
	public function getFooter()
	{
		return $this->footer;
	}

	// Retorna path do rout
	public function getPath($id = null)
	{
		return $this->route->getPath($id);
	}

	// Carrega um arquivo qualquer
	public function getFile($file)
	{
		$rtn = "";

		if(file_exists($file))
		{
			ob_start();
			require_once($file);
			$rtn = ob_get_contents();
			ob_end_clean();
		}

		if($this->getReplace())
			$rtn = preg_replace("/\n|\r|\t/", "", $rtn);

		return $rtn;
	}

	// Retorna um ID único de 13 caracteres
	public function getUID()
	{
		return strtoupper(uniqid());
	}

	public function getDate($format = null, $date = null)
	{
		$format = is_null($format) ? "Y-m-d H:i:s" : $format;
		$date = is_null($date) ? time() : $date;

		return date($format, $date);
	}

	// Header
	public function addPHPHeader($header)
	{
		$this->headers[] = $header;
	}
	public function getPHPHeaders()
	{
		for($i = 0; $i < count($this->headers); $i++)
		{
			header($this->headers[$i]);
		}
	}

	// Carrega a view
	public function getPathView()
	{
		$rtn = "";

		$this->getPHPHeaders();

		$rtn .= $this->getHeader();
		$rtn .= $this->getFile($this->view);
		$rtn .= $this->getFooter();

		return $rtn;
	}

	// Adiciona imagens ao relatório
	public function addImage($file, $path, $infos)
	{
		if($file["error"] == 4)
		{
			return array(false, "Ocorreu um erro ao adicionar a foto <b>".$file["name"]."</b>!");
		}
		else if(!is_array($infos))
		{
			return array(false, "Deve enviar uma array com as informações da imagem enviada!");
		}
		else
		{
			if($file["error"] != 0)
			{
				switch($file["error"])
				{
					case 1: return array(false, "O arquivo enviado excede o limite de tamanho de arquivos");
					case 2: return array(false, "O arquivo excede o limite do tamanho de um formulário");
					case 3: return array(false, "O upload do arquivo foi feito parcialmente");
					case 6: return array(false, "Pasta temporária ausênte");
					case 7: return array(false, "Falha em escrever o arquivo em disco");
					case 8: return array(false, "Uma extensão do PHP interrompeu o upload do arquivo. O PHP não fornece uma maneira de determinar qual extensão causou a interrupção. Examinar a lista das extensõs) carregadas com o phpinfo() pode ajudar");
					default: return array(false, "Erro desconhecido [".$file["error"]."]");
				}
			}
			else
			{
				$validaImagem = getimagesize($file["tmp_name"]);

				if(!!!$validaImagem)
				{
					return array(false, "O arquivo <b>".$file["name"]."</b> não é uma imagem!");
				}
				else
				{
					// Carrega a classe de upload
					$this->loadClass("img.class.upload");

					// Gera um ID único
					$fileName = strtoupper(uniqid());

					// Inicia a classe de Upload
					$upload = new Upload($file);

					// Verifica se arquivo foi enviado corretamente
					if(!!!$upload->uploaded)
					{
						return array(false, "A foto <b>".$file["name"]."</b> não foi enviada corretamente");
					}
					else
					{
						foreach($infos as $info)
						{
							$ratio = isset($info[3]) ? $info[3] : true;
							$path2 = $path.(isset($info[2]) ? $info[2] : "");
							$this->uploadImage($upload, $fileName, $ratio, $info[0], $info[1], $path2);
						}

						$fileName .= ".".$upload->file_dst_name_ext;

						if(!!!$upload->processed)
						{
							return array(false, "Falha ao processar a foto <b>".$file["name"]."</b>!");
						}
						else
						{
							$upload->Clean();

							return array(true, "Imagem <b>".$file["name"]."</b> enviada com sucesso!", $fileName);
						}
					}
				}
			}
		}
	}

	// Ajudante para o envio de imagens
	public function uploadImage($upload, $fileName, $ratio, $x, $y, $path)
	{
		$upload->file_new_name_body = $fileName;
		$upload->image_resize = true;
		$upload->image_ratio = $ratio;
		$upload->image_x = $x;
		$upload->image_y = $y;

		if($upload->Process($path))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

?>