<?php

class Route
{
	private $path;

	private $route;

	private $foundRoute = false;

	public function __construct($path)
	{
		$this->path = $path;
		$this->route = Array();
	}

	// Adiciona rotas
	public function Add($path, $view, $class)
	{
		$this->route[] = Array($path, $view, $class);
	}

	// Inicia as rotas
	public function Init()
	{
		for($i = 0; $i < count($this->route); $i++)
		{
			$var = $this->route[$i];

			if(preg_match("#^".$var[0]."$#", implode("/", $this->path)))
			{
				$this->SetFoundRoute($var);
				break;
			}
		}

		$this->LoadRoute();
	}

	// Executa a rota encontrada
	public function LoadRoute()
	{
		if($this->foundRoute) // Alguma rota encontrada
		{
			// Pega a página atual
			$load = $this->foundRoute;

			// Inicia a classe Model e Control da página
			$control = $this->GetControl($load[1], $load[2], array($this, $load[1], $load[2]));

			// Realiza as ações da página
			$control->doActions();

			// Mostra a página
			exit($control->getPathView());
		}
		else
		{
			$return = $this->ReturnURL();
			$this->Redirect($return != "" ? $return : $this->route[0][0]);
		}
	}

	// Path
	public function setPath($path)
	{
		$this->path = $path;
	}
	public function getPath($id = null)
	{
		if(is_null($id))
		{
			return implode("/", $this->path);
		}
		else
		{
			if(isset($this->path[$id]))
			{
				return $this->path[$id];
			}
			else
			{
				return false;
			}
		}
	}

	// Altera o estado para ver se a route final foi encontrada
	public function SetFoundRoute($found)
	{
		$this->foundRoute = $found;
	}

	// Pega o Control da rota atual
	public function GetControl($file, $class, $args)
	{
		require_once(FOLDER_CONTROL.$file.".php");

		$control = new ReflectionClass("Control".$class);

		return $control->newInstanceArgs($args);
	}

	// Retorna um path para ser usado com a URL de uma página anterior a atual
	public function ReturnURL()
	{
		array_pop($this->path);

		if(count($this->path) > 0)
		{
			return implode("/", $this->path);
		}
		else
		{
			return $this->route[0][0];
		}
	}

	// Redireciona usuário para outra página
	public function Redirect($link)
	{
		header("Location: ".URL.$link);
		exit();
	}
}

?>