<?php

class ControlAjaxDatatableUsuarios extends Control
{
	public $painel;
	public $json;

	public $_conta;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");
		$this->loadClass("json");

		// Inicia as classes necessárias
		$this->painel = new Painel($this);
		$this->json = new JSON();

		// Verifica se está logado
		if($this->painel->isLogged())
		{
			// Inclui a classe
			$this->loadClass("conta");

			// Inicia classes
			$this->_conta = new Conta($this);

			// Retorno para a tabela
			$this->json->add("data", $this->_conta->Listar());
		}

		exit($this->json->getString());
	}
}

?>