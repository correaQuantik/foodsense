<?php

class ControlAjaxDatatablePostagem extends Control
{
	public $painel;
	public $json;

	public $_p;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");
		$this->loadClass("json");

		// Inicia as classes necessárias
		$this->painel = new Painel($this);
		$this->json = new JSON();

		// Verifica se está logado
		if($this->painel->isLogged())
		{
			// Inclui a classe
			$this->loadClass("postagem");

			// Inicia classes
			$this->_p = new Postagem($this);

			// Retorno para a tabela
			$this->json->add("data", $this->_p->Listar($this->painel->conta));
		}

		exit($this->json->getString());
	}
}

?>