<?php

class ControlAjaxFrontPostagem extends Control
{
	public $json;

	public $facebook;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("json");

		// Inicia a sessão
		session_start();

		// Inclui a classe do Facebook
		$this->loadClass("Facebook/autoload");
		$this->loadClass("facebook");

		// Inicia as classes necessárias
		$this->json = new JSON();

		// Inicia o Facebook
		$this->facebook = new Facebook($this->sql);

		if(isset($_POST["url"]))
		{
			// URL de redirecionamento
			$url = $_POST["url"];

			// Guarda a URL na sessão
			$_SESSION["url"] = $url;

			// Retorno da URL para o usuário
			$this->json->add("url", $this->facebook->LoginFacebook(URL."facebook-login.php"));
		}
		else
		{
			$user = $this->facebook->GetUserData();

			if(!!!$user)
			{
				$this->json->add("error", "Você deve estar logado para escrever um comentário!");
			}
			else
			{
				if(isset($_POST["postagem"]) && isset($_POST["comment"]))
				{
					$id_postagem = (int)($_POST["postagem"]);
					$id_comentario = isset($_POST["comentario"]) ? (int)($_POST["comentario"]) : 0;
					$comment = $_POST["comment"];

					if($id_postagem <= 0)
					{
						$this->json->add("error", "Postagem inválida!");
					}
					else if($id_comentario < 0)
					{
						$this->json->add("error", "Comentário inválido!");
					}
					else
					{
						$continue = true;

						if($comment == "")
						{
							$this->json->add("error", ($id_comentario > 0 ? "Resposta" : "Comentário")." não pode ser vazio!");
							$continue = false;
						}

						if($continue)
						{
							if($this->sql->count("postagem", array(array("id", $id_postagem))) == 0)
							{
								$this->json->add("error", "Postagem inválida!");
								$continue = false;
							}

							if($id_comentario > 0 && $this->sql->count("postagem_comentario", array(array("id", $id_comentario), array("idpostagem", $id_postagem))) == 0)
							{
								$this->json->add("error", "Comentário inválido!");
								$continue = false;
							}

							if($continue)
							{
								$comment = preg_replace("/[\n]/", "<br>", $comment);
								$comment = preg_replace("/[\r\t]+/", "", $comment);

								$insert = $this->sql->insert("postagem_comentario", array(
									array("idpostagem", $id_postagem),
									array("idfacebook", $user->sql->id),
									array("aprovado", 0),
									array("resposta", ($id_comentario > 0 ? 1 : 0)),
									array("comentario", $comment),
									array("criado", $this->sql->date())
								));

								if(!!$insert)
								{
									if($id_comentario > 0)
									{
										$this->sql->insert("postagem_resposta", array(
											array("idcomentario", $id_comentario),
											array("idresposta", $insert)
										));
									}

									$this->json->add("success", "Comentário enviado com sucesso! Ele passará pela aprovação antes de ser exibido no Blog!");
								}
								else
								{
									$this->json->add("error", "Falha ao enviar comentário, tente novamente!");
								}
							}
						}
					}
				}
			}
		}

		exit($this->json->getString());
	}
}

?>