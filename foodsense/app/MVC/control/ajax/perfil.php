<?php

class ControlAjaxPerfil extends Control
{
	public $painel;
	public $json;

	public $_c;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");
		$this->loadClass("json");

		// Inicia as classes necessárias
		$this->painel = new Painel($this);
		$this->json = new JSON();

		// Verifica se não está logado
		if(!$this->painel->isLogged())
		{
			$this->json->add("error", "Você precisa estar logado!");
		}
		else
		{
			// Inclui a classe
			$this->loadClass("conta");

			// Inicia a classe
			$this->_c = new Conta($this);

			if(isset($_FILES["file"]))
			{
				// Name
				$type = isset($_POST["type"]) ? $_POST["type"] : false;

				if(!$type)
				{
					$this->json->add("error", "Tipo de imagem inválido!");
				}
				else if($type == "foto")
				{
					// Adiciona imagem
					$this->_c->AddImagem($this->painel->conta, $_FILES["file"], $this->json, $type);
				}
			}
			else
			{
				// Editar
				$this->_c->Editar($this->painel->conta->id, $_POST, $this->json, true);
			}
		}

		exit($this->json->getString());
	}
}

?>