<?php

class ControlAjaxTermos extends Control
{
	protected $painel;
	protected $json;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");
		$this->loadClass("json");

		// Inicia as classes necessárias
		$this->painel = new Painel($this, true);
		$this->json = new JSON();

		// Verifica se não está logado
		if($this->painel->isLogged())
		{
			$this->sql->insert("conta_termos", array(array("idconta", $this->painel->conta->id), array("ip", $_SERVER['REMOTE_ADDR']), array("criado", $this->sql->date())));

			$this->json->add("success", "Obrigado por aceitar nossos termos!<br><br><b>Aguarde...</b>");
			$this->json->add("reload", true);
		}
		else
		{
			$this->json->add("error", "Você já está logado!");
		}

		exit($this->json->getString());
	}
}

?>