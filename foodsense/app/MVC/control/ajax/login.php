<?php

class ControlAjaxLogin extends Control
{
	protected $painel;
	protected $json;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");
		$this->loadClass("json");

		// Inicia as classes necessárias
		$this->painel = new Painel($this);
		$this->json = new JSON();

		// Verifica se não está logado
		if(!$this->painel->isLogged())
		{
			$this->startLogin();
		}
		else
		{
			$this->json->add("error", "Você já está logado!");
		}

		exit($this->json->getString());
	}

	public function startLogin()
	{
		if(isset($_POST["login"]) && isset($_POST["senha"]))
		{
			$login = $_POST["login"];
			$senha = $_POST["senha"];

			if($login == "")
			{
				$this->json->add("error", "Preencha o campo do nome de usuário!");
			}
			else if($senha == "")
			{
				$this->json->add("error", "Preencha o campo da senha!");
			}
			else
			{
				$senha = md5($senha);

				if(!!$this->sql->selectFirst(array("1"), "conta", array(array("login", $login), array("senha", $senha))))
				{
					$this->painel->setSession("login", $login);
					$this->painel->setSession("senha", $senha);

					$this->json->add("success", "Seja bem-vindo ao painel!");
				}
				else
				{
					$this->json->add("error", "Login ou senha inválido!");
				}
			}
		}
		else
		{
			$this->json->add("error", "Preencha os campos requeridos!");
		}
	}
}

?>