<?php

class ControlAjaxUsuarioEditar extends Control
{
	public $painel;
	public $json;

	public $_conta;

	public $idconta;
	public $conta;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");
		$this->loadClass("json");

		// Inicia as classes necessárias
		$this->painel = new Painel($this);
		$this->json = new JSON();

		// Verifica se não está logado
		if($this->painel->isLogged())
		{
			// Inclui a classe
			$this->loadClass("conta");

			// Inicia a classe
			$this->_conta = new Conta($this);

			// ID
			$this->idconta = $this->getRoute(3);

			// Retorna a conta selecionada
			$this->conta = $this->_conta->Mostra($this->idconta);

			if(isset($_FILES["file"]))
			{
				// Name
				$type = isset($_POST["type"]) ? $_POST["type"] : false;

				if(!$type)
				{
					$this->json->add("error", "Tipo de imagem inválido!");
				}
				else if($type == "foto")
				{
					// Adiciona imagem
					$this->_conta->AddImagem($this->conta, $_FILES["file"], $this->json, $type);
				}
			}
			else
			{
				// Editar
				$this->_conta->Editar($this->idconta, $_POST, $this->json);
			}
		}
		else
		{
			$this->json->add("error", "Você não pode fazer isso!");
		}

		exit($this->json->getString());
	}
}

?>