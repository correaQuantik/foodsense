<?php

class ControlAjaxUsuarioApagar extends Control
{
	public $painel;
	public $json;

	public $_c;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");
		$this->loadClass("json");

		// Inicia as classes necessárias
		$this->painel = new Painel($this);
		$this->json = new JSON();

		// Verifica se não está logado
		if($this->painel->isLogged())
		{
			// Inclui a classe
			$this->loadClass("conta");

			// Inicia a classe
			$this->_u = new Conta($this);

			// ID da conta selecionado
			$id_conta = $this->getRoute(3);

			// Apagar
			$this->_u->Apagar($id_conta, $this->json);
		}
		else
		{
			$this->json->add("error", "Você não pode fazer isso!");
		}

		exit($this->json->getString());
	}
}

?>