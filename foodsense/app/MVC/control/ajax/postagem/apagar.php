<?php

class ControlAjaxPostagemApagar extends Control
{
	public $painel;
	public $json;

	public $_postagem;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");
		$this->loadClass("json");

		// Inicia as classes necessárias
		$this->painel = new Painel($this);
		$this->json = new JSON();

		// Verifica se não está logado
		if($this->painel->isLogged())
		{
			// Inclui a classe
			$this->loadClass("postagem");

			// Inicia classes
			$this->_postagem = new Postagem($this);

			// ID
			$id_postagem = $this->getRoute(3);

			// Apagar
			$this->_postagem->Apagar($id_postagem, $this->json);
		}

		exit($this->json->getString());
	}
}

?>