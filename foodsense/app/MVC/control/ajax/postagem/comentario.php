<?php

class ControlAjaxPostagemComentario extends Control
{
	public $painel;
	public $json;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");
		$this->loadClass("json");

		// Inicia as classes necessárias
		$this->painel = new Painel($this);
		$this->json = new JSON();

		// Verifica se não está logado
		if($this->painel->isLogged())
		{
			if(isset($_POST["id"]) && isset($_POST["type"]))
			{
				$id = (int)($_POST["id"]);
				$type = (int)($_POST["type"]);

				if($this->sql->count("postagem_comentario", array(array("id", $id))) == 0)
				{
					$this->json->add("error", "Comentário inválido!");
				}
				else
				{
					if($type == 0) // Deleta
					{
						$this->sql->delete("postagem_comentario", array(array("id", $id)));
						$this->json->add("success", "Comentário apagado com sucesso!");
						$this->json->add("reload", true);
					}
					else if($type == 1) // Aceita
					{
						$this->sql->update("postagem_comentario", array(array("aprovado", 1)), array(array("id", $id)));
						$this->json->add("success", "Comentário aprovado com sucesso!");
						$this->json->add("reload", true);
					}
					else
					{
						$this->json->add("error", "Ação inválida!");
					}
				}
			}
			else
			{
				$this->json->add("error", "Inválido!");
			}
		}

		exit($this->json->getString());
	}
}

?>