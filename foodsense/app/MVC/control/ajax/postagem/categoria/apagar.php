<?php

class ControlAjaxPostagemCategoriaApagar extends Control
{
	public $painel;
	public $json;

	public $postagem;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");
		$this->loadClass("json");

		// Inicia as classes necessárias
		$this->painel = new Painel($this);
		$this->json = new JSON();

		// Verifica se não está logado
		if($this->painel->isLogged())
		{
			// Inclui a classe
			$this->loadClass("postagem");

			// Inicia classes
			$this->postagem = new Postagem($this);

			// ID
			$idcategoria = $this->getRoute(4);

			// Apagar
			$this->postagem->ApagarCategoria($idcategoria, $this->json);
		}

		exit($this->json->getString());
	}
}

?>