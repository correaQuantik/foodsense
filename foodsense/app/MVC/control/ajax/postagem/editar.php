<?php

class ControlAjaxPostagemEditar extends Control
{
	public $painel;
	public $json;

	public $_postagem;

	public $idpostagem;
	public $postagem;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");
		$this->loadClass("json");

		// Inicia as classes necessárias
		$this->painel = new Painel($this);
		$this->json = new JSON();

		// Verifica se não está logado
		if($this->painel->isLogged())
		{
			// Inclui a classe
			$this->loadClass("postagem");

			// Inicia classes
			$this->_postagem = new Postagem($this);

			// ID
			$this->idpostagem = $this->getRoute(3);

			// Retorna a postagem selecionada
			$this->postagem = $this->_postagem->Mostra($this->idpostagem);

			if(!!!$this->postagem)
			{
				$this->json->add("error", "Postagem inválida!");
			}
			else
			{
				if($this->painel->conta->categoria != 1 && $this->postagem->status == 6)
				{
					$this->json->add("error", "Está postagem não pode ser editada!");
				}
				else
				{
					if(isset($_FILES["file"]))
					{
						// Name
						$type = isset($_POST["type"]) ? $_POST["type"] : false;

						if(!$type)
						{
							$this->json->add("error", "Tipo de imagem inválido! #1");
						}
						else if($type == "capa" || $type == "galeria")
						{
							// Adiciona imagem
							$this->_postagem->AddImagem($this->postagem, $_FILES["file"], $this->json, $type);
						}
						else if($type == "arquivos")
						{
							// Adiciona arquivo
							$this->_postagem->AddArquivo($this->postagem, $_FILES["file"], $this->json);
						}
					}
					else
					{
						// Editar
						$this->_postagem->Editar($this->postagem->id, $_POST, $this->json);
					}
				}
			}
		}

		exit($this->json->getString());
	}
}

?>