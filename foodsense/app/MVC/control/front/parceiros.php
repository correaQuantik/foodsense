<?php

class ControlParceiros extends Control
{
	public $_front;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("front");

		// Inicia as classes necessárias
		$this->_front = new Front($this);

		// Define menu ativo
		$this->_front->SetActive("Parceiros");

		// Deixar esses dois por ultimo
		$this->setHeader("home/header");
		$this->setFooter("home/footer");
	}
}

?>