<?php

function InsertCheck ($categoria, $stringId, $count,$title,$kcal, $kcalB = null, $subtitle = null) {
	$rtn = "<div class='col-md-12 customCheck ".$stringId."'>";

	if ($count == 1) {
		$rtn .= "<input type='checkbox' name='".$stringId."[]' value='".$title."' class='".$categoria."'>";
	}
	else {
		for ($c = 0; $c < $count; $c++) {
			$rtn .= "<input type='checkbox' name='".$stringId."[]' value='".$title.$c."' class='".$categoria."'>";
		}
	}

	$rtn .= "<div class='col-md-10'>";
	
	if (!is_null($kcalB)) {
		$rtn .= utf8_encode($title)." - ".$kcal."kcal/".$kcalB."kcal";
	}
	else {
		$rtn .= utf8_encode($title)." - ".$kcal."kcal";
	}
	
	$rtn .= "</div>";

	if (!is_null($subtitle)) {
		$rtn .= "<div class='col-md-12 descrip'>";
		$rtn .= utf8_encode($subtitle);
		$rtn .= "</div>";
	}

	$rtn .= "</div>";

	return $rtn;
}

class ControlMontar extends Control
{
	public $_front;

	public function Populate() {
		$categorias = 	$this->sql->select(array("*"),"produtos_categoria",array(array("habilitado","1")));
		$igredientes = 	$this->sql->select(array("*"),"produtos_igredientes",array(array("habilitado","1")));
		$molhos = 		$this->sql->select(array("*"),"produtos_molhos",array(array("habilitado","1")));
		$papinhaS = 	$this->sql->select(array("*"),"produtos_papinhasal",array(array("habilitado","1")));
		$papinhaF = 	$this->sql->select(array("*"),"produtos_papinhafr",array(array("habilitado","1")));

		?>

		<form method="POST">
				<!--Header-->
				<div class="col-md-3 Box">
					<div class="col-md-12">
						<div class="control-group">
							<select id="categoriaSelect" class="control-option col-md-12">
								<?php
									foreach ($categorias as $key=>$value) {
										echo "<option value=\"".$value->stringId."\">".$value->title."</option>";
									}
								 ?>
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est.</p>
					</div>
				</div>
				<!--Content-->
				<div class="col-md-6">

				
				<div class='col-md-6 Box ForcedWidth'>
				<div class='col-md-12'>

				<?php

				foreach ($categorias as $key=>$value) {
					if ($value->gQuente != ""){
						
						if ($value->guarniCount == 1) {
							echo "<span class='col-md-12 customCheck ".$value->stringId."'> Escolha <span class='number guarnição ".$value->stringId."'>1</span> guarnição para colocar no seu combo</span>";
						}
						else {
							echo "<span class='col-md-12 customCheck ".$value->stringId."'> Escolha <span class='number guarnição ".$value->stringId."'>".$value->guarniCount."</span> guarnições para colocar no seu combo</span>";
						}
						
						if ($value->isGuarnSum == 1) {
							echo "<span class='descrip customCheck sum ".$value->stringId."'><br>A caloria é a soma de todas as guarnições escolhidas</span>";
						}
					}
				}

				echo "</div>";
				echo "<div class='col-md-12'>";

				foreach ($categorias as $key=>$value) {
					if ($value->gQuente != ""){

						$checkboxes = explode(",",$value->gQuente);

						foreach ($checkboxes as $subKey=>$subValue) {
							$guarnInfo = $this->sql->select(array("*"),"produtos_guarnicaoquente",array(array("habilitado","1"),array("id",$subValue)));

							echo "<div class='col-md-12 customCheck ".$value->stringId."'>";
							echo "<input type='checkbox' name='".$value->stringId."[]' value='".$guarnInfo[0]->title."' class='guarnição'>";
							echo "<div class='col-md-10'>";
							if (!isset($guarnInfo[0]->kcalU)) {
								echo utf8_encode($guarnInfo[0]->title)." - ".$guarnInfo[0]->kcalP."kcal/".$guarnInfo[0]->kcalM."kcal";
							}
							else {
								echo utf8_encode($guarnInfo[0]->title)." - ".$guarnInfo[0]->kcalU."kcal";
							}
							echo "</div>";
							if ($guarnInfo[0]->subTitle != "") {
							echo "<div class='col-md-12 descrip'>";
							echo utf8_encode($guarnInfo[0]->subTitle);
							echo "</div>";
							}
							echo "</div>";
						}
					}	
				}
				echo "</div>";
				echo "</div>";
				
				//Colocar extra
				echo "<div class='col-md-6 Box ForcedWidth'>";
				echo "<div class='col-md-12'>";

				foreach ($categorias as $key=>$value) {
					if ($value->extra != ""){
						
						if ($value->extraCount == 1) {
							echo "<span class='col-md-12 customCheck ".$value->stringId."'> Escolha <span class='number extra ".$value->stringId."'>1</span> extra para colocar no seu combo</span>";
						}
						else {
							echo "<span class='col-md-12 customCheck ".$value->stringId."'> Escolha <span class='number extra ".$value->stringId."'>".$value->guarniCount."</span> extras para colocar no seu combo</span>";
						}
					}
				}

				echo "</div>";
				echo "<div class='col-md-12'>";

				foreach ($categorias as $key=>$value) {
					if ($value->extra != ""){

						$checkboxes = explode(",",$value->extra);

						foreach ($checkboxes as $subKey=>$subValue) {
							$guarnInfo = $this->sql->select(array("*"),"produtos_extra",array(array("habilitado","1"),array("id",$subValue)));

							echo "<div class='col-md-12 customCheck ".$value->stringId."'>";
							echo "<input type='checkbox' name='".$value->stringId."[]' value='".$guarnInfo[0]->title."' class='extra'>";
							echo "<div class='col-md-10'>";
							echo utf8_encode($guarnInfo[0]->title)." - ".$guarnInfo[0]->cal."kcal";
							echo "</div>";
							if ($guarnInfo[0]->subTitle != "") {
							echo "<div class='col-md-12 descrip'>";
							echo utf8_encode($guarnInfo[0]->subTitle);
							echo "</div>";
							}
							echo "</div>";
						}
					}	
				}
				echo "</div>";
				echo "</div>";

				//Colocar opcional
				echo "<div class='col-md-6 Box ForcedWidth'>";
				echo "<div class='col-md-12'>";

				foreach ($categorias as $key=>$value) {
					if ($value->opcional != ""){
						echo "<span class='col-md-12 customCheck ".$value->stringId."'> Escolha <span class='number opcional ".$value->stringId."'>1</span> opcional para colocar no seu combo</span>";
					}
				}

				echo "</div>";
				echo "<div class='col-md-12'>";

				foreach ($categorias as $key=>$value) {
					if ($value->opcional != ""){

						$checkboxes = explode(",",$value->opcional);

						foreach ($checkboxes as $subKey=>$subValue) {
							$guarnInfo = $this->sql->select(array("*"),"produtos_opcional",array(array("habilitado","1"),array("id",$subValue)));

							echo "<div class='col-md-12 customCheck ".$value->stringId."'>";
							echo "<input type='checkbox' name='".$value->stringId."[]' value='".$guarnInfo[0]->title."' class='opcional'>";
							echo "<div class='col-md-10'>";
							echo utf8_encode($guarnInfo[0]->title)." - ".$guarnInfo[0]->kcal."kcal";
							echo "</div>";
							if ($guarnInfo[0]->subTitle != "") {
							echo "<div class='col-md-12 descrip'>";
							echo utf8_encode($guarnInfo[0]->subTitle);
							echo "</div>";
							}
							echo "</div>";
						}
					}	
				}
				echo "</div>";
				echo "</div>";

				//Colocar papinha salgada
				echo "<div class='col-md-6 Box ForcedWidth'>";
				echo "<div class='col-md-12'>";

				foreach ($categorias as $key=>$value) {
					if ($value->papinhaS == 1){
						foreach ($papinhaS as $subKey=>$subValue) {
							echo "<div class='col-md-12 customCheck ".$value->stringId."'>";
							echo "<input type='checkbox' name='".$value->stringId."[]' value='".$subValue->title."'' class='papinhaS'>";
							echo "<div class='col-md-10'>";
							echo utf8_encode($subValue->title)." - ".$subValue->kcal."kcal";
							echo "</div>";
							if ($subValue->subTitle != "") {
							echo "<div class='col-md-12 descrip'>";
							echo utf8_encode($subValue->subTitle);
							echo "</div>";
							}
							echo "</div>";
						}
					}	
				}
				echo "</div>";
				echo "</div>";

				//Colocar papinha de frutas
				echo "<div class='col-md-6 Box ForcedWidth'>";
				echo "<div class='col-md-12'>";

				foreach ($categorias as $key=>$value) {
					if ($value->papinhaF == 1){
						foreach ($papinhaF as $subKey=>$subValue) {
							echo "<div class='col-md-12 customCheck ".$value->stringId."'>";
							echo "<input type='checkbox' name='".$value->stringId."[]' value='".$subValue->title."' class='papinhaF'>";
							echo "<div class='col-md-10'>";
							echo utf8_encode($subValue->title)." - ".$subValue->kcal."kcal";
							echo "</div>";
							if ($subValue->subTitle != "") {
							echo "<div class='col-md-12 descrip'>";
							echo utf8_encode($subValue->subTitle);
							echo "</div>";
							}
							echo "</div>";
						}
					}	
				}
				echo "</div>";
				echo "</div>";

				//Colocar igredientes
				echo "<div class='col-md-6 Box ForcedWidth'>";
				echo "<div class='col-md-12'>";

				foreach ($categorias as $key=>$value) {
					if ($value->igrediente == 1){
						
						if ($value->igredienteCount == 1) {
							echo "<span class='col-md-12 customCheck ".$value->stringId."'> Escolha <span class='number igrediente ".$value->stringId."'>1</span> ingrediente para colocar no seu combo</span>";
						}
						else {
							echo "<span class='col-md-12 customCheck ".$value->stringId."'> Escolha <span class='number igrediente ".$value->stringId."'>".$value->igredienteCount."</span> ingredientes para colocar no seu combo</span>";
						}
					}
				}

				echo "</div>";
				echo "<div class='col-md-12'>";

				foreach ($categorias as $key=>$value) {
					if ($value->igrediente == 1){
						foreach ($igredientes as $subKey=>$subValue) {
							echo "<div class='col-md-12 customCheck ".$value->stringId."'>";

							for ($count = 0; $count < $value->igredienteCount; $count++) {
								echo "<input type='checkbox' name='".$value->stringId."[]' value='".$subValue->title.$count."' class='igrediente'>";
							}

							echo "<div class='col-md-10'>";
							echo utf8_encode($subValue->title)." - ".$subValue->kcal."kcal";
							echo "</div>";
							if ($subValue->subTitle != "") {
							echo "<div class='col-md-12 descrip'>";
							echo utf8_encode($subValue->subTitle);
							echo "</div>";
							}
							echo "</div>";
						}
					}	
				}
				echo "</div>";
				echo "</div>";

				//Colocar molhos
				echo "<div class='col-md-6 Box ForcedWidth'>";
				echo "<div class='col-md-12'>";

				foreach ($categorias as $key=>$value) {
					if ($value->molho == 1){
						
						if ($value->molhoCount == 1) {
							echo "<span class='col-md-12 customCheck ".$value->stringId."'> Escolha <span class='number molho ".$value->stringId."'>1</span> molho para colocar no seu combo</span>";
						}
						else {
							echo "<span class='col-md-12 customCheck ".$value->stringId."'> Escolha <span class='number molho ".$value->stringId."'>".$value->molhoCount."</span> molhos para colocar no seu combo</span>";
						}
					}
				}

				echo "</div>";
				echo "<div class='col-md-12'>";

				foreach ($categorias as $key=>$value) {
					if ($value->molho == 1){
						foreach ($molhos as $subKey=>$subValue) {
							echo InsertCheck("molho",$value->stringId,1,$subValue->title,$subValue->kcal,null,$subValue->subTitle);
						}
					}	
				}

				?>
				</div>
				</div>
		</div>
		<!--Footer-->
		<div class="col-md-3 Box">
			<div class="col-md-12"><h2>Qual é a sua opção</h2></div>
			<!--Tamanho do prato-->
			<div class="col-md-12">
				<div class="col-md-12 form-group">
					<label class="col-md-11">Tamanho médio</label>
					<input type="radio" name="size" value="m" checked="checked" class="col-md-1">
				</div>
				<div class="col-md-12 form-group">
					<label class="col-md-11">Tamanho grande</label>
					<input type="radio" name="size" value="g" class="col-md-1">
				</div>
			</div>
			<!--Quantidade de calorias-->
			<div class="col-md-12">
				<span>A quantidade de calorias do prato é:</span> 
				<span style="font-weight: bold; text-align: center; font-size: 1.4em;" class="col-md-12"><span id="totalCalories" style="font-weight: bold; text-align: center; font-size: 1.4em;">0</span> kcal</span>
			</div>
			<!--Inserir no carrinho de compras-->
			<div class="col-md-12 form-group">
				<br>
				<div class="col-md-12">
					<input type="checkbox" name="insertCarrinho" class="col-md-1">
					<label class="col-md-10">Desejo colocar esse combo no meu carrinho</label>
				</div>
				<div class="col-md-12">
					<input type="submit" value="Finalizar" name="send" class="form-control" style="background-color: #ff6400; color:white; border:none;">
				</div>
			</div>
		</div>
		<input type="hidden" name="jsonData">
		</form>

		<script type="text/javascript">

		var allCheckboxes = {};
		var selectedCheckboxes = {};

		var jsonFile = {};

		selectedCheckboxes["guarnição"] = {};
		selectedCheckboxes["opcional"] = {};
		selectedCheckboxes["extra"] = {};
		selectedCheckboxes["papinhaS"] = {};
		selectedCheckboxes["papinhaF"] = {};
		selectedCheckboxes["igrediente"] = {};
		selectedCheckboxes["molho"] = {};

		var totalCalories = 0;

		<?php

			foreach ($categorias as $key=>$value) {

				if ($value->gQuente != ""){

					$checkboxes = explode(",",$value->gQuente);

					foreach ($checkboxes as $subKey=>$subValue) {
						$guarnInfo = $this->sql->select(array("*"),"produtos_guarnicaoquente",array(array("habilitado","1"),array("id",$subValue)));

						if (!isset($guarnInfo[0]->kcalU)) {
							echo "allCheckboxes['".$guarnInfo[0]->title."'] = ".$guarnInfo[0]->kcalP." + '/' + ".$guarnInfo[0]->kcalM.";";
						}
						else {
							echo "allCheckboxes['".$guarnInfo[0]->title."'] = ".$guarnInfo[0]->kcalU.";";
						}
					}
				}

				if ($value->extra != ""){

					$checkboxes = explode(",",$value->extra);

					foreach ($checkboxes as $subKey=>$subValue) {
						$guarnInfo = $this->sql->select(array("*"),"produtos_extra",array(array("habilitado","1"),array("id",$subValue)));

						echo "allCheckboxes['".$guarnInfo[0]->title."'] = ".$guarnInfo[0]->cal.";";
					}	
				}

				if ($value->opcional != ""){

					$checkboxes = explode(",",$value->opcional);

					foreach ($checkboxes as $subKey=>$subValue) {
						$guarnInfo = $this->sql->select(array("*"),"produtos_opcional",array(array("habilitado","1"),array("id",$subValue)));

						echo "allCheckboxes['".$guarnInfo[0]->title."'] = ".$guarnInfo[0]->kcal.";";
					}
				}

				if ($value->papinhaS == 1){
					foreach ($papinhaS as $subKey=>$subValue) {
						echo "allCheckboxes['".$subValue->title."'] = ".$subValue->kcal.";";
					}
				}

				if ($value->papinhaF == 1){
					foreach ($papinhaF as $subKey=>$subValue) {
							echo "allCheckboxes['".$subValue->title."'] = ".$subValue->kcal.";";
					}
				}

				if ($value->igrediente == 1){
					foreach ($igredientes as $subKey=>$subValue) {
						echo "allCheckboxes['".$subValue->title."'] = ".$subValue->kcal.";";
					}
				}

				if ($value->molho == 1){
					foreach ($molhos as $subKey=>$subValue) {
						echo "allCheckboxes['".$subValue->title."'] = ".$subValue->kcal.";";
					}
				}
			}
		?>
		</script>

		<?php
	}

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("front");
		$this->loadClass("categoria.class");

		// Inicia as classes necessárias
		$this->_front = new Front($this);

		// Define menu ativo
		$this->_front->SetActive("Montar");

		// Deixar esses dois por ultimo
		$this->setHeader("home/header");
		$this->setFooter("home/footer");
	}
}

?>