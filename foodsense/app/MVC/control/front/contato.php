<?php

class ControlContato extends Control
{
	public $_front;

	public function doActions()
	{
		$this->SendEmail();

		// Classes que serão usadas
		$this->loadClass("front");

		// Inicia as classes necessárias
		$this->_front = new Front($this);

		// Define menu ativo
		$this->_front->SetActive("Contato");

		// Deixar esses dois por ultimo
		$this->setHeader("home/header");
		$this->setFooter("home/footer");
	}

	public function SendEmail () {
		if (isset($_POST["contactName"]) && isset($_POST["contactEmail"]) && isset($_POST["contactTel"]) && isset($_POST["contactSubject"])) {
			$name = $_POST["contactName"];
			$email = $_POST["contactEmail"];
			$telefone = $_POST["contactTel"];
			$assunto = $_POST["contactSubject"];

			$rtn = "<div class='form-group'><a class='btn btn-success col-md-12'>Email enviado com sucesso</a></div>";
		}
	}
}

?>