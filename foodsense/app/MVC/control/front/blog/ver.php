<?php

class ControlBlogVer extends Control
{
	public $_front;

	public $_postagem;

	public $_conta;

	public $search;
	public $pagina;

	public $id_postagem;
	public $postagem;

	public $facebook;
	public $user;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("front");
		$this->loadClass("postagem");
		$this->loadClass("conta");

		// Inicia as classes necessárias
		$this->_front = new Front($this);
		$this->_postagem = new Postagem($this);
		$this->_conta = new Conta($this);

		// Tipo da busca
		$this->search = $this->getRoute(1);

		// Número da página atual
		$this->pagina = (int)($this->getRoute(2));
		$this->pagina = ($this->pagina - 1 < 0 ? 1 : $this->pagina);

		// Postagem aberta
		$this->id_postagem = (int)($this->getRoute(3));

		// Retorna a postagem selecionada
		$this->postagem = $this->_postagem->Mostra($this->id_postagem);

		if(!!$this->postagem)
		{
			if($this->postagem->status != 6)
			{
				// Inclui classe do painel
				$this->loadClass("painel");

				// Inicia o painel
				$painel = new Painel($this);

				if(!$painel->isLogged())
				{
					$this->getRoute()->Redirect("Blog/".$this->search."/".$this->pagina);
				}
			}

			// Inicia a sessão
			@session_start();

			// Inclui a classe do Facebook
			$this->loadClass("Facebook/autoload");
			$this->loadClass("facebook");

			// Inicia o Facebook
			$this->facebook = new Facebook($this->sql);
			$this->user = $this->facebook->GetUserData();

			// Define menu ativo
			$this->_front->SetActive("Blog");
			$this->_front->SetNavbar(true);

			// Deixar esses dois por ultimo
			$this->setHeader("home/header");
			$this->setFooter("home/footer");
		}
		else
		{
			$this->getRoute()->Redirect("Blog/".$this->search."/".$this->pagina);
		}
	}



	public function GetCapa()
	{
		if($this->postagem->capa != "none.png")
		{
			$capa = FOLDER_APP."../img/noticias/capa/".$this->postagem->capa;

			return "<div class=\"postagem-view-capa\">
				<img src=\"".URL."img/noticias/capa/".$this->postagem->capa."\">
			</div>";
		}
	}



	public function GetYoutube()
	{
		$rtn = "";

		if($this->postagem->youtube != "")
		{
			$rtn = "<div class=\"postagem-view-paper\">
				<div class=\"postagem-view-paper-title\"><i class=\"fa fa-youtube fa-fw\"></i>Vídeo</div>

				<div class=\"postagem-view-youtube\">
					<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/".$this->postagem->youtube."\" frameborder=\"0\" allowfullscreen></iframe>
				</div>
			</div>";
		}

		return $rtn;
	}



	public function GetGaleria()
	{
		$rtn = "";

		foreach($this->postagem->fotos as $foto)
		{
			$url = URL."img/noticias/galeria/".$foto->imagem;

			$rtn .= "<a href=\"".$url."\" class=\"postagem-view-foto\" data-fancybox=\"galeria\"><img src=\"".$url."\"></a>";
		}

		if($rtn != "")
		{
			$rtn = "<div class=\"postagem-view-paper\">
				<div class=\"postagem-view-paper-title\"><i class=\"fa fa-picture-o fa-fw\"></i>Galeria</div>

				<div class=\"postagem-view-fotos\">".$rtn."</div>
			</div>";
		}

		return $rtn;
	}



	public function GetArquivos()
	{
		$rtn = "";

		if(!!$this->postagem->arquivos)
		{
			$rtn .= "<div class=\"postagem-view-paper\">";

			$rtn .= "<div class=\"postagem-view-paper-title\"><i class=\"fa fa-download fa-fw\"></i>Arquivos para download</div>";

			$rtn .= "<div class=\"postagem-view-arquivos\">";
			foreach($this->postagem->arquivos as $val)
			{
				$rtn .= "<a href=\"".URL."Download/".$val->id."\" class=\"postagem-view-arquivo\" target=\"_blank\">".$val->nome."</a>";
			}
			$rtn .= "</div>";

			$rtn .= "</div>";
		}

		return $rtn;
	}



	public function GetCategorias()
	{
		$rtn = "";

		foreach($this->postagem->categorias as $categoria)
		{
			$rtn .= "<span class=\"postagem-view-categoria\">".$categoria->nome."</span>";
		}

		if($rtn != "")
		{
			$rtn = "<div class=\"postagem-view-paper\">
				<div class=\"postagem-view-paper-title\"><i class=\"fa fa-tags fa-fw\"></i>Categorias</div>

				<div class=\"postagem-view-categorias\">".$rtn."</div>
			</div>";
		}

		return $rtn;
	}



	public function GetTAGs()
	{
		$rtn = "";

		foreach($this->postagem->tags as $tag)
		{
			$rtn .= "<span class=\"postagem-view-tag\">".$tag->nome."</span>";
		}

		if($rtn != "")
		{
			$rtn = "<div class=\"postagem-view-paper\">
				<div class=\"postagem-view-paper-title\"><i class=\"fa fa-tags fa-fw\"></i>TAGs</div>

				<div class=\"postagem-view-tags\">".$rtn."</div>
			</div>";
		}

		return $rtn;
	}



	public function GetAutor()
	{
		$conta = $this->_conta->Mostra($this->postagem->idconta);

		return "<div class=\"postagem-autor\">
			<div class=\"postagem-view-paper-title\"><i class=\"fa fa-user-o fa-fw\"></i>Autor</div>

			<div class=\"postagem-autor-box\">
				".($conta->foto != "" ? "<img src=\"".URL."img/usuarios/".$conta->foto."\" class=\"postagem-autor-foto\">" : "")."

				<div class=\"postagem-autor-nome\">".$conta->nome."</div>

				".($conta->profissao != "" ? "<div class=\"postagem-autor-profissao\">".$conta->profissao."</div>" : "")."

				".($conta->sobre != "" ? "<div class=\"postagem-autor-sobre\">".$conta->sobre."</div>" : "")."

				".($conta->telefone != "" ? "<div class=\"postagem-autor-telefone\"><i class=\"fa fa-phone fa-fw\"></i> ".$conta->telefone."</div>" : "")."

				".($conta->link != "" ? "<div class=\"postagem-autor-link\"><i class=\"fa fa-link fa-fw\"></i> <a href=\"".$conta->link."\">".$conta->link."</a></div>" : "")."
			</div>
		</div>";
	}



	public function GetComentarios()
	{
		$logged = !!$this->user;

		$rtn = "<div class=\"postagem-view-paper\">
			<div class=\"postagem-view-paper-title\"><i class=\"fa fa-comments-o fa-fw\"></i>Comentários (".count($this->postagem->comentarios).")</div>

			<form action=\"".URL."Ajax/Front/Postagem\" class=\"postagem-view-comment-form".($logged ? " logged" : "")."\">";

		if($logged)
		{
			$rtn .= "<div class=\"postagem-view-comment-cell picture\">
				<span><img src=\"".$this->facebook->GetUserPicture($this->user->graph->id)."\"></span>
			</div>

			<div class=\"postagem-view-comment-cell\">
				<textarea name=\"comment\" class=\"postagem-view-comment-text\" placeholder=\"Envie um comentário\"></textarea>

				<input name=\"postagem\" class=\"hidden\" value=\"".$this->postagem->id."\">

				<button class=\"postagem-view-comment-btn on-click\">
					<i class=\"icon1 fa fa-send fa-fw\"></i>
					<i class=\"icon2 fa fa-spinner fa-spin fa-fw hidden\"></i>
					<span>Enviar comentário</span>
				</button>
			</div>";
		}
		else
		{
			$rtn .= "<input type=\"text\" name=\"url\" class=\"hidden\">

			<button class=\"postagem-view-comment-facebook on-click\">
				<i class=\"icon1 fa fa-facebook fa-fw\"></i>
				<i class=\"icon2 fa fa-spinner fa-spin fa-fw hidden\"></i>
				<span>Entre com o <b>Facebook</b> para escrever um comentário!</span>
			</button>";
		}

		$rtn .= "</form>";

		if(count($this->postagem->comentarios) > 0)
		{
			foreach($this->postagem->comentarios as $comentario)
			{
				$autor = $this->sql->selectFirst(array("*"), "facebook", array(array("id", $comentario->idfacebook)));

				$replies = "";
				$repliesSQL = $this->sql->select(array("*"), "postagem_resposta", array(array("idcomentario", $comentario->id)), null, array(array("id", "ASC")));

				if($repliesSQL != null)
				{
					foreach($repliesSQL as $replySQL)
					{
						$reply = $this->sql->selectFirst(array("*"), "postagem_comentario", array(array("id", $replySQL->idresposta)));

						$replyAutor = $this->sql->selectFirst(array("*"), "facebook", array(array("id", $reply->idfacebook)));

						$replies .= "<div class=\"postagem-comment\">
							<div class=\"postagem-comment-cell picture\">
								<span><img src=\"".$this->facebook->GetUserPicture($replyAutor->idfacebook)."\"></span>
							</div>

							<div class=\"postagem-comment-cell\">
								<div class=\"postagem-comment-box\">
									<div class=\"postagem-comment-name\">".$replyAutor->nome." <span class=\"postagem-comment-date\"><i class=\"fa fa-clock-o fa-fw\"></i> ".date("d/m/Y H:i:s", strtotime($reply->criado))."</span></div>

									<div class=\"postagem-comment-content\">".$reply->comentario."</div>
								</div>
							</div>
						</div>";
					}
				}

				$replyForm = "";

				if($logged)
				{
					$replyForm = "<form action=\"".URL."Ajax/Front/Postagem\" class=\"postagem-view-comment-form logged\">

						<div class=\"postagem-view-comment-cell picture\">
							<span><img src=\"".$this->facebook->GetUserPicture($this->user->graph->id)."\"></span>
						</div>

						<div class=\"postagem-view-comment-cell\">
							<textarea name=\"comment\" class=\"postagem-view-comment-text\" placeholder=\"Envie uma resposta\"></textarea>

							<input name=\"postagem\" class=\"hidden\" value=\"".$this->postagem->id."\">
							<input name=\"comentario\" class=\"hidden\" value=\"".$comentario->id."\">

							<button class=\"postagem-view-comment-btn on-click\">
								<i class=\"icon1 fa fa-send fa-fw\"></i>
								<i class=\"icon2 fa fa-spinner fa-spin fa-fw hidden\"></i>
								<span>Enviar resposta</span>
							</button>
						</div>
					</form>";
				}

				$rtn .= "<div class=\"postagem-comment\">
					<div class=\"postagem-comment-cell picture\">
						<span><img src=\"".$this->facebook->GetUserPicture($autor->idfacebook)."\"></span>
					</div>

					<div class=\"postagem-comment-cell\">
						<div class=\"postagem-comment-box\">
							<div class=\"postagem-comment-name\">".$autor->nome." <span class=\"postagem-comment-date\"><i class=\"fa fa-clock-o fa-fw\"></i> ".date("d/m/Y H:i:s", strtotime($comentario->criado))."</span></div>

							<div class=\"postagem-comment-content\">".$comentario->comentario."</div>

							<div class=\"postagem-comment-reply\">
								".$replies."
								".$replyForm."
							</div>
						</div>
					</div>
				</div>";
			}
		}
		else
		{
			$rtn .= "<div class=\"alert information\">Nenhum comentário encontrado!</div>";
		}

		$rtn .= "</div>";

		return $rtn;
	}
}

?>