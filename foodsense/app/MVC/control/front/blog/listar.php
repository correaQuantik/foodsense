<?php

class ControlBlogListar extends Control
{
	public $_front;

	public $search;
	public $pagina;

	public $maximo_postagens = 10;
	public $maximo_corpo = 350;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("front");
		$this->loadClass("postagem");

		// Inicia as classes necessárias
		$this->_front = new Front($this);
		$this->_postagem = new Postagem($this);

		// Define menu ativo
		$this->_front->SetActive("Blog");
		$this->_front->SetNavbar(true);

		// Tipo da busca
		$this->search = $this->getRoute(1);

		// Número da página atual
		$this->pagina = (int)($this->getRoute(2));
		$this->pagina = ($this->pagina - 1 < 0 ? 1 : $this->pagina);

		// Deixar esses dois por ultimo
		$this->setHeader("home/header");
		$this->setFooter("home/footer");
	}

	// Retorna a lista de postagens de acordo com a página
	public function ListaPostagens()
	{
		$rtn = "";

		$min = 0;
		$max = 0;

		$postagens = $this->sql->select(array("id"), "postagem", array(array("status", 6), array("titulo", "%".($this->search == "Todas" ? "" : $this->search)."%", " LIKE ")), array(($this->pagina - 1) * $this->maximo_postagens, $this->maximo_postagens), array(array("id", "DESC")));

		if(!!$postagens)
		{
			foreach($postagens as $postagem)
			{
				$postagem = $this->_postagem->Mostra($postagem->id);

				$conteudo = strip_tags($postagem->conteudo);
				$conteudo = preg_replace("/\s+/", " ", $conteudo);
				$dots = (strlen($conteudo) > $this->maximo_corpo ? true : false);
				$conteudo = substr($conteudo, 0, $this->maximo_corpo).($dots ? "..." : "");

				$data = date("d/m/Y H:i:s", strtotime($postagem->criado));

				$rtn .= "<a href=\"".URL."Blog/".$this->search."/".$this->pagina."/".$postagem->id."\" class=\"postagens-box on-click\">
					<span class=\"postagens-box-image\"><img src=\"".URL."img/noticias/capa/".$postagem->capa."\"></span>

					<span class=\"postagens-box-content\">
						<span class=\"postagens-box-title\">".$postagem->titulo."</span>

						<span class=\"postagens-box-body\">".$conteudo."</span>

						<span class=\"postagens-box-footer\">
							<span class=\"postagens-box-footer-cell\"><i class=\"fa fa-clock-o\"></i>".$data."</span>
						</span>
					</span>
				</a>";

				if($max == 0)
					$max = $postagem->id;

				$min = $postagem->id;
			}

			// Paginação aqui

			$like = ($this->search == "Todas" ? "" : $this->search);
			$count_before = ceil($this->sql->count("postagem", array(array("status", 6), array("id", $max, ">"), array("titulo", "%".$like."%", " LIKE "))) / $this->maximo_postagens);
			$count_after = ceil($this->sql->count("postagem", array(array("status", 6), array("id", $min, "<"), array("titulo", "%".$like."%", " LIKE "))) / $this->maximo_postagens);

			if($count_before > 0 || $count_after > 0) // Se não tiver antes e depois nem mostra
			{
				$current = $count_before + 1;

				$page = 1;

				$rtn .= "<div class=\"postagens-pagination\">";
				for($i = 0; $i < $count_before; $i++)
				{
					$rtn .= "<a href=\"".URL."Blog/".$this->search."/".$page."\" class=\"postagens-pagination-link on-click\"><span>".$page++."</span></a>";
				}
				$rtn .= "<a href=\"".URL."Blog/".$this->search."/".$page."\" class=\"postagens-pagination-link on-click active\"><span>".$page++."</span></a>";
				for($i = 0; $i < $count_after; $i++)
				{
					$rtn .= "<a href=\"".URL."Blog/".$this->search."/".$page."\" class=\"postagens-pagination-link on-click\"><span>".$page++."</span></a>";
				}
				$rtn .= "</div>";
			}
		}
		else
		{
			$rtn .= "<div class=\"alert information\">Nenhuma postagem encontrada para a página <b>".$this->pagina."</b> do <b>Blog</b>!</div>";
		}

		return $rtn;
	}
}

?>