<?php

class ControlProdutos extends Control
{
	public $_front;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("front");
		$this->loadClass("categoria.class");

		// Inicia as classes necessárias
		$this->_front = new Front($this);

		// Define menu ativo
		$this->_front->SetActive("Produtos");

		// Deixar esses dois por ultimo
		$this->setHeader("home/header");
		$this->setFooter("home/footer");
	}
}

?>