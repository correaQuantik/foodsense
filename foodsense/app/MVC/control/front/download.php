<?php

class ControlDownload extends Control
{
	public function doActions()
	{
		$arquivo = $this->sql->selectFirst(array("*"), "postagem_arquivo", array(array("id", $this->getRoute(1))));

		if(!!$arquivo)
		{
			set_time_limit(0);

			$path = dirname(__FILE__)."/../../../../files/".$arquivo->arquivo;

			if(file_exists($path))
			{
				if(is_file($path))
				{
					header("Content-Description: File Transfer");
					header("Content-Disposition: attachment; filename=\"".$arquivo->nome."\"");
					header("Content-Type: application/octet-stream");
					header("Content-Transfer-Encoding: binary");
					header("Content-Length: " . filesize($path));
					header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
					header("Pragma: public");
					header("Expires: 0");

					readfile($path);

					exit();
				}
			}

			exit($path);
		}
		else
		{
			exit("...");
		}
	}
}

?>