<?php

class ControlPainelSair extends Control
{
	public $painel;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");

		// Inicia as classes necessárias
		$this->painel = new Painel($this);

		// Verifica se está logado
		if($this->painel->isLogged())
		{
			// Sai da sessão
			$this->painel->logout();

			// Redireciona pro login
			$this->getRoute()->Redirect("Painel/Login");
		}
		else
		{
			$this->getRoute()->Redirect("Painel/Login");
		}
	}
}

?>