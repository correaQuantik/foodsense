<?php

class ControlPainelPostagemComentarios extends Control
{
	public $painel;

	public $_postagem;

	public $id_postagem;
	public $postagem;

	public $categorias;
	public $tags;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");

		// Inicia as classes necessárias
		$this->painel = new Painel($this);

		// Verifica se está logado
		if($this->painel->isLogged())
		{
			// Inclui a classe
			$this->loadClass("postagem");

			// Inicia classes
			$this->_postagem = new Postagem($this);

			// ID
			$this->id_postagem = $this->getRoute(2);

			// Retorna a postagem selecionada
			$this->postagem = $this->_postagem->Mostra($this->id_postagem);

			if(!!$this->postagem)
			{
				// Título da página
				$this->painel->setTitle("Comentários de \"".$this->postagem->titulo."\"");
				$this->painel->setMenuActive("postagem");
				$this->painel->setSubMenuActive("listar");

				// Adiciona as breadcrumbs
				$this->painel->addBreadcrumb("Página Inicial", "Painel/Home", "fa fa-tachometer fa-fw");
				$this->painel->addBreadcrumb("Listar postagens", "Painel/Postagem/Listar");
				$this->painel->addBreadcrumb($this->postagem->titulo, "Painel/Postagem/".$this->id_postagem);

				// Deixar esses dois por ultimo
				$this->setHeader("painel/header");
				$this->setFooter("painel/footer");
			}
			else
			{
				$this->getRoute()->Redirect("Painel/Postagem/Listar");
			}
		}
		else
		{
			$this->getRoute()->Redirect("Painel/Login");
		}
	}

	public function GetComentarios($aprovado)
	{
		$rtn = "";

		$comentarios = $this->sql->selectJoin(array("facebook.nome", "postagem_comentario.id", "postagem_comentario.aprovado", "postagem_comentario.comentario", "postagem_comentario.criado"), "postagem_comentario", array(array("facebook", "facebook.id", "postagem_comentario.idfacebook")), array(array("postagem_comentario.idpostagem", $this->id_postagem), array("postagem_comentario.aprovado", $aprovado)));

		if(is_array($comentarios))
		{
			foreach($comentarios as $row)
			{
				$rtn .= "<tr>
					<td>".$row->nome."</td>
					<td>".$row->comentario."</td>
					<td class=\"text-center\">".date("d/m/Y", strtotime($row->criado))."<br>".date("H:i:s", strtotime($row->criado))."</td>
					<td class=\"text-center\">
						<div class=\"btn-group btn-group-xs\">
							".($aprovado == 0 ? "<button class=\"btn btn-success blog-comment\" data-id=\"".$row->id."\" data-type=\"1\"><i class=\"fa fa-thumbs-o-up fa-fw\"></i></button>" : "")."
							<button class=\"btn btn-danger blog-comment\" data-id=\"".$row->id."\" data-type=\"0\"><i class=\"fa fa-trash-o fa-fw\"></i></button>
						</div>
					</td>
				</tr>";
			}
		}
		else
		{
			$rtn .= "<tr>
				<td colspan=\"4\">Nenhum comentário encontrado</td>
			</tr>";
		}

		return $rtn;
	}
}

?>