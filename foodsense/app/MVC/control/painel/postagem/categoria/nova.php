<?php

class ControlPainelPostagemCategoriaNova extends Control
{
	public $painel;

	public $_postagem;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");

		// Inicia as classes necessárias
		$this->painel = new Painel($this);

		// Verifica se está logado
		if($this->painel->isLogged())
		{
			// Inclui a classe
			$this->loadClass("postagem");

			// Inicia classes
			$this->_postagem = new Postagem($this);

			// Título da página
			$this->painel->setTitle("Nova categoria");
			$this->painel->setMenuActive("postagem");
			$this->painel->setSubMenuActive("nova_categoria");

			// Adiciona as breadcrumbs
			$this->painel->addBreadcrumb("Página Inicial", "Painel/Home", "fa fa-tachometer fa-fw");
			$this->painel->addBreadcrumb("Nova categoria", "Painel/Postagem/Categoria/Nova");

			// Deixar esses dois por ultimo
			$this->setHeader("painel/header");
			$this->setFooter("painel/footer");
		}
		else
		{
			$this->getRoute()->Redirect("Painel/Login");
		}
	}
}

?>