<?php

class ControlPainelPostagemCategoriaVer extends Control
{
	public $painel;

	public $_postagem;

	public $idcategoria;
	public $categoria;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");

		// Inicia as classes necessárias
		$this->painel = new Painel($this);

		// Verifica se está logado
		if($this->painel->isLogged())
		{
			// Inclui a classe
			$this->loadClass("postagem");

			// Inicia classes
			$this->_postagem = new Postagem($this);

			// ID
			$this->idcategoria = $this->getRoute(3);

			// Retorna a categoria selecionada
			$this->categoria = $this->_postagem->MostraCategoria("id", $this->idcategoria);

			if(!!$this->categoria)
			{
				// Título da página
				$this->painel->setTitle("Editar categoria: ".$this->categoria->nome);
				$this->painel->setMenuActive("postagem");
				$this->painel->setSubMenuActive("listar_categoria");

				// Adiciona as breadcrumbs
				$this->painel->addBreadcrumb("Página Inicial", "Painel/Home", "fa fa-tachometer fa-fw");
				$this->painel->addBreadcrumb("Listar categorias", "Painel/Postagem/Categoria/Listar");
				$this->painel->addBreadcrumb($this->categoria->nome, "Painel/Postagem/Categoria/".$this->idcategoria);

				// Deixar esses dois por ultimo
				$this->setHeader("painel/header");
				$this->setFooter("painel/footer");
			}
			else
			{
				$this->getRoute()->Redirect("Painel/Postagem/Listar");
			}
		}
		else
		{
			$this->getRoute()->Redirect("Painel/Login");
		}
	}
}

?>