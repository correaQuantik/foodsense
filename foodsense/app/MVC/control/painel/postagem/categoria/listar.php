<?php

class ControlPainelPostagemCategoriaListar extends Control
{
	public $painel;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");

		// Inicia as classes necessárias
		$this->painel = new Painel($this);

		// Verifica se está logado
		if($this->painel->isLogged())
		{
			// Título da página
			$this->painel->setTitle("Listar categorias");
			$this->painel->setMenuActive("postagem");
			$this->painel->setSubMenuActive("listar_categoria");

			// Adiciona as breadcrumbs
			$this->painel->addBreadcrumb("Página Inicial", "Painel/Home", "fa fa-tachometer fa-fw");
			$this->painel->addBreadcrumb("Listar categorias", "Painel/Postagem/Categoria/Listar");

			// Deixar esses dois por ultimo
			$this->setHeader("painel/header");
			$this->setFooter("painel/footer");
		}
		else
		{
			$this->getRoute()->Redirect("Painel/Login");
		}
	}
}

?>