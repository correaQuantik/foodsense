<?php

class ControlPainelPostagemVer extends Control
{
	public $painel;

	public $_postagem;

	public $idpostagem;
	public $postagem;

	public $url;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");

		// Inicia as classes necessárias
		$this->painel = new Painel($this);

		// Verifica se está logado
		if($this->painel->isLogged())
		{
			// Inclui a classe
			$this->loadClass("postagem");

			// Inicia classes
			$this->_postagem = new Postagem($this);

			// ID
			$this->idpostagem = $this->getRoute(2);

			// Retorna a postagem selecionada
			$this->postagem = $this->_postagem->Mostra($this->idpostagem);

			if(!!$this->postagem)
			{
				if($this->painel->conta->categoria != 1)
				{
					if($this->painel->conta->id != $this->postagem->idconta)
					{
						$this->getRoute()->Redirect("Painel/Postagem/Listar");
					}
				}

				// Define a URL da postagem
				$this->url = URL."Blog/Todas/1/".$this->idpostagem;

				// Título da página
				$this->painel->setTitle("Editar: ".$this->postagem->titulo);
				$this->painel->setMenuActive("postagem");
				$this->painel->setSubMenuActive("listar");

				// Adiciona as breadcrumbs
				$this->painel->addBreadcrumb("Página Inicial", "Painel/Home", "fa fa-tachometer fa-fw");
				$this->painel->addBreadcrumb("Listar postagens", "Painel/Postagem/Listar");
				$this->painel->addBreadcrumb($this->postagem->titulo, "Painel/Postagem/".$this->idpostagem);

				// Deixar esses dois por ultimo
				$this->setHeader("painel/header");
				$this->setFooter("painel/footer");
			}
			else
			{
				$this->getRoute()->Redirect("Painel/Postagem/Listar");
			}
		}
		else
		{
			$this->getRoute()->Redirect("Painel/Login");
		}
	}

	public function AlteraAutor()
	{
		$rtn = "";

		if($this->painel->conta->categoria == 1)
		{
			$contas = $this->sql->select(array("id", "nome"), "conta");

			$rtn .= "<div class=\"form-group\">
				<label for=\"form-autor\"><b>Autor</b></label>
				<select name=\"autor\" id=\"form-autor\" class=\"form-control\">";

				foreach($contas as $conta)
				{
					$rtn .= "<option value=\"".$conta->id."\"".($this->postagem->idconta == $conta->id ? " selected" : "").">".$conta->nome."</option>";
				}

			$rtn .= "</select>
			</div>";
		}

		return $rtn;
	}

	public function MostraStatus()
	{
		$rtn = "";

		if($this->painel->conta->categoria == 1)
		{
			$rtn .= "<div class=\"form-group\">
				<label for=\"form-status\"><b>Status</b></label>
				".$this->_postagem->ListaStatus($this->postagem->status)."
			</div>";
		}

		return $rtn;
	}

	public function getHostorico()
	{
		$rtn = "";

		$historicos = json_decode($this->postagem->historico);

		if(count($historicos) > 0)
		{
			$status = $this->_postagem->Status();

			foreach($historicos as $historico)
			{
				$rtn .= "<tr>
					<td>".$status[$historico[0]]."</td>
					<td>".$historico[1]."</td>
					<td>".date("d/m/Y H:i:s", $historico[2])."</td>
				</tr>";
			}
		}

		if($rtn == "")
		{
			$rtn = "<tr>
				<td colspan=\"99\" class=\"text-center\">Nenhum histórico disponível!</td>
			</tr>";
		}

		return $rtn;
	}

	public function ExibeArquivos()
	{
		$rtn = "";

		if(!!$this->postagem->arquivos)
		{
			$rtn .= "<table class=\"table table-striped table-bordered table-hover table-middle no-margin-bottom\">";

			$rtn .= "<thead>
				<tr>
					<td width=\"1px\"></td>
					<td>Arquivo</td>
				</tr>
			</thead>";

			$rtn .= "<tbody>";
			foreach($this->postagem->arquivos as $val)
			{
				$id = "arquivo-".$val->id;

				$rtn .= "<tr>
					<td>
						<label for=\"".$id."\">
							<div class=\"checkbox no-margin\">
								<input type=\"checkbox\" name=\"arquivos[]\" value=\"".$val->id."\" id=\"".$id."\" class=\"ace hidden\">
								<span class=\"lbl\"></span>
							</div>
						</label>
					</td>

					<td><label for=\"".$id."\">".$val->nome."</label></td>
				</tr>";
			}
			$rtn .= "</tbody>";

			$rtn .= "</table>";
		}

		if($rtn == "")
		{
			$rtn = "<div class=\"alert alert-info\">Nenhum arquivo encontrado para está postagem!</div>";
		}

		return $rtn;
	}

	public function SendButton()
	{
		if($this->painel->conta->categoria == 1)
		{
			return "<button id=\"btn-snd\" class=\"btn btn-warning\">
				<span class=\"msg1\"><i class=\"fa fa-send fa-fw\"></i> Enviar</span>
				<span class=\"msg2 hidden\"><i class=\"fa fa-spinner fa-fw fa-spin\"></i> Enviando...</span>
			</button>";
		}
		else
		{
			if($this->postagem->status == 6)
			{
				return "<button class=\"btn btn-danger\" disabled><i class=\"fa fa-times fa-fw\"></i> Postagem já <b>Habilitada</b></button>";
			}
			else
			{
				return "<button id=\"btn-snd\" class=\"btn btn-warning\">
					<span class=\"msg1\"><i class=\"fa fa-send fa-fw\"></i> Enviar</span>
					<span class=\"msg2 hidden\"><i class=\"fa fa-spinner fa-fw fa-spin\"></i> Enviando...</span>
				</button>";
			}
		}
	}
}

?>