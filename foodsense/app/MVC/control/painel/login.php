<?php

class ControlPainelLogin extends Control
{
	public $painel;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");

		// Inicia as classes necessárias
		$this->painel = new Painel($this);

		// Verifica se está logado
		if($this->painel->isLogged())
		{
			$this->getRoute()->Redirect("Painel/Home");
		}
		else
		{
			// Título da página
			$this->painel->setTitle("Login");

			// Deixar esses dois por ultimo
			$this->setHeader("login/header");
			$this->setFooter("login/footer");
		}
	}
}

?>