<?php

class ControlPainelUsuariosListar extends Control
{
	public $painel;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");

		// Inicia as classes necessárias
		$this->painel = new Painel($this);

		// Verifica se está logado
		if($this->painel->isLogged())
		{
			// Título da página
			$this->painel->setTitle("Listar usuários");
			$this->painel->setMenuActive("usuario");
			$this->painel->setSubMenuActive("listar");

			// Adiciona as breadcrumbs
			$this->painel->addBreadcrumb("Página Inicial", "Painel/Home", "fa fa-home fa-fw");
			$this->painel->addBreadcrumb("Listar usuários", "Painel/Usuarios");

			// Deixar esses dois por ultimo
			$this->setHeader("painel/header");
			$this->setFooter("painel/footer");
		}
		else
		{
			$this->getRoute()->Redirect("Painel/Login");
		}
	}
}

?>