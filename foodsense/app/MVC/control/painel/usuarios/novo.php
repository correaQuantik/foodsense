<?php

class ControlPainelUsuariosNovo extends Control
{
	public $painel;

	public $_c;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");

		// Inicia as classes necessárias
		$this->painel = new Painel($this);

		// Verifica se está logado
		if($this->painel->isLogged())
		{
			// Inclui a classe
			$this->loadClass("conta");

			// Inicia a classe
			$this->_c = new Conta($this);

			// Título da página
			$this->painel->setTitle("Novo usuário");
			$this->painel->setMenuActive("usuario");
			$this->painel->setSubMenuActive("novo");

			// Adiciona as breadcrumbs
			$this->painel->addBreadcrumb("Página Inicial", "Painel/Home", "fa fa-home fa-fw");
			$this->painel->addBreadcrumb("Novo usuário", "Painel/Usuarios/Novo");

			// Deixar esses dois por ultimo
			$this->setHeader("painel/header");
			$this->setFooter("painel/footer");
		}
		else
		{
			$this->getRoute()->Redirect("Painel/Login");
		}
	}
}

?>