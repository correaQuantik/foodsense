<?php

class ControlPainelUsuariosVer extends Control
{
	public $painel;

	public $_c;

	public $id_conta;
	public $conta;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");

		// Inicia as classes necessárias
		$this->painel = new Painel($this);

		// Verifica se está logado
		if($this->painel->isLogged())
		{
			// Inclui a classe
			$this->loadClass("conta");

			// Inicia a classe
			$this->_c = new Conta($this);

			// ID da conta selecionado
			$this->id_conta = $this->getRoute(2);

			// Dados da conta selecionado
			$this->conta = $this->_c->Mostra($this->id_conta);

			if(!!$this->conta)
			{
				// Título da página
				$this->painel->setTitle("Usuário | ".$this->conta->nome);
				$this->painel->setMenuActive("usuario");
				$this->painel->setSubMenuActive("listar");

				// Adiciona as breadcrumbs
				$this->painel->addBreadcrumb("Página Inicial", "Painel/Home", "fa fa-home fa-fw");
				$this->painel->addBreadcrumb("Listar usuários", "Painel/Usuarios");
				$this->painel->addBreadcrumb($this->conta->nome, "Painel/Usuarios/".$this->id_conta);

				// Deixar esses dois por ultimo
				$this->setHeader("painel/header");
				$this->setFooter("painel/footer");
			}
			else
			{
				$this->getRoute()->Redirect("Painel/Usuarios");
			}
		}
		else
		{
			$this->getRoute()->Redirect("Painel/Login");
		}
	}
}

?>