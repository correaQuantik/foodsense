<?php

class ControlPainelUsuariosApagar extends Control
{
	public $painel;
	public $conta;
	public $id_usuario;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");

		// Inicia as classes necessárias
		$this->painel = new Painel($this);

		// Verifica se está logado
		if($this->painel->isLogged())
		{
			// Atribui a conta carregada no painel para facilitar futuros acessos a ela
			$this->conta = $this->painel->getConta();

			// Inclui a classe do usuario
			$this->loadClass("usuario");

			// Inicia a classe usuario
			$this->_u = new Usuario($this);

			// ID do usuario selecionado
			$this->id_usuario = $this->getRoute(2);

			// Dados do usuario selecionado
			$this->usuario = $this->_u->exibir($this->id_usuario);

			if(!!$this->usuario)
			{
				// Título da página
				$this->painel->setTitle("Usuário | ".$this->usuario->nome." | Apagar");
				$this->painel->setMenuActive("usuarios");

				// Adiciona as breadcrumbs
				$this->painel->addBreadcrumb("Página Inicial", "Painel/Home", "fa fa-home fa-fw");
				$this->painel->addBreadcrumb("Usuários", "Painel/Usuarios");
				$this->painel->addBreadcrumb($this->usuario->nome, "Painel/Usuarios/".$this->id_usuario);
				$this->painel->addBreadcrumb("Apagar", "Painel/Usuarios/".$this->id_usuario."/Apagar");

				// Deixar esses dois por ultimo
				$this->setHeader("painel/header");
				$this->setFooter("painel/footer");
			}
			else
			{
				$this->getRoute()->Redirect("Painel/Usuarios");
			}
		}
		else
		{
			$this->getRoute()->Redirect("Painel/Login");
		}
	}
}

?>