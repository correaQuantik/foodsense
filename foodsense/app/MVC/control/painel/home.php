<?php

class ControlPainelHome extends Control
{
	public $painel;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");

		// Inicia as classes necessárias
		$this->painel = new Painel($this);

		// Verifica se está logado
		if($this->painel->isLogged())
		{
			// Título da página
			$this->painel->setTitle("Página Inicial");
			$this->painel->setMenuActive("home");

			// Adiciona as breadcrumbs
			$this->painel->addBreadcrumb("Página Inicial", "Painel/Home", "fa fa-tachometer fa-fw");

			// Deixar esses dois por ultimo
			$this->setHeader("painel/header");
			$this->setFooter("painel/footer");
		}
		else
		{
			$this->getRoute()->Redirect("Painel/Login");
		}
	}

	public function prepareHome()
	{
		$rtn = "";

		if($this->painel->conta->categoria == 1)
			$rtn .= $this->painel->getAniversariantes();

		if($this->painel->conta->categoria != 1)
			$rtn .= $this->getPostagens();

		return $rtn;
	}

	public function getPostagens()
	{
		$rtn = "";

		$rtn .= "<div class=\"widget-box widget-color-blue\">";

		$rtn .= "<div class=\"widget-header\">
			<h5 class=\"widget-title bigger lighter ui-sortable-handle\">Suas postagens</h5>
		</div>";

		$rtn .= "<div class=\"widget-body\">";

		$rtn .= "<table class=\"table table-striped table-bordered table-hover datatable\" data-ajax=\"".URL."Ajax/Datatable/Postagem\">";

		$rtn .= "<thead>
			<tr>
				<th width=\"60px\">ID</th>
				<th width=\"130px\">Status</th>
				<th>Título</th>
				<th width=\"110px\">Pendentes</th>
				<th width=\"100px\">Categorias</th>
				<th width=\"75px\">TAGs</th>
				<th width=\"145px\">Criado</th>
				<th class=\"no-sort\" width=\"30px\"></th>
			</tr>
		</thead>";

		$rtn .= "<tbody>";
		$rtn .= "</tbody>";

		$rtn .= "</table>";

		$rtn .= "</div>";

		$rtn .= "</div>";

		return $rtn;
	}
}

?>