<?php

class ControlPainelTermos extends Control
{
	public $painel;

	public function doActions()
	{
		// Classes que serão usadas
		$this->loadClass("painel");

		// Inicia as classes necessárias
		$this->painel = new Painel($this, true);

		// Verifica se está logado
		if($this->painel->isLogged())
		{
			// Título da página
			$this->painel->setTitle("Termos de uso");

			// Adiciona as breadcrumbs
			$this->painel->addBreadcrumb("Termos de uso", "Painel/Termos", "fa fa-file-text-o fa-fw");

			// Deixar esses dois por ultimo
			$this->setHeader("painel/header");
			$this->setFooter("painel/footer");
		}
		else
		{
			$this->getRoute()->Redirect("Painel/Login");
		}
	}
}

?>