<?php

class ControlExtra extends Control
{
	public function doActions()
	{
		$type = $this->getPath(0);
		$place = $this->getPath(1);
		$file = $this->getPath(2);

		$this->setReplace(false);

		if($place == "painel")
		{
			// Classes que serão usadas
			$this->loadClass("painel");

			// Inicia as classes necessárias
			$this->painel = new Painel($this, true);

			if($this->painel->isLogged())
			{
				// Altera o arquivo a ser chamado
				$this->setView(FOLDER_INCLUDE.$place."/".$type."/".$this->getPath(2).".min.".$type);
			}
		}
		else
		{
			// Altera o arquivo a ser chamado
			$this->setView(FOLDER_INCLUDE.$place."/".$type."/".$this->getPath(2).".min.".$type);
		}

		if($type == "css")
		{
			// Modifica o header
			$this->addPHPHeader("Content-type: text/css");
		}
		else if($type == "js")
		{
			// Modifica o header
			$this->addPHPHeader("Content-type: application/x-javascript");
		}
	}
}

?>