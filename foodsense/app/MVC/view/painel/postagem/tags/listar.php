<div class="row">
	<div class="col-xs-12">
		<div class="widget-box widget-color-blue">
			<div class="widget-body">
				<table class="table table-striped table-bordered table-hover datatable" data-ajax="<?= URL."Ajax/Datatable/Postagem/TAGs"; ?>">
					<thead>
						<tr>
							<th width="80px">ID</th>
							<th>TAG</th>
							<th class="no-sort" width="100px">Postagens</th>
							<th class="no-sort" width="30px"></th>
						</tr>
					</thead>

					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>