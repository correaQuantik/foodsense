<div class="row">
	<div class="col-xs-12">
		<form action="<?= URL."Ajax/Postagem/Nova"; ?>">
			<?= $this->MostraStatus(); ?>

			<div class="form-group">
				<label for="form-categorias"><b>Categoria</b></label>
				<select name="categorias[]" id="form-categorias" class="chosen hidden" multiple><?= $this->_postagem->ListarCategoriasSelect(); ?></select>
			</div>

			<div class="form-group">
				<label><b>TAGs</b></label>
				<input type="text" name="tags" class="tags hidden">
			</div>

			<div class="form-group">
				<label for="form-titulo"><span class="red">*</span> <b>Título</b></label>
				<input type="text" name="titulo" id="form-titulo" class="form-control">
			</div>

			<div class="form-group">
				<label for="form-conteudo"><span class="red">*</span> <b>Conteúdo</b></label>
				<textarea name="conteudo" id="form-conteudo" class="ckeditor hidden"></textarea>
			</div>

			<div class="form-group">
				<label for="form-youtube"><b>Youtube</b></label>
				<input type="text" name="youtube" id="form-youtube" class="form-control">
			</div>

			<div class="form-group">
				<label for="form-capa"><b>Capa da publicação</b></label>
				<div class="input-group">
					<span class="input-group-btn">
						<label class="btn btn-purple">
							<i class="fa fa-upload fa-fw"></i>
							<input type="file" name="capa" id="form-capa" class="hidden">
						</label>
					</span>
					<input type="text" class="form-control" placeholder="Selecione as fotos que deseja enviar" disabled="disabled">
				</div>
			</div>

			<div class="form-group">
				<label for="form-galeria"><b>Galeria de fotos</b></label>
				<div class="input-group">
					<span class="input-group-btn">
						<label class="btn btn-purple">
							<i class="fa fa-upload fa-fw"></i>
							<input type="file" name="galeria[]" id="form-galeria" class="hidden" multiple="multiple">
						</label>
					</span>
					<input type="text" class="form-control" placeholder="Selecione as fotos que deseja enviar" disabled="disabled">
				</div>
			</div>

			<div class="form-group">
				<label for="form-observacoes"><b>Observações</b></label>
				<textarea name="observacoes" id="form-observacoes" class="ckeditor hidden"></textarea>
			</div>

			<div class="form-group">
				<label for="form-arquivos"><b>Arquivos para download</b></label>
				<div class="input-group">
					<span class="input-group-btn">
						<label class="btn btn-purple">
							<i class="fa fa-upload fa-fw"></i>
							<input type="file" name="arquivos[]" id="form-arquivos" class="hidden" multiple="multiple">
						</label>
					</span>
					<input type="text" class="form-control" placeholder="Selecione as fotos que deseja enviar" disabled="disabled">
				</div>
			</div>

			<div class="text-right">
				<button id="btn-snd" class="btn btn-primary">
					<span class="msg1"><i class="fa fa-send fa-fw"></i> Enviar</span>
					<span class="msg2 hidden"><i class="fa fa-spinner fa-fw fa-spin"></i> Enviando...</span>
				</button>
			</div>
		</form>
	</div>
</div>