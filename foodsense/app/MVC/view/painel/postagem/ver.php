<div class="row">
	<div class="col-xs-12">
		<div class="form-group">
			<label><b>URL da publicação</b></label>
			<div class="form-control"><a href="<?= $this->url; ?>" target="_blank"><?= $this->url; ?></a></div>
		</div>

		<form action="<?= URL."Ajax/Postagem/Editar/".$this->idpostagem; ?>">
			<?= $this->AlteraAutor(); ?>

			<?= $this->MostraStatus(); ?>

			<div class="form-group">
				<label for="form-categoria"><b>Categoria</b></label>
				<select name="categorias[]" id="form-categorias" class="chosen hidden" multiple><?= $this->_postagem->PostagemCategorias($this->postagem); ?></select>
			</div>

			<div class="form-group">
				<label><b>TAGs</b></label>
				<input type="text" name="tags" class="tags hidden" value="<?= $this->_postagem->PostagemTAGs($this->postagem); ?>">
			</div>

			<div class="form-group">
				<label for="form-titulo"><span class="red">*</span> <b>Título</b></label>
				<input type="text" name="titulo" id="form-titulo" class="form-control" value="<?= $this->postagem->titulo; ?>">
			</div>

			<div class="form-group">
				<label for="form-conteudo"><span class="red">*</span> <b>Conteúdo</b></label>
				<textarea name="conteudo" id="form-conteudo" class="ckeditor hidden"><?= $this->postagem->conteudo; ?></textarea>
			</div>

			<div class="form-group">
				<label for="form-youtube"><b>Youtube</b></label>
				<input type="text" name="youtube" id="form-youtube" class="form-control" value="<?= $this->postagem->youtube; ?>">
			</div>

			<div class="form-group">
				<label for="form-capa"><b>Capa da publicação</b></label>
				<div class="input-group">
					<span class="input-group-btn">
						<label class="btn btn-purple">
							<i class="fa fa-upload fa-fw"></i>
							<input type="file" name="capa" id="form-capa" class="hidden">
						</label>
					</span>
					<input type="text" class="form-control" placeholder="Selecione as fotos que deseja enviar" disabled="disabled">
				</div>
			</div>

			<div class="form-group">
				<label for="form-galeria"><b>Galeria de fotos</b></label>
				<div class="input-group">
					<span class="input-group-btn">
						<label class="btn btn-purple">
							<i class="fa fa-upload fa-fw"></i>
							<input type="file" name="galeria[]" id="form-galeria" class="hidden" multiple="multiple">
						</label>
					</span>
					<input type="text" class="form-control" placeholder="Selecione as fotos que deseja enviar" disabled="disabled">
				</div>
			</div>

			<div class="form-group">
				<label><b>Selecione as fotos que deseja apagar</b></label>
				<?= $this->_postagem->ExibeFotos($this->postagem); ?>
			</div>

			<div class="form-group">
				<label for="form-observacoes"><b>Observações</b></label>
				<textarea name="observacoes" id="form-observacoes" class="form-control"><?= $this->postagem->observacoes; ?></textarea>
			</div>

			<div class="form-group">
				<label><b>Histórico</b></label>
				<table class="table table-striped table-bordered table-hover no-margin table-middle">
					<thead>
						<tr>
							<th width="180px">Status</th>
							<th>Observação</th>
							<th width="150px">Data</th>
						</tr>
					</thead>

					<tbody><?= $this->getHostorico(); ?></tbody>
				</table>
			</div>

			<div class="form-group">
				<label for="form-arquivos"><b>Arquivos para download</b></label>
				<div class="input-group">
					<span class="input-group-btn">
						<label class="btn btn-purple">
							<i class="fa fa-upload fa-fw"></i>
							<input type="file" name="arquivos[]" id="form-arquivos" class="hidden" multiple="multiple">
						</label>
					</span>
					<input type="text" class="form-control" placeholder="Selecione as fotos que deseja enviar" disabled="disabled">
				</div>
			</div>

			<div class="form-group">
				<label><b>Selecione os arquivos que deseja apagar</b></label>
				<?= $this->ExibeArquivos(); ?>
			</div>

			<div class="row">
				<div class="col-xs-6">
					<a href="<?= URL."Painel/Postagem/Listar"; ?>" class="btn btn-primary"><i class="fa fa-chevron-left fa-fw"></i> <span class="hidden-xs">Voltar</span></a>
				</div>

				<div class="col-xs-6 text-right"><?= $this->SendButton(); ?></div>
			</div>
		</form>
	</div>
</div>