<div class="row">
	<div class="col-xs-12">
		<div class="widget-box widget-color-blue">
			<div class="widget-header">
				<h5 class="widget-title bigger lighter ui-sortable-handle">Comentários não aprovados</h5>
			</div>

			<div class="widget-body">
				<table class="table table-striped table-bordered table-hover table-middle no-margin">
					<thead>
						<tr>
							<th width="150px">Quem comentou</th>
							<th>Comentário</th>
							<th width="100px" class="text-center">Enviao em</th>
							<th width="90px" class="text-center">Ações</th>
						</tr>
					</thead>

					<tbody><?= $this->GetComentarios(0); ?></tbody>
				</table>
			</div>
		</div>

		<div class="widget-box widget-color-blue">
			<div class="widget-header">
				<h5 class="widget-title bigger lighter ui-sortable-handle">Comentários aprovados</h5>
			</div>

			<div class="widget-body">
				<table class="table table-striped table-bordered table-hover table-middle no-margin">
					<thead>
						<tr>
							<th width="150px">Quem comentou</th>
							<th>Comentário</th>
							<th width="100px" class="text-center">Enviao em</th>
							<th width="90px" class="text-center">Ações</th>
						</tr>
					</thead>

					<tbody><?= $this->GetComentarios(1); ?></tbody>
				</table>
			</div>
		</div>
	</div>
</div>