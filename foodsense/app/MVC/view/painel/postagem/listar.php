<div class="row">
	<div class="col-xs-12">
		<div class="widget-box widget-color-blue">
			<div class="widget-body">
				<table class="table table-striped table-bordered table-hover table-middle datatable" data-ajax="<?= URL."Ajax/Datatable/Postagem"; ?>">
					<thead>
						<tr>
							<th width="60px">ID</th>
							<th width="180px">Status</th>
							<th>Título</th>
							<th width="170px">Novos comentários</th>
							<th class="no-sort" width="30px"></th>
						</tr>
					</thead>

					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>