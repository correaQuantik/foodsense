<div class="row">
	<div class="col-xs-12">
		<form action="<?= URL."Ajax/Postagem/Categoria/Editar/".$this->idcategoria; ?>">
			<div class="form-group">
				<label for="form-nome"><span class="red">*</span> <b>Nome</b></label>
				<input type="text" name="nome" id="form-nome" class="form-control" value="<?= $this->categoria->nome; ?>">
			</div>

			<div class="row">
				<div class="col-xs-6">
					<a href="<?= URL."Painel/Postagem/Categoria/Listar"; ?>" class="btn btn-primary"><i class="fa fa-chevron-left fa-fw"></i> <span class="hidden-xs">Voltar</span></a>
				</div>

				<div class="col-xs-6 text-right">
					<button id="btn-snd" class="btn btn-warning">
						<span class="msg1"><i class="fa fa-send fa-fw"></i> Enviar</span>
						<span class="msg2 hidden"><i class="fa fa-spinner fa-fw fa-spin"></i> Enviando...</span>
					</button>
				</div>
			</div>
		</form>
	</div>
</div>