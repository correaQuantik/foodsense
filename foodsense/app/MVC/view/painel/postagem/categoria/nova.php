<div class="row">
	<div class="col-xs-12">
		<form action="<?= URL."Ajax/Postagem/Categoria/Nova"; ?>">
			<div class="form-group">
				<label for="form-nome"><span class="red">*</span> <b>Nome</b></label>
				<input type="text" name="nome" id="form-nome" class="form-control">
			</div>

			<div class="text-right">
				<button id="btn-snd" class="btn btn-primary">
					<span class="msg1"><i class="fa fa-send fa-fw"></i> Enviar</span>
					<span class="msg2 hidden"><i class="fa fa-spinner fa-fw fa-spin"></i> Enviando...</span>
				</button>
			</div>
		</form>
	</div>
</div>