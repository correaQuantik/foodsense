<div class="row">
	<div class="col-xs-12">
		<form action="<?= URL."Ajax/Perfil"; ?>">
			<div class="form-group">
				<label for="form-senha"><b>Senha</b></label>
				<input type="password" name="senha" id="form-senha" class="form-control">
			</div>

			<div class="form-group">
				<label for="form-email"><b>E-mail</b></label>
				<input type="email" name="email" id="form-email" class="form-control" value="<?= $this->painel->conta->email; ?>">
			</div>

			<div class="form-group">
				<label for="form-nome"><b>Nome</b></label>
				<input type="text" name="nome" id="form-nome" class="form-control" value="<?= $this->painel->conta->nome; ?>">
			</div>

			<div class="form-group">
				<label for="form-nascimento"><b>Nascimento</b></label>
				<input type="text" name="nascimento" id="form-nascimento" class="form-control mask datepicker" value="<?= $this->painel->conta->aniversario; ?>" mask="99/99/9999">
			</div>

			<div class="form-group">
				<label for="form-foto"><b>Foto</b></label>
				<div class="input-group">
					<span class="input-group-btn">
						<label class="btn btn-purple">
							<i class="fa fa-upload fa-fw"></i>
							<input type="file" name="foto" id="form-foto" class="hidden">
						</label>
					</span>
					<input type="text" class="form-control" placeholder="Selecione a foto de perfil" disabled="disabled">
				</div>
			</div>

			<div class="form-group">
				<label for="form-sobre"><b>Sobre</b></label>
				<textarea name="sobre" id="form-sobre" class="form-control ckeditor"><?= $this->painel->conta->sobre; ?></textarea>
			</div>

			<div class="form-group">
				<label for="form-profissao"><b>Profissão</b></label>
				<input type="text" name="profissao" id="form-profissao" class="form-control" value="<?= $this->painel->conta->profissao; ?>">
			</div>

			<div class="form-group">
				<label for="form-telefone"><b>Telefone</b></label>
				<input type="text" name="telefone" id="form-telefone" class="form-control" value="<?= $this->painel->conta->telefone; ?>">
			</div>

			<div class="form-group">
				<label for="form-link"><b>Link</b></label>
				<input type="text" name="link" id="form-link" class="form-control" value="<?= $this->painel->conta->link; ?>">
			</div>

			<div class="row">
				<div class="col-xs-6">
					<a href="<?= URL."Painel/Usuarios/Listar"; ?>" class="btn btn-primary"><i class="fa fa-chevron-left fa-fw"></i><span class="hidden-xs"> Voltar</span></a>
				</div>

				<div class="col-xs-6 text-right">
					<button id="btn-snd" class="btn btn-warning">
						<span class="msg1"><i class="fa fa-send fa-fw"></i> Enviar</span>
						<span class="msg2 hidden"><i class="fa fa-spinner fa-fw fa-spin"></i> Enviando...</span>
					</button>
				</div>
			</div>
		</form>
	</div>
</div>