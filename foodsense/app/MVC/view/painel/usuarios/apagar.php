<div class="row">
	<div class="col-xs-12">
		<div class="alert alert-danger">Tem certeza de que deseja apagar o usuário <b><?= $this->usuario->nome; ?></b></div>

		<div class="row">
			<div class="col-xs-6">
				<a href="<?= URL."Painel/Usuarios/".$this->id_usuario; ?>" class="btn btn-info"><i class="fa fa-chevron-left fa-fw"></i><span class="hidden-xs"> Voltar</span></a>
			</div>

			<div class="col-xs-6 text-right">
				<button id="btn-snd" class="btn btn-danger apagar" data-url="<?= URL."Ajax/Usuario/".$this->id_usuario."/Apagar"; ?>"><i class="fa fa-trash-o fa-fw"></i><span class="hidden-xs"> Apagar</span></button>
			</div>
		</div>
	</div>
</div>