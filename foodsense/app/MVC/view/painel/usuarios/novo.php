<div class="row">
	<div class="col-xs-12">
		<form action="<?= URL."Ajax/Usuario/Novo"; ?>">
			<div class="form-group">
				<label for="form-categoria"><b>Categoria</b></label>
				<?= $this->_c->ListaCategorias(0); ?>
			</div>

			<div class="form-group">
				<label for="form-login"><b>Login</b></label>
				<input type="text" name="login" id="form-login" class="form-control">
			</div>

			<div class="form-group">
				<label for="form-senha"><b>Senha</b></label>
				<input type="password" name="senha" id="form-senha" class="form-control">
			</div>

			<div class="form-group">
				<label for="form-email"><b>E-mail</b></label>
				<input type="email" name="email" id="form-email" class="form-control">
			</div>

			<div class="form-group">
				<label for="form-nome"><b>Nome</b></label>
				<input type="text" name="nome" id="form-nome" class="form-control">
			</div>

			<div class="form-group">
				<label for="form-nascimento"><b>Nascimento</b></label>
				<input type="text" name="nascimento" id="form-nascimento" class="form-control mask datepicker" mask="99/99/9999">
			</div>

			<div class="form-group">
				<label for="form-foto"><b>Foto</b></label>
				<div class="input-group">
					<span class="input-group-btn">
						<label class="btn btn-purple">
							<i class="fa fa-upload fa-fw"></i>
							<input type="file" name="foto" id="form-foto" class="hidden">
						</label>
					</span>
					<input type="text" class="form-control" placeholder="Selecione a foto de perfil" disabled="disabled">
				</div>
			</div>

			<div class="form-group">
				<label for="form-sobre"><b>Sobre</b></label>
				<textarea name="sobre" id="form-sobre" class="form-control ckeditor"></textarea>
			</div>

			<div class="form-group">
				<label for="form-profissao"><b>Profissão</b></label>
				<input type="text" name="profissao" id="form-profissao" class="form-control">
			</div>

			<div class="form-group">
				<label for="form-telefone"><b>Telefone</b></label>
				<input type="text" name="telefone" id="form-telefone" class="form-control">
			</div>

			<div class="form-group">
				<label for="form-link"><b>Link</b></label>
				<input type="text" name="link" id="form-link" class="form-control">
			</div>

			<div class="text-right">
				<button id="btn-snd" class="btn btn-primary">
					<span class="msg1"><i class="fa fa-send fa-fw"></i> Enviar</span>
					<span class="msg2 hidden"><i class="fa fa-spinner fa-fw fa-spin"></i> Enviando...</span>
				</button>
			</div>
		</form>
	</div>
</div>