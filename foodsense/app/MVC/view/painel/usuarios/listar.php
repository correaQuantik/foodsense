<div class="row">
	<div class="col-xs-12">
		<?= $this->painel->getAniversariantes(); ?>

		<div class="widget-box widget-color-blue">
			<div class="widget-body">
				<table class="table table-striped table-bordered table-hover datatable" data-ajax="<?= URL."Ajax/Datatable/Usuarios"; ?>">
					<thead>
						<tr>
							<th width="80px">ID</th>
							<th>Nome</th>
							<th width="120px">Categoria</th>
							<th width="110px">Postagens</th>
							<th width="120px">Profissão</th>
							<th width="130px">Aniversário</th>
							<th class="no-sort" width="30px"></th>
						</tr>
					</thead>

					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>