<div class="row">
	<div class="col-sm-10 col-sm-offset-1">
		<div class="login-container">
			<div class="logo text-center">
				<img src="<?= URL."img/icon.png"; ?>">
			</div>

			<div class="position-relative">
				<div class="login-box visible widget-box no-border">
					<div class="widget-body">
						<div class="widget-main">
							<h4 class="header blue lighter bigger text-center">Faça login para acessar o painel</h4>

							<form action="<?= URL."Ajax/Login"; ?>" id="login">
								<div class="form-group">
									<span class="block input-icon input-icon-right">
										<input type="text" name="login" class="form-control" placeholder="Nome de usuário">
										<i class="ace-icon fa fa-envelope-o fa-fw"></i>
									</span>
								</div>

								<div class="form-group">
									<span class="block input-icon input-icon-right">
										<input type="password" name="senha" class="form-control" placeholder="Senha">
										<i class="ace-icon fa fa-unlock-alt fa-fw"></i>
									</span>
								</div>

								<div class="text-right">
									<button class="btn btn-primary btn-sm"><i class="ace-icon fa fa-sign-in fa-fw"></i> Entrar</button>
								</div>
							</form>
						</div>

						<div class="toolbar clearfix">
							<div>
								<a href="<?= URL."Home"; ?>" class="white"><i class="ace-icon fa fa-chevron-left fa-fw"></i> Página Inicial</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>