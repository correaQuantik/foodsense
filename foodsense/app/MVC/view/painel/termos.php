<div class="row">
	<div class="col-xs-12">
		<form action="<?= URL."Ajax/Termos"; ?>" id="login">
			<div class="form-group">
				<ul>
					<li class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque convallis, erat sed ultrices vestibulum, massa mauris malesuada libero, sed iaculis libero turpis quis tortor. Nam maximus luctus purus eu interdum. Vestibulum id lacinia ipsum, sed posuere tortor. Vestibulum nibh risus, mattis eu nulla eget, scelerisque lacinia turpis. Phasellus sit amet urna aliquam, blandit dolor nec, aliquet erat. Nullam malesuada erat non dolor ultrices, at semper augue lobortis. Phasellus ac justo mi. Ut sed lobortis nulla. Morbi ante velit, vulputate porta mollis sed, ornare eu odio.<div>&nbsp;</div></li>

					<li class="text-justify">Maecenas efficitur augue sem, vitae convallis odio efficitur sed. Fusce viverra elit non pellentesque iaculis. Suspendisse dictum vel ligula id sollicitudin. Praesent in odio vitae diam ultricies euismod vel a velit. Praesent placerat justo a odio vulputate, et sodales elit gravida. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel magna aliquam, facilisis risus ut, dapibus felis. Vivamus ornare imperdiet nibh, in auctor sem fringilla ac. Quisque vehicula nunc vitae nunc porta auctor a eu elit. Phasellus scelerisque cursus libero a ornare. Vestibulum dignissim interdum libero, a volutpat sem. Nam pharetra vitae nisl sed varius. Aliquam et tellus tellus. Phasellus sem lacus, tristique in nisi in, aliquet tristique ex. Aliquam erat volutpat.<div>&nbsp;</div></li>

					<li class="text-justify">Vestibulum sollicitudin pellentesque augue ac mollis. Ut sit amet mattis lorem. Sed aliquam suscipit pellentesque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus est mauris, aliquet et dolor non, vulputate fermentum purus. Praesent posuere magna id ligula bibendum pellentesque. Nunc at risus eget odio consequat aliquam. Aliquam rutrum suscipit elit vitae aliquet. Sed euismod tristique gravida. Suspendisse ultricies condimentum orci et convallis. Suspendisse ut consequat tortor, non mattis eros. Cras feugiat, libero eu gravida ultricies, mauris nunc gravida nisl, at placerat nisi tellus feugiat diam. Nunc semper velit imperdiet, tincidunt ante in, facilisis ex. Nullam a dapibus orci, sed tristique turpis. Nunc vestibulum a mauris eget aliquet. Integer id odio dapibus, maximus enim sed, auctor enim.<div>&nbsp;</div></li>

					<li class="text-justify">Cras imperdiet pulvinar pulvinar. Mauris consequat mauris posuere sodales varius. Phasellus vel sodales nisi, vitae tristique dui. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque venenatis venenatis ipsum finibus efficitur. Morbi tristique turpis in dui feugiat ullamcorper. Praesent lobortis gravida metus, eu laoreet diam efficitur quis. Pellentesque facilisis neque turpis, eu dapibus nulla consequat in. Quisque vitae fringilla massa. Nulla pharetra lectus eget libero fermentum, sollicitudin mattis mauris elementum. Maecenas at massa non massa lacinia pharetra. Donec sit amet libero maximus mi tempus blandit. Vivamus sapien dolor, pretium pharetra facilisis tempor, vestibulum et diam. Aenean mattis feugiat nulla. Nulla pulvinar ante id mauris imperdiet, et viverra leo lobortis.<div>&nbsp;</div></li>

					<li class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas convallis purus tortor. Pellentesque facilisis quis nisl in bibendum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin dignissim vulputate nibh, vel porta lectus pellentesque vel. Proin sed pharetra tellus. Donec vitae suscipit enim. Ut laoreet est et sem cursus, non viverra arcu blandit. Phasellus et dolor a lacus molestie pellentesque.</li>
				</ul>
			</div>

			<div class="form-group">
				<div class="checkbox">
					<label>
						<input name="confirmacao" type="checkbox" value="1" class="ace" required="required">
						<span class="lbl"> Eu aceito os termos e condições para o uso do sistema.</span>
					</label>
				</div>
			</div>

			<div class="text-right">
				<button class="btn btn-primary">Continuar</button>
			</div>
		</form>
	</div>
</div>