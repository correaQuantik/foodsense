<div class="header">
	<div class="header-slider owl-carousel">
		<div class="header-slide"><img src="<?= URL."img/header-1.jpg"; ?>"></div>
		<div class="header-slide"><img src="<?= URL."img/header-2.jpg"; ?>"></div>
	</div>

	<div class="container">
		<div class="header-logo"><img src="<?= URL."img/header-logo.png"; ?>"></div>

		<div class="header-action"><img src="<?= URL."img/header-action.png"; ?>"></div>
	</div>
</div>

<div class="header-dots owl-dots">
	<div class="container"></div>
</div>

<div class="content">
	<div class="container">
		<div class="content-title">Proposta e Diferenciais</div>

		<style type="text/css">
			h2 {
				font-size: 20px;
				margin: 0 0 20px 5px;
			}
			h3 {
				font-size: 16px;
				margin: 0 0 15px 15px;
			}
			h4 {
				font-size: 14px;
				margin: 0 0 10px 25px;
				font-weight: normal;
			}
			p {
				text-align: justify;
				margin-bottom: 20px;
			}
		</style>

		<h2>Espaço Físico e Ambiente</h2>

		<p>Morbi a metus. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Nullam sapien sem, ornare ac, nonummy non, lobortis a, enim. Nunc tincidunt ante vitae massa. Duis ante orci, molestie vitae, vehicula venenatis, tincidunt ac, pede. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo. Etiam commodo dui eget wisi. Donec iaculis gravida nulla. Donec quis nibh at felis congue commodo. Etiam bibendum elit eget erat.</p>

			<h3>Localização, Telefone e Endereço</h3>

			<h3>Fotos do Local</h3>

		<h2>Conveniência</h2>

			<h3>Sem taxa de Serviço no local</h3>

 				<p>In sem justo, commodo ut, suscipit at, pharetra vitae, orci. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam id dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos.</p>

 			<h3>Rapidez de Entrega/Delivery</h3>

 				<p>In sem justo, commodo ut, suscipit at, pharetra vitae, orci. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam id dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos.</p>

		<h2>Atendimento</h2>

			<h3>Conhecimento técnico/Equipe treinada</h3>

			<h3>Cortesia e Educação</h3>

			<h3>Rapidez no Atendimento</h3>

		<h2>Diferenciais do Produto</h2>

			<h3>Tecnologia para garantir qualidade</h3>

			<h3>Ingredientes sem conservantes ou aditivos</h3>

			<h3>Embalagem selada e livres de BPA</h3>

 		<p>Maecenas ipsum velit, consectetuer eu, lobortis ut, dictum at, dui. In rutrum. Sed ac dolor sit amet purus malesuada congue. In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet sapien wisi sed libero. Suspendisse sagittis ultrices augue. Mauris metus. Nunc dapibus tortor vel mi dapibus sollicitudin. Etiam posuere lacus quis dolor. Praesent id justo in neque elementum ultrices. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. In convallis. Fusce suscipit libero eget elit. Praesent vitae arcu tempor neque lacinia pretium. Morbi imperdiet, mauris ac auctor dictum, nisl ligula egestas nulla, et sollicitudin sem purus in lacus.</p>

		<h2>Conceito Diferenciado</h2>

			<h3>Planejamento de Cardápio</h3>

				<h4>Cadastre-se e obtenha vantagens</h4>

				<h4>Programe sua dieta</h4>

				<h4>Faça seu pedido semanal</h4>

			<h3>Personalização na montagem dos pratos</h3>

				<h4>Poder de escolha</h4>

			<h3>Utilização de ingredientes funcionais</h3>

			<h3>Contagem de calorias</h3>
	</div>
</div>

<div class="leaf-side">
	<img src="<?= URL."img/leaf-side.png"; ?>">
</div>