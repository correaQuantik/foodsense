<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<style type="text/css">
body * {
	font-size: 14px;
}
.descrip {
	font-size: 0.8em;
}
.Box {
	margin:10px 0;
	padding:5px;
	/*border-bottom:solid 2px #BBB;*/
	border: solid 1px #DDDDDD;
	border-radius: 4px;

	box-shadow: 0px 2px 5px #888;
}

.ForcedWidth {
	width:100%!important;
	padding-top: 10px;
}
.ForcedWidth > div:first-child span {
	font-size: 1.4em;
}

.Box input[type='checkbox'] {
	margin: 0 5px;
	border-radius: 4px;
}
.Box > div {
	padding: 0px;
}
#categoriaSelect {
	margin: 20px 0;
}
.navbar {
	margin-bottom: 0px;
}
.customCheck {
	margin-bottom: 10px;
	border-bottom: 1px solid #E5E5E5;
}
</style>
<div class="header">
	<div class="header-slider owl-carousel">
		<div class="header-slide"><img src="<?= URL."img/header-1.jpg"; ?>"></div>
		<div class="header-slide"><img src="<?= URL."img/header-2.jpg"; ?>"></div>
	</div>

	<div class="container">
		<div class="header-logo"><img src="<?= URL."img/header-logo.png"; ?>"></div>

		<div class="header-action"><img src="<?= URL."img/header-action.png"; ?>"></div>
	</div>
</div>

<div class="header-dots owl-dots">
	<div class="container"></div>
</div>

<div class="leaf-side">
	<img src="<?= URL."img/leaf-side.png"; ?>">
</div>

<div class="content">
	<div class="container">
		<div class="content-title">MONTE O SEU PRATO</div>

	<?php $this->Populate(); ?>
	
	</div>
</div>