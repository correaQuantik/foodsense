<div class="header">
	<div class="header-slider owl-carousel">
		<div class="header-slide"><img src="<?= URL."img/header-1.jpg"; ?>"></div>
		<div class="header-slide"><img src="<?= URL."img/header-2.jpg"; ?>"></div>
	</div>

	<div class="container">
		<div class="header-logo"><img src="<?= URL."img/header-logo.png"; ?>"></div>

		<div class="header-action"><img src="<?= URL."img/header-action.png"; ?>"></div>
	</div>
</div>

<div class="header-dots owl-dots">
	<div class="container"></div>
</div>

<div class="content">
	<div class="container">
		<div class="content-title">Qual é o seu <b>estilo</b>?</div>

		<style type="text/css">
			p {
				text-align: justify;
				margin-bottom: 20px;
			}
			.imgHolder {

			}
			.imgHolder > img {
				margin: 20px 0 20px 35px;
			}
			.imgHolder:nth-child(even) img {
				display: block;
				margin: auto 35px 20px auto;
			}

			@media screen and (max-width: 600px) {
				.imgHolder > img {
					display: block;
					margin: auto;
					margin-bottom: 10px;
				}
			}
		</style>

		<div class="imgs">
			<div class="imgHolder">
				<img src="<?= URL."img/estilo-1.png"; ?>">
				<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
			</div>
			<div class="imgHolder">
				<img src="<?= URL."img/estilo-2.png"; ?>">
				<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
			</div>
			<div class="imgHolder">
				<img src="<?= URL."img/estilo-3.png"; ?>">
				<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
			</div>
			<div class="imgHolder">
				<img src="<?= URL."img/estilo-4.png"; ?>">
				<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
			</div>
			<div class="imgHolder">
				<img src="<?= URL."img/estilo-5.png"; ?>">
				<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
			</div>
			<div class="imgHolder">
				<img src="<?= URL."img/estilo-6.png"; ?>">
				<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
			</div>
			<div class="imgHolder">
				<img src="<?= URL."img/estilo-7.png"; ?>">
				<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
			</div>
			<div class="imgHolder">
				<img src="<?= URL."img/estilo-8.png"; ?>">
				<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
			</div>
		</div>
	</div>
</div>

<div class="leaf-side">
	<img src="<?= URL."img/leaf-side.png"; ?>">
</div>