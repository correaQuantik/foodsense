<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<style type="text/css">
.Box {
	padding:10px;
	border: solid 1px #DDDDDD;
	border-radius: 4px;

	box-shadow: 0px 2px 5px #888;

	margin-bottom: 40px;

	height: 100%;
}
.Box ul li {
	list-style: none;
}
.Box ul li a {
	margin-bottom: 10px;
}
.Box h3 {
	margin-top: 0;
	text-align: center;
}
.btn {
	border-radius: 0;
}
.col-md-3 {
	margin-bottom: 20px;
}
.col-md-12 {
	width:100%;
}
.navbar{
	margin-bottom: 0;
}
.Product img {
	width: 100%;
}
@media screen and (max-width: 600px) {
	.Product a {
		height: 50px;
		font-size: 15px;
	}
}
</style>
<div class="header">
	<div class="header-slider owl-carousel">
		<div class="header-slide"><img src="<?= URL."img/header-1.jpg"; ?>"></div>
		<div class="header-slide"><img src="<?= URL."img/header-2.jpg"; ?>"></div>
	</div>

	<div class="container">
		<div class="header-logo"><img src="<?= URL."img/header-logo.png"; ?>"></div>

		<div class="header-action"><img src="<?= URL."img/header-action.png"; ?>"></div>
	</div>
</div>

<div class="header-dots owl-dots">
	<div class="container"></div>
</div>

<div class="leaf-side">
	<img src="<?= URL."img/leaf-side.png"; ?>">
</div>

<div class="content">
	<div class="container">
		<div class="content-title">PRODUTOS</div>

		<!-- Pratos prontos -->
		<div class="col-md-3 Box">
			<h3>Opções</h3>
			<ul>
				<li><a href="#" class="btn btn-default col-md-12">Smart Salad</a></li>
				<li><a href="#" class="btn btn-default col-md-12">Smart Kids</a></li>
				<li><a href="#" class="btn btn-default col-md-12">Smart Baby</a></li>
				<li><a href="#" class="btn btn-default col-md-12">Smart Sense</a></li>
				<li><a href="#" class="btn btn-default col-md-12">Smart Training</a></li>
				<li><a href="#" class="btn btn-default col-md-12">Smart Veg</a></li>
				<li><a href="#" class="btn btn-default col-md-12">Smart Gourmet</a></li>
				<li><a href="Montar" class="btn btn-warning col-md-12">Monte o seu prato</a></li>
			</ul>
		</div>

		<!-- Mostra de pratos -->
		<div class="col-md-9">

			<?php
				for ($i = 0; $i < 15; $i++) {
			?>
			<div class="col-md-3 Product">
				<div style="box-shadow: 0px 2px 4px #838383; padding-top: 5px;">
					<h4 style="text-align: center;">Nome Prato</h4>
					<img src='<?=URL?>img/header-2.jpg'>
					<div>
						<a href="#" class="btn btn-success col-md-12">Adicionar</a>
						<a href="#" class="btn btn-warning col-md-12">Editar</a>
					</div>
				</div>
			</div>
			<?php
				}
			?>
		</div>
	</div>
</div>