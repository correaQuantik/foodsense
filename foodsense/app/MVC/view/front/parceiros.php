<div class="header">
	<div class="header-slider owl-carousel">
		<div class="header-slide"><img src="<?= URL."img/header-1.jpg"; ?>"></div>
		<div class="header-slide"><img src="<?= URL."img/header-2.jpg"; ?>"></div>
	</div>

	<div class="container">
		<div class="header-logo"><img src="<?= URL."img/header-logo.png"; ?>"></div>

		<div class="header-action"><img src="<?= URL."img/header-action.png"; ?>"></div>
	</div>
</div>

<div class="header-dots owl-dots">
	<div class="container"></div>
</div>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<style type="text/css">
	.navbar {
		margin-bottom: 0;
	}
	.block {
		margin-bottom: 35px;
	}
	.block:nth-child(odd) {
		text-align: right;
	}
</style>
<div class="content">
	<div class="container">
		<div class="content-title">CONHEÇA NOSSOS <b>PARCEIROS</b></div>

		<div class="col-md-12 block">
			<img src="<?= URL."img/header-logo.png"; ?>" class="col-md-2">
			<div class="col-md-10">
				<h2>PARCEIRO 01</h2>
				<p>Morbi a metus. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Nullam sapien sem, ornare ac, nonummy non, lobortis a, enim. Nunc tincidunt ante vitae massa. Duis ante orci, molestie vitae, vehicula venenatis, tincidunt ac, pede.</p>
				<a href="www.foodsense.com.br">www.foodsense.com.br</a>
			</div>
		</div>

		<div class="col-md-12 block">
			<div class="col-md-10">
				<h2>PARCEIRO 01</h2>
				<p>Morbi a metus. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Nullam sapien sem, ornare ac, nonummy non, lobortis a, enim. Nunc tincidunt ante vitae massa. Duis ante orci, molestie vitae, vehicula venenatis, tincidunt ac, pede.</p>
				<a href="www.foodsense.com.br">www.foodsense.com.br</a>
			</div>
			<img src="<?= URL."img/header-logo.png"; ?>" class="col-md-2">
		</div>
		
		<div class="col-md-12 block">
			<img src="<?= URL."img/header-logo.png"; ?>" class="col-md-2">
			<div class="col-md-10">
				<h2>PARCEIRO 01</h2>
				<p>Morbi a metus. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Nullam sapien sem, ornare ac, nonummy non, lobortis a, enim. Nunc tincidunt ante vitae massa. Duis ante orci, molestie vitae, vehicula venenatis, tincidunt ac, pede.</p>
				<a href="www.foodsense.com.br">www.foodsense.com.br</a>
			</div>
		</div>

		<div class="col-md-12 block">
			<div class="col-md-10">
				<h2>PARCEIRO 01</h2>
				<p>Morbi a metus. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Nullam sapien sem, ornare ac, nonummy non, lobortis a, enim. Nunc tincidunt ante vitae massa. Duis ante orci, molestie vitae, vehicula venenatis, tincidunt ac, pede.</p>
				<a href="www.foodsense.com.br">www.foodsense.com.br</a>
			</div>
			<img src="<?= URL."img/header-logo.png"; ?>" class="col-md-2">
		</div>
	</div>
</div>