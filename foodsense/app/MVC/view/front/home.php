<div class="header">
	<div class="header-slider owl-carousel">
		<div class="header-slide"><img src="<?= URL."img/header-1.jpg"; ?>"></div>
		<div class="header-slide"><img src="<?= URL."img/header-2.jpg"; ?>"></div>
	</div>

	<div class="container">
		<div class="header-logo"><img src="<?= URL."img/header-logo.png"; ?>"></div>

		<div class="header-action"><img src="<?= URL."img/header-action.png"; ?>"></div>
	</div>
</div>

<div class="header-dots owl-dots">
	<div class="container"></div>
</div>

<div class="content">
	<div class="container">
		<div class="content-title">QUAL O SEU <b>ESTILO?</b></div>

		<div class="estilos">
			<div class="estilo"><img src="<?= URL."img/estilo-1.png"; ?>"></div>
			<div class="estilo"><img src="<?= URL."img/estilo-2.png"; ?>"></div>
			<div class="estilo"><img src="<?= URL."img/estilo-3.png"; ?>"></div>
			<div class="estilo"><img src="<?= URL."img/estilo-4.png"; ?>"></div>
			<div class="estilo"><img src="<?= URL."img/estilo-5.png"; ?>"></div>
			<div class="estilo"><img src="<?= URL."img/estilo-6.png"; ?>"></div>
			<div class="estilo"><img src="<?= URL."img/estilo-7.png"; ?>"></div>
			<div class="estilo"><img src="<?= URL."img/estilo-8.png"; ?>"></div>
		</div>
	</div>
</div>

<div class="content">
	<div class="container">
		<div class="home-buttons">
			<a href="" class="home-button hover">ENCONTRE NOSSOS PRODUTOS</a>
			<a href="" class="home-button hover">CARDÁPIO</a>
			<a href="" class="home-button hover">FAÇA SEU PEDIDO SEMANAL</a>
		</div>
	</div>
</div>

<div class="leaf-side">
	<img src="<?= URL."img/leaf-side.png"; ?>">
</div>

<section style="position: relative;">
	<div id="map"></div>
	<div class="overlay">
		<h4>O que está procurando hoje?</h4>
		<div class="form-group">
			<select class="form-control">
				<option>Restaurante</option>
				<option>Empório</option>
			</select>
		</div>
		<div class="form-group">
			<label>Digite seu CEP ou escolha uma cidade</label>
			<input type="text" class="form-control" name="" placeholder="99999-999" style="width: 100%;" id="address">
		</div>
		<div class="form-group">
			<span>Você está em <b>Taubaté - SP</b>, <a style="color: #ec6408;" href="#">clique aqui</a> para alterar a cidade.</span>
		</div>
		<button class="btn btn-warning " id="search">Buscar</button>
	</div>
</section>

<div class="content" style="overflow: hidden;">
	<div class="container">
		<div class="content-title"><b>PORQUE NOSSOS PRODUTOS SÃO TÃO ESPECIAIS</b></div>
	</div>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<style type="text/css">
		.navbar {
			margin-bottom: 0;
		}
		.item {
			width: 100vw;
			padding: 0 100px;
		}
		.item img {
			width:auto!important;
		}
		#map {
			position: relative;

		}
		.overlay {
			position: absolute;

			padding: 20px 40px;

			top: 80px;
			left: 50px;

			background-color: white;

			border-radius: 25px;

			z-index: 10000;

			width:325px;

			text-align: center;

			border-right: 5px #E0E0E0 solid;
		}
		#search {
			float: right;
			border-radius: 0;
			background-color: #bed001;
			border: none;
		}
		.form-control {
			text-align: center;
			border-radius: 0;
		}
	</style>
	<div class="container">
		<div class="owl-carousel item-slider">
			<div class="item">
				<div class="col-md-offset-1 col-md-3"><img src="<?= URL."img/suco.png"; ?>"></div>
				<div style="font-size: 15px;" class="col-md-6">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</div>
			</div>
			<div class="item">
				<div class="col-md-offset-1 col-md-3"><img src="<?= URL."img/suco.png"; ?>"></div>
				<div style="font-size: 15px;" class="col-md-6">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</div>
			</div>
			<div class="item">
				<div class="col-md-offset-1 col-md-3"><img src="<?= URL."img/suco.png"; ?>"></div>
				<div style="font-size: 15px;" class="col-md-6">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</div>
			</div>
		</div>
	</div>
	<div class="item-dots owl-dots">
		<div class="container"></div>
	</div>
</div>