<div class="content fill">
	<div class="container">
		<div class="postagens">
			<div class="postagem-view-title"><?= $this->postagem->titulo; ?></div>

			<div class="postagem-view-data"><i class="fa fa-clock-o"></i><?= date("d/m/Y H:i:s", strtotime($this->postagem->criado)); ?></div>

			<?= $this->GetCapa(); ?>

			<?= $this->GetCategorias(); ?>

			<div class="postagem-view-body"><?= $this->postagem->conteudo; ?></div>

			<?= $this->GetYoutube(); ?>

			<?= $this->GetGaleria(); ?>

			<?= $this->GetTAGs(); ?>

			<?= $this->GetArquivos(); ?>

			<?= $this->GetComentarios(); ?>
		</div>

		<div class="postagens-lateral">
			<div class="postagens-scroll">
				<?= $this->GetAutor(); ?>

				<a href="<?= URL."Blog/".$this->search."/".$this->pagina; ?>" class="postagem-view-voltar on-click"><i class="fa fa-chevron-left"></i><span>Voltar</span></a>

				<?= $this->_postagem->GetLast5Postagens($this->search, $this->pagina); ?>
			</div>
		</div>
	</div>
</div>