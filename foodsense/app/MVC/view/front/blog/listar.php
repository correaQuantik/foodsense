<div class="content fill">
	<div class="container">
		<div class="postagens"><?= $this->ListaPostagens(); ?></div>

		<div class="postagens-lateral">
			<div class="postagens-scroll">
				<form action="<?= URL."Blog/Todas/1"; ?>" method="POST" class="postagens-search">
					<div class="postagens-cell big">
						<input type="text" name="name" placeholder="Buscar postagem" value="<?= ($this->search == "Todas" ? "" : $this->search); ?>">
					</div>

					<div class="postagens-cell">
						<button class="on-click"><i class="icon1 fa fa-search"></i><i class="icon2 fa fa-spinner fa-spin hidden"></i></button>
					</div>
				</form>

				<?= $this->_postagem->GetLast5Postagens($this->search, $this->pagina); ?>
			</div>
		</div>
	</div>
</div>