<div class="header">
	<div class="header-slider owl-carousel">
		<div class="header-slide"><img src="<?= URL."img/header-1.jpg"; ?>"></div>
		<div class="header-slide"><img src="<?= URL."img/header-2.jpg"; ?>"></div>
	</div>

	<div class="container">
		<div class="header-logo"><img src="<?= URL."img/header-logo.png"; ?>"></div>

		<div class="header-action"><img src="<?= URL."img/header-action.png"; ?>"></div>
	</div>
</div>

<div class="header-dots owl-dots">
	<div class="container"></div>
</div>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<style type="text/css">
	.navbar {
		margin-bottom: 0;
	}
	form {
		margin-top: 25px;
	}
</style>
<div class="content">
	<div class="container">
		<div class="content-title">Entre em contato</div>

		<div class="col-md-offset-3 col-md-6">
			<div><i></i>WhatsApp: (12)99341-3123</div>
			<div><i></i>Tel: (12)3341-3123</div>
		</div>


		<div class="col-md-offset-3 col-md-6">
			<form method="POST">
				<?= (isset($rtn))? $rtn:""; ?>
				<div class="form-group">
					<label>Seu nome:</label>
					<input type="text" name="contactName" class="form-control">
				</div>
				<div class="form-group">
					<label>email:</label>
					<input type="email" name="contactEmail" class="form-control">
				</div>
				<div class="form-group">
					<label>Telefone:</label>
					<input type="tel" name="contactTel" class="form-control">
				</div>
				<div class="form-group">
					<label>Assunto:</label>
					<textarea name="contactSubject" class="form-control"></textarea>
				</div>
				<input type="submit" name="" value="Enviar" class="btn btn-warning col-md-12">
			</form>
		</div>
	</div>
</div>

<div class="leaf-side">
	<img src="<?= URL."img/leaf-side.png"; ?>">
</div>