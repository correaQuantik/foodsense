<?php

class Facebook
{
	private $facebookID = "320336535019414";
	private $facebookSecret = "c2b8265e27238cda030d3ae58cd80acf";

	public $connected = false;

	private $sql = null;

	public function __construct($sql)
	{
		$this->sql = $sql;

		$this->connected = $this->GetUserData();
	}

	public function InitFacebook()
	{
		return new Facebook\Facebook([
			"app_id" => $this->facebookID,
			"app_secret" => $this->facebookSecret,
			"default_graph_version" => "v2.8",
		]);
	}

	public function LoginFacebook($url)
	{
		$fb = $this->InitFacebook();

		$helper = $fb->getRedirectLoginHelper();

		return $helper->getLoginUrl($url, ["email"]);
	}

	public function RedirectFacebook()
	{
		$fb = $this->InitFacebook();

		$helper = $fb->getRedirectLoginHelper();

		try
		{
			$accessToken = $helper->getAccessToken();
		}
		catch(Facebook\Exceptions\FacebookResponseException $e)
		{
			// When Graph returns an error
			return false;
		}
		catch(Facebook\Exceptions\FacebookSDKException $e)
		{
			// When validation fails or other local issues
			return false;
		}

		if(!isset($accessToken))
		{
			if($helper->getError())
			{
				return false;
			}
			else
			{
				return false;
			}
		}

		// The OAuth 2.0 client handler helps us manage access tokens
		$oAuth2Client = $fb->getOAuth2Client();

		// Get the access token metadata from /debug_token
		$tokenMetadata = $oAuth2Client->debugToken($accessToken);

		// Validation (these will throw FacebookSDKException"s when they fail)
		$tokenMetadata->validateAppId($this->facebookID); // Replace {app-id} with your app id
		// If you know the user ID this access token belongs to, you can validate it here
		//$tokenMetadata->validateUserId("123");
		$tokenMetadata->validateExpiration();

		if(!$accessToken->isLongLived())
		{
			// Exchanges a short-lived access token for a long-lived one
			try
			{
				$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
			}
			catch(Facebook\Exceptions\FacebookSDKException $e)
			{
				// return array("error", "<p>Error getting long-lived access token: ".$helper->getMessage()."</p>\n\n");
				return false;
			}
		}

		$this->SetAcessToken($accessToken->getValue());

		return array("success", $this->GetAccessToken());
	}

	public function GetUserData($all = true)
	{
		$fb = $this->InitFacebook();

		try
		{
			$data = array();
			$data[] = "id";
			$data[] = "name";
			$data[] = "email";
			$data = implode(",", $data);

			// Returns a `Facebook\FacebookResponse` object
			$response = $fb->get("/me?fields=".$data, $this->GetAccessToken());
		}
		catch(Facebook\Exceptions\FacebookResponseException $e)
		{
			// When Graph returns an error
			// return array("error", "Graph returned an error: ".$e->getMessage());
			return false;
		}
		catch(Facebook\Exceptions\FacebookSDKException $e)
		{
			// When validation fails or other local issues
			// return array("error", "Facebook SDK returned an error: ".$e->getMessage());
			return false;
		}

		$graph = $response->getGraphUser();

		if(is_object($graph))
		{
			if($all)
			{
				$user = (object)(array(
					"graph" => (object)(array(
						"id" => $graph["id"],
						"name" => $graph["name"],
						"email" => $graph["email"]
					)),
					"sql" => $this->sql->selectFirst(array("*"), "facebook", array(array("idfacebook", $graph["id"]))),
				));

				if(is_object($user->sql))
				{
					return $user;
				}
				else
				{
					$this->DestroyToken();
					return false;
				}
			}
			else
			{
				return (object)(array(
					"id" => $graph["id"],
					"name" => $graph["name"],
					"email" => $graph["email"]
				));
			}
		}
		else
		{
			$this->DestroyToken();
			return false;
		}
	}

	public function GetUserPicture($id)
	{
		// $request = file_get_contents("http://graph.facebook.com/1395640013793015/picture?type=large");
		return "http://graph.facebook.com/".$id."/picture?type=large";
	}

	public function SetAcessToken($token)
	{
		$_SESSION["facebook_token"] = $token;
	}
	public function DestroyToken()
	{
		if(isset($_SESSION["facebook_token"]))
			unset($_SESSION["facebook_token"]);
	}
	public function GetAccessToken()
	{
		if(isset($_SESSION["facebook_token"]))
		{
			return $_SESSION["facebook_token"];
		}
		else
		{
			return false;
		}
	}
}

?>