<?php

class categoria {
	public $title;				//string
	public $descricao;			//string
	public $img;				//string
	public $habilitado;			//int/bool
	public $igrediente;			//int/bool
	public $igredienteCount;	//int
	public $extra;				//int(array)
	public $extraCount;			//int
	public $molho;				//int/bool
	public $molhoCount;			//int
	public $opcional;			//int(array)
	public $gQuente;			//int(array)
	public $guarniCount;		//int
	public $isGuarnSum;			//int/bool
	public $diaPrato;			//int(array)
	public $papinhaS;			//int/bool
	public $papinhaF;			//int/bool

	public function __construct() {
	}
}

?>