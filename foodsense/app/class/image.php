<?php

class Image
{
	// Variáveos
	public $control;

	// Construtor
	public function __construct($control)
	{
		$this->control = $control;
	}

	// Upload da imagem
	public function Upload($file, $ratio, $width, $height, $path)
	{
		if($file["error"] == 4)
		{
			return array("error", "Ocorreu um erro ao adicionar a foto <b>".$file["name"]."</b>!");
		}
		else
		{
			if($file["error"] != 0)
			{
				switch($file["error"])
				{
					case 1: return array("error", "O arquivo enviado excede o limite de tamanho de arquivos");
					case 2: return array("error", "O arquivo excede o limite do tamanho de um formulário");
					case 3: return array("error", "O upload do arquivo foi feito parcialmente");
					case 6: return array("error", "Pasta temporária ausênte");
					case 7: return array("error", "Falha em escrever o arquivo em disco");
					case 8: return array("error", "Uma extensão do PHP interrompeu o upload do arquivo. O PHP não fornece uma maneira de determinar qual extensão causou a interrupção. Examinar a lista das extensõs) carregadas com o phpinfo() pode ajudar");
					default: return array("error", "Erro desconhecido [".$file["error"]."]");
				}
			}
			else
			{
				$validaImagem = getimagesize($file["tmp_name"]);

				if(!!!$validaImagem)
				{
					return array("error", "O arquivo <b>".$file["name"]."</b> não é uma imagem!");
				}
				else
				{
					// Carrega a classe de upload
					$this->control->loadClass("img.class.upload");

					// Gera um ID único
					$fileName = strtoupper(uniqid());

					// Inicia a classe de Upload
					$upload = new Upload($file);

					// Verifica se arquivo foi enviado corretamente
					if(!!!$upload->uploaded)
					{
						return array("error", "A foto <b>".$file["name"]."</b> não foi enviada corretamente");
					}
					else
					{
						$this->DoUpload($upload, $fileName, $ratio, $width, $height, $path);

						$fileName .= ".".$upload->file_dst_name_ext;

						if(!!!$upload->processed)
						{
							return array("error", "Falha ao processar a foto <b>".$file["name"]."</b>!");
						}
						else
						{
							$upload->Clean();

							return array("success", $fileName, $file["name"]);
						}
					}
				}
			}
		}
	}

	// Ajudante para o envio de imagens
	public function DoUpload($upload, $fileName, $ratio, $width, $height, $path)
	{
		$upload->file_new_name_body = $fileName;
		$upload->image_resize = true;
		$upload->image_ratio = $ratio;
		$upload->image_x = $width;
		$upload->image_y = $height;
		$upload->Process($path);
	}

	// Apaga imagem
	public function Apagar($path)
	{
		if(is_file($path))
		{
			if(file_exists($path))
			{
				if(unlink($path))
				{
					return true;
				}
			}
		}

		return false;
	}
}

?>