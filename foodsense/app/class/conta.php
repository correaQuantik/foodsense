<?php

class Conta
{
	// Variáveos
	public $control;
	public $sql;

	public $_imagem;





	// Construtor
	public function __construct($control)
	{
		$this->control = $control;
		$this->sql = $this->control->getSQL();

		$this->control->loadClass("image");

		$this->_imagem = new Image($this->control);
	}





	// Listar postagen
	public function Listar()
	{
		$contas = $this->sql->select(array("*"), "conta");

		if(!!!$contas)
		{
			return array();
		}
		else
		{
			$rtn = array();

			foreach($contas as $conta)
			{
				$categoria = $this->Categorias();
				$categoriaID = (int)($conta->categoria);

				if(isset($categoria[$categoriaID]))
					$categoria = $categoria[$categoriaID];
				else
					$categoria = "-";

				$time = strtotime($conta->aniversario);

				$rtn[] = array(
					$conta->id,
					"<a href=\"".URL."Painel/Usuarios/".$conta->id."\">".$conta->nome."</a>",
					$categoria,
					$this->sql->count("postagem", array(array("idconta", $conta->id))),
					$conta->profissao,
					"<span class=\"hidden\">".$time."</span>".($conta->aniversario != "0000-00-00" ? date("d/m/Y", $time) : ""),
					"<div class=\"text-center\"><span class=\"btn btn-danger btn-minier btn-apagar\" data-url=\"".URL."Ajax/Usuario/Apagar/".$conta->id."\"><i class=\"fa fa-trash-o fa-fw\"></i></span></div>"
				);
			}

			return $rtn;
		}
	}





	public function Categorias()
	{
		return array(
			1 => "Admin Master",
			2 => "Colaborador",
			3 => "Comprador",
			4 => "Loja"
		);
	}
	public function ListaCategorias($id_categoria)
	{
		$rtn = "";

		$categorias = $this->Categorias();

		$rtn .= "<select name=\"categoria\" id=\"form-categoria\" class=\"form-control\">";
		$rtn .= "<option value=\"0\"".($id_categoria == 0 ? " selected" : "")." disabled>Selecione uma categoria para o novo usuário</option>";

		foreach($categorias as $id => $val)
		{
			$rtn .= "<option value=\"".$id."\"".($id_categoria == $id ? " selected" : "").">".$val."</option>";
		}

		$rtn .= "</select>";

		return $rtn;
	}





	public function NovaConta($post, $json)
	{
		$categoria = isset($post["categoria"]) ? (int)($post["categoria"]) : "";
		$login = isset($post["login"]) ? $post["login"] : "";
		$senha = isset($post["senha"]) ? $post["senha"] : "";
		$email = isset($post["email"]) ? $post["email"] : "";
		$nome = isset($post["nome"]) ? $post["nome"] : "";
		$nascimento = isset($post["nascimento"]) ? $post["nascimento"] : "";
		$sobre = isset($post["sobre"]) ? $post["sobre"] : "";
		$profissao = isset($post["profissao"]) ? $post["profissao"] : "";
		$telefone = isset($post["telefone"]) ? $post["telefone"] : "";
		$link = isset($post["link"]) ? $post["link"] : "";

		if($categoria < 1 || $categoria > 2)
		{
			$json->add("error", "Deve selecionar uma categoria!");
		}
		else if($login == "")
		{
			$json->add("error", "Deve preencher o campo do login!");
		}
		else if($senha == "")
		{
			$json->add("error", "Deve preencher o campo da senha!");
		}
		else if($email == "")
		{
			$json->add("error", "Deve preencher o campo do e-mail!");
		}
		else if($nome == "")
		{
			$json->add("error", "Deve preencher o campo do nome!");
		}
		else
		{
			$senha = md5($senha);

			if($nascimento != "")
			{
				$nascimento = implode("-", array_reverse(explode("/", $nascimento)));
			}

			$id_conta = $this->sql->insert("conta", array(array("categoria", $categoria), array("login", $login), array("senha", $senha), array("email", $email), array("nome", $nome), array("aniversario", $nascimento), array("sobre", $sobre), array("profissao", $profissao), array("telefone", $telefone), array("link", $link), array("criado", $this->sql->date())));

			if(!!!$id_conta)
			{
				$json->add("error", "Falha ao criar usuário, tente novamente!");
			}
			else
			{
				$json->add("success", "Usuário <b>".$nome."</b> criado com sucesso!");
				$json->add("url", URL."Painel/Usuarios/".$id_conta);
				$json->add("id", $id_conta);
				$json->add("files", true);
			}
		}
	}

	public function Mostra($id_conta)
	{
		$conta = $this->sql->selectFirst(array("*"), "conta", array(array("id", $id_conta)));

		$conta->aniversario = implode("/", array_reverse(explode("-", $conta->aniversario)));

		return $conta;
	}

	// Edita uma conta
	public function Editar($id_conta, $post, $json, $perfil = false)
	{
		if(!!!$this->Mostra($id_conta))
		{
			$json->add("error", "Usuário não existe!");
			$json->add("redirect", URL."Painel/Usuarios/Listar");
		}
		else
		{
			$categoria = isset($post["categoria"]) ? (int)($post["categoria"]) : "";
			$login = isset($post["login"]) ? $post["login"] : "";
			$senha = isset($post["senha"]) ? $post["senha"] : "";
			$email = isset($post["email"]) ? $post["email"] : "";
			$nome = isset($post["nome"]) ? $post["nome"] : "";
			$nascimento = isset($post["nascimento"]) ? $post["nascimento"] : "";
			$sobre = isset($post["sobre"]) ? $post["sobre"] : "";
			$profissao = isset($post["profissao"]) ? $post["profissao"] : "";
			$telefone = isset($post["telefone"]) ? $post["telefone"] : "";
			$link = isset($post["link"]) ? $post["link"] : "";

			if(!$perfil && ($categoria < 1 || $categoria > 2))
			{
				$json->add("error", "Deve selecionar uma categoria!");
			}
			else if(!$perfil && $login == "")
			{
				$json->add("error", "Deve preencher o campo do login!");
			}
			else if($email == "")
			{
				$json->add("error", "Deve preencher o campo do e-mail!");
			}
			else if($nome == "")
			{
				$json->add("error", "Deve preencher o campo do nome!");
			}
			else
			{
				if($nascimento != "")
				{
					$nascimento = implode("-", array_reverse(explode("/", $nascimento)));
				}

				$set = array(array("email", $email), array("nome", $nome), array("aniversario", $nascimento), array("sobre", $sobre), array("profissao", $profissao), array("telefone", $telefone), array("link", $link));

				if(!$perfil)
				{
					$set[] = array("categoria", $categoria);
					$set[] = array("login", $login);
				}

				if($senha != "")
				{
					$senha = md5($senha);
					$set[] = array("senha", $senha);
				}

				$this->sql->update("conta", $set, array(array("id", $id_conta)));

				$json->add("success", ($perfil ? "Perfil editado com sucesso!" : "Usuário <b>".$nome."</b> editado com sucesso!"));
				$json->add("url", URL."Painel/".($perfil ? "Perfil" : "Usuarios/".$id_conta));
				$json->add("files", true);

				return true;
			}

			return false;
		}
	}

	public function Apagar($id_conta, $json)
	{
		$json->add("no-refresh", true);

		$conta = $this->Mostra($id_conta);

		if(!$conta)
		{
			$json->add("error", "Usuário não existe!");
		}
		else
		{
			if($conta->foto != "")
			{
				$this->ApagaFoto($conta->foto);
			}

			if($this->sql->delete("conta", array(array("id", $conta->id))))
			{
				$json->add("success", "Usuário apagado com sucesso!");
			}
			else
			{
				$json->add("error", "Houve uma falha ao apagar o usuário, tente novamente!");
			}
		}
	}





	// Adiciona imagem
	public function AddImagem($conta, $file, $json, $type)
	{
		$dir = false;

		if($type == "foto")
		{
			$dir = "usuarios";
		}

		if(!!!$dir)
		{
			$json->add("error", "Tipo de imagem inválido! #2");
		}
		else
		{
			$upload = $this->_imagem->Upload($file, true, 1024, 1024, dirname(__FILE__)."/../../img/".$dir."/");

			if(!is_array($upload))
			{
				$json->add("error", "Falha no upload!");
			}
			else if(count($upload) < 2)
			{
				$json->add("error", "Falha no upload!");
			}
			else if($upload[0] == "error")
			{
				$json->add($upload[0], $upload[1]);
			}
			else if($upload[0] == "success")
			{
				$savename = $upload[1];
				$filename = $upload[2];

				$insert = false;

				if($type == "foto")
				{
					if($conta->foto != "")
					{
						$this->ApagaFoto($conta->foto);
					}

					$insert = $this->sql->update("conta", array(array("foto", $savename)), array(array("id", $conta->id)));
				}

				if(!!!$insert)
				{
					$json->add("error", "Falha ao enviar a imagem <b>".$filename."</b>!");
					return false;
				}
				else
				{
					$json->add("success", "Imagem <b>".$filename."</b> enviada com sucesso!");
					return true;
				}
			}
		}

		return false;
	}

	// Apaga a foto
	public function ApagaFoto($foto)
	{
		$this->_imagem->Apagar(dirname(__FILE__)."/../../img/usuarios/".$foto);
	}
}

?>