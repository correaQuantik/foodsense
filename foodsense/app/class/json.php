<?php

class JSON
{
	protected $json;

	public function __construct()
	{
		$this->json = Array();
	}

	// Adiciona valor no JSON
	public function add($key, $val)
	{
		$this->json[$key] = $val;
	}

	public function getString()
	{
		return json_encode($this->json);
	}
}

?>