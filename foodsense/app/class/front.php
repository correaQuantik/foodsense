<?php

class Front
{
	// Variables
	private $control;
	private $sql;

	private $active;

	private $navbar_relative = false;

	// Constructor
	public function __construct($control)
	{
		$this->control = $control;
		$this->sql = $control->getSQL();
	}

	// Set active
	public function SetActive($active)
	{
		$this->active = $active;
	}

	// Get active
	public function GetActive() {
		return $this->active;
	}

	// Get menu
	public function GetMenu($class)
	{
		$links = array(
			// Texto, Link
			array("HOME", array("Home")),
			array("SOBRE", array("Sobre")),
			array("PRODUTOS", array("Produtos")),
			array("QUAL O SEU ESTILO?", array("QualSeuEstilo")),
			array("DIFERENCIAIS", array("Diferenciais")),
			array("BLOG", array("Blog", "Todas", "1")),
			array("PARCEIROS", array("Parceiros")),
			array("CONTATO", array("Contato")),
		);

		$rtn = "";

		foreach($links as $link)
		{
			$url = URL.implode("/", $link[1]);
			$active = ($link[1][0] == $this->active ? " active" : "");
			$rtn .= "<a href=\"".$url."\" class=\"".$class.$active."\"><span>".$link[0]."</span></a>";
		}

		return $rtn;
	}

	// Set navbar relative
	public function SetNavbar($relative)
	{
		$this->navbar_relative = $relative;
	}

	// Get navbat classes
	public function GetNavbarClasses()
	{
		return "navbar".(!!$this->navbar_relative ? " static" : "");
	}
}

?>