<?php

class SQL
{



	protected $pdo;



	public function __construct()
	{
		try
		{
			$this->pdo = new PDO("mysql:host=localhost;dbname=".DBname, DBuser, DBpassword);
			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch(PDOException $e)
		{
			exit("Error!: ".$e->getMessage());
		}
	}



	// Retorna o ultimo ID
	public function getLastID()
	{
		return $this->pdo->lastInsertId();
	}



	// Retorna dados do banco
	public function select($fields, $table, $where = null, $limit = null, $orderby = null, $having = null, $groupby = null)
	{
		return $this->selectJoin($fields, $table, null, $where, $limit, $orderby, $having, $groupby);
	}



	// Retorna o primeiro dado do banco
	public function selectFirst($fields, $table, $where = null, $limit = null, $orderby = null, $having = null, $groupby = null)
	{
		$rtn = $this->selectJoin($fields, $table, null, $where, $limit, $orderby, $having, $groupby);
		return is_array($rtn) ? $rtn[0] : $rtn;
	}



	// Retorna o primeiro dado do banco
	public function selectJoinFirst($fields, $table, $join = null, $where = null, $limit = null, $orderby = null, $having = null, $groupby = null)
	{
		$rtn = $this->selectJoin($fields, $table, $join, $where, $limit, $orderby, $having, $groupby);
		return is_array($rtn) ? $rtn[0] : $rtn;
	}



	// Retorna dados do banco
	public function selectJoin($fields, $table, $join = null, $where = null, $limit = null, $orderby = null, $having = null, $groupby = null)
	{
		// Prepara a string da query
		$query = "SELECT ".implode(", ", array_map(function($el){
			if($el == "*" || $el == "1")
			{
				return $el;
			}
			else if(preg_match("/(DAY\([A-Za-z0-9]+\))|(MONTH\([A-Za-z0-9]+\))|(YEAR\([A-Za-z0-9]+\))/", $el))
			{
				return $el;
			}
			else if(preg_match("/[.]/", $el))
			{
				$el = explode(".", $el);

				if($el[1] == "*" || $el[1] == "1")
				{
					$el[1] = $el[1];
				}
				else if(preg_match("/(DAY\([A-Za-z0-9]+\))|(MONTH\([A-Za-z0-9]+\))|(YEAR\([A-Za-z0-9]+\))/", $el[1]))
				{
					$el[1] = $el[1];
				}
				else
				{
					$el[1] = "`".$el[1]."`";
				}

				return "`".$el[0]."`.".$el[1];
			}
			else
			{
				return "`".$el."`";
			}
		}, $fields))." FROM `".$table."`";

		if(!is_null($join))
		{
			$query .= implode(" ", array_map(function($el){
				$el[1] = explode(".", $el[1]);
				$el[2] = explode(".", $el[2]);
				return " INNER JOIN `".$el[0]."` ON `".$el[1][0]."`.`".$el[1][1]."`=`".$el[2][0]."`.`".$el[2][1]."`";
			}, $join));
		}

		if(!is_null($where))
		{
			$prepare = array();

			for($i = 0; $i < count($where); $i++)
			{
				if(preg_match("/(DAY\([A-Za-z0-9]+\))|(MONTH\([A-Za-z0-9]+\))|(YEAR\([A-Za-z0-9]+\))/", $where[$i][0]))
				{
					$prepare[] = $where[$i][0].(isset($where[$i][2]) ? $where[$i][2] : "=").":where_".$i.$this->fixDot($where[$i][0]);
				}
				else
				{
					$prepare[] = "`".$where[$i][0]."`".(isset($where[$i][2]) ? $where[$i][2] : "=").":where_".$i.$this->fixDot($where[$i][0]);
				}
			}

			for($i = 0; $i < count($prepare); $i++)
			{
				$inner = explode(".", $prepare[$i]);

				if(count($inner) == 2)
				{
					$prepare[$i] = $inner[0]."`.`".$inner[1];
				}
			}

			$query .= " WHERE ".implode(" AND ", $prepare);
		}

		if(!is_null($groupby))
		{
			$query .= " GROUP BY ".implode(", ", array_map(function($el){
				if(preg_match("/(DAY\([A-Za-z0-9]+\))|(MONTH\([A-Za-z0-9]+\))|(YEAR\([A-Za-z0-9]+\))/", $el))
					return $el;
				else
					return "`".$el."`";
			}, $groupby));
		}

		if(!is_null($having))
		{
			$prepare = array();

			for($i = 0; $i < count($having); $i++)
			{
				if(preg_match("/(DAY\([A-Za-z0-9]+\))|(MONTH\([A-Za-z0-9]+\))|(YEAR\([A-Za-z0-9]+\))/", $having[$i][0]))
				{
					$prepare[] = $having[$i][0].(isset($having[$i][2]) ? $having[$i][2] : "=").":having_".$i.$this->fixDot($having[$i][0]);
				}
				else
				{
					$prepare[] = "`".$having[$i][0]."`".(isset($having[$i][2]) ? $having[$i][2] : "=").":having_".$i.$this->fixDot($having[$i][0]);
				}
			}

			for($i = 0; $i < count($prepare); $i++)
			{
				$inner = explode(".", $prepare[$i]);

				if(count($inner) == 2)
				{
					$prepare[$i] = $inner[0]."`.`".$inner[1];
				}
			}

			$query .= " HAVING ".implode(" AND ", $prepare);
		}

		if(!is_null($orderby))
		{
			$query .= " ORDER BY ".implode(", ", array_map(function($el){
				if(preg_match("/rand()/", $el[0]))
					return $el[0];
				else
					return "`".$el[0]."`".(isset($el[1]) ? " ".$el[1] : "");
			}, $orderby));
		}

		if(!is_null($limit))
		{
			$query .= is_null($limit) ? "" : " LIMIT ".implode(", ", $limit);
		}

		// if(!is_null($join))
		// {
		// 	return $query;
		// }

		// Prepara a query
		$sql = $this->pdo->prepare($query);

		if(!is_null($where))
		{
			for($i = 0; $i < count($where); $i++)
				$sql->bindParam(":where_".$i.$this->fixDot($where[$i][0]), $where[$i][1]);
		}

		if(!is_null($having))
		{
			for($i = 0; $i < count($having); $i++)
				$sql->bindParam(":having_".$i.$this->fixDot($having[$i][0]), $having[$i][1]);
		}

		// Executa a query
		$sql->execute();

		if($sql->rowCount() > 0)
		{
			$rtn = array();

			// Varre a query executada
			while($row = $sql->fetchObject())
				$rtn[] = $row;

			// Retorna uma array com os resultados encontrados
			return $rtn;
		}
		else
		{
			// Retorna false caso nada seja encontrado
			return false;
		}
	}



	// Insere dado no banco
	public function insert($table, $data)
	{
		// Prepara a string da query
		$query = "INSERT INTO `".$table."` SET ";

		$query .= implode(", ", array_map(function($el){
			return "`".$el[0]."`=:set_".$el[0];
		}, $data));

		// Prepara a query
		$sql = $this->pdo->prepare($query);

		for($i = 0; $i < count($data); $i++)
			$sql->bindParam(":set_".$this->fixDot($data[$i][0]), $data[$i][1]);

		// Executa a query
		$sql->execute();

		// Retorna o ultimo ID inserido
		return $sql->rowCount() > 0 ? $this->getLastID() : false;
	}



	// Atualiza dado no banco
	public function update($table, $data, $where = null)
	{
		// Prepara a string da query
		$query = "UPDATE `".$table."` SET ";

		$query .= implode(", ", array_map(function($el){
			return "`".$el[0]."`=:set_".$el[0];
		}, $data));

		if(!is_null($where))
		{
			$prepare = array();
			for($i = 0; $i < count($where); $i++)
				if(preg_match("/(DAY\([A-Za-z0-9]+\))|(MONTH\([A-Za-z0-9]+\))|(YEAR\([A-Za-z0-9]+\))/", $where[$i][0]))
					$prepare[] = $where[$i][0].(isset($where[$i][2]) ? $where[$i][2] : "=").":where_".$i.$this->fixDot($where[$i][0]);
				else
					$prepare[] = "`".$where[$i][0]."`".(isset($where[$i][2]) ? $where[$i][2] : "=").":where_".$i.$this->fixDot($where[$i][0]);
			$query .= " WHERE ".implode(" AND ", $prepare);
		}

		// Prepara a query
		$sql = $this->pdo->prepare($query);

		for($i = 0; $i < count($data); $i++)
			$sql->bindParam(":set_".$this->fixDot($data[$i][0]), $data[$i][1]);

		if(!is_null($where))
		{
			for($i = 0; $i < count($where); $i++)
				$sql->bindParam(":where_".$i.$this->fixDot($where[$i][0]), $where[$i][1]);
		}

		// Executa a query
		$sql->execute();

		// Retorna quantod dados foram alterados
		return $sql->rowCount();
	}



	// Apaga dado do banco
	public function delete($table, $where = null)
	{
		// Prepara a string da query
		$query = "DELETE FROM `".$table."`";

		if(!is_null($where))
		{
			$prepare = array();
			for($i = 0; $i < count($where); $i++)
				if(preg_match("/(DAY\([A-Za-z0-9]+\))|(MONTH\([A-Za-z0-9]+\))|(YEAR\([A-Za-z0-9]+\))/", $where[$i][0]))
					$prepare[] = $where[$i][0].(isset($where[$i][2]) ? $where[$i][2] : "=").":where_".$i.$this->fixDot($where[$i][0]);
				else
					$prepare[] = "`".$where[$i][0]."`".(isset($where[$i][2]) ? $where[$i][2] : "=").":where_".$i.$this->fixDot($where[$i][0]);
			$query .= " WHERE ".implode(" AND ", $prepare);
		}

		// Prepara a query
		$sql = $this->pdo->prepare($query);

		if(!is_null($where))
		{
			for($i = 0; $i < count($where); $i++)
				$sql->bindParam(":where_".$i.$this->fixDot($where[$i][0]), $where[$i][1]);
		}

		// Executa a query
		$sql->execute();

		// Retorna quantod dados foram alterados
		return $sql->rowCount();
	}



	// Conta registro no BD
	public function count($table, $where = null)
	{
		// Prepara a string da query
		$query = "SELECT count(1) AS count FROM `".$table."`";

		if(!is_null($where))
		{
			$prepare = array();
			for($i = 0; $i < count($where); $i++)
				if(preg_match("/(DAY\([A-Za-z0-9]+\))|(MONTH\([A-Za-z0-9]+\))|(YEAR\([A-Za-z0-9]+\))/", $where[$i][0]))
					$prepare[] = $where[$i][0].(isset($where[$i][2]) ? $where[$i][2] : "=").":val_".$i.$this->fixDot($where[$i][0]);
				else
					$prepare[] = "`".$where[$i][0]."`".(isset($where[$i][2]) ? $where[$i][2] : "=").":val_".$i.$this->fixDot($where[$i][0]);
			$query .= " WHERE ".implode(" AND ", $prepare);
		}

		// Prepara a query
		$sql = $this->pdo->prepare($query);

		if(!is_null($where))
		{
			for($i = 0; $i < count($where); $i++)
				$sql->bindParam(":val_".$i.$this->fixDot($where[$i][0]), $where[$i][1]);
		}

		// Executa a query
		$sql->execute();

		// Retorna objeto
		$row = $sql->fetchObject();

		// Retorna quantod dados foram alterados
		return $row->count;
	}



	// Retorna a data
	public function date($format = null, $time = null)
	{
		if(is_null($format))
			$format = "Y-m-d H:i:s";

		if(is_null($time))
			$time = time();

		return date($format, $time);
	}



	// Remove pontos para a query
	public function fixDot($str)
	{
		return preg_replace("/[.()]/", "", $str);
	}



}

?>