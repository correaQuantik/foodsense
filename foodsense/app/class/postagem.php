<?php

class Postagem
{
	// Variáveos
	public $control;
	public $sql;

	public $_imagem;





	// Construtor
	public function __construct($control)
	{
		$this->control = $control;
		$this->sql = $this->control->getSQL();

		$this->control->loadClass("image");

		$this->_imagem = new Image($this->control);
	}





	public function Status()
	{
		return array(
			1 => "Rascunho",
			2 => "Enviado Pelo Autor",
			3 => "Post com pendências de alterações do Autor", // caso admin peça alterações
			4 => "Aguardando Aprovação do Autor", // envia email para o autor conferir e dar ok para habilitar
			5 => "Aprovado Pelo Autor",
			6 => "Habilitado", // só admin pode alterar
			7 => "Desabilitado pelo Autor",
			8 => "Desabilitado pelo Admin",
		);
	}
	public function ListaStatus($id_status)
	{
		$rtn = "";

		$status = $this->Status();

		$rtn .= "<select name=\"status\" id=\"form-status\" class=\"form-control\">";
		$rtn .= "<option value=\"0\"".($id_status == 0 ? " selected" : "")." disabled>Selecione um status para a postagem</option>";

		foreach($status as $id => $val)
		{
			$rtn .= "<option value=\"".$id."\"".($id_status == $id ? " selected" : "").">".$val."</option>";
		}

		$rtn .= "</select>";

		return $rtn;
	}





	// Listar postagen
	public function Listar($conta)
	{
		$postagens = false;

		if($conta->categoria == 1)
		{
			$postagens = $this->sql->select(array("*"), "postagem");
		}
		else
		{
			$postagens = $this->sql->select(array("*"), "postagem", array(array("idconta", $conta->id)));
		}

		if(!!!$postagens)
		{
			return array();
		}
		else
		{
			$rtn = array();

			foreach($postagens as $postagem)
			{
				$comentarios = $this->sql->count("postagem_comentario", array(array("idpostagem", $postagem->id), array("aprovado", 0)));

				$status = $this->Status();
				$statusID = (int)($postagem->status);

				if(isset($status[$statusID]))
					$status = $status[$statusID];
				else
					$status = "-";

				$rtn[] = array(
					$postagem->id,
					$status,
					"<a href=\"".URL."Painel/Postagem/".$postagem->id."\">".$postagem->titulo."</a>",
					"<a href=\"".URL."Painel/Postagem/".$postagem->id."/Comentarios\">".($comentarios == 0 ? "Nenhum" : $comentarios." novo".($comentarios > 1 ? "s" : ""))."</a>",
					"<div class=\"text-center\"><span class=\"btn btn-danger btn-minier btn-apagar\" data-url=\"".URL."Ajax/Postagem/Apagar/".$postagem->id."\"><i class=\"fa fa-trash-o fa-fw\"></i></span></div>"
				);
			}

			return $rtn;
		}
	}





	// Cria uma nova postagem
	public function Nova($post, $json)
	{
		$status = isset($post["status"]) ? (int)($post["status"]) : 0;
		$tags = isset($post["tags"]) ? $post["tags"] : "";
		$categorias = isset($post["categorias"]) ? (is_array($post["categorias"]) ? $post["categorias"] : array()) : array();
		$titulo = isset($post["titulo"]) ? $post["titulo"] : "";
		$conteudo = isset($post["conteudo"]) ? $post["conteudo"] : "";
		$youtube = isset($post["youtube"]) ? $post["youtube"] : "";
		$observacoes = isset($post["observacoes"]) ? $post["observacoes"] : "";

		if($this->control->painel->conta->categoria != 1)
		{
			$status = 2;
		}

		if(!isset($this->Status()[$status]))
		{
			$json->add("error", "Status inválido!");
		}
		else if($titulo == "")
		{
			$json->add("error", "Preencha o campo do título!");
		}
		else if($conteudo == "")
		{
			$json->add("error", "Preencha o campo do conteúdo!");
		}
		else
		{
			if($youtube != "")
			{
				if(strlen($youtube) != 11)
				{
					$json->add("error", "Link do <b>Youtube</b> deve conter apenas 11 caracteres!");
					return;
				}
			}

			$idconta = $this->control->painel->conta->id;

			$historico = array();
			$historico[] = array($status, $observacoes, time());
			$historico = json_encode($historico);

			$idpostagem = $this->sql->insert("postagem", array(array("idconta", $idconta), array("status", $status), array("titulo", $titulo), array("conteudo", $conteudo), array("youtube", $youtube), array("observacoes", $observacoes), array("historico", $historico), array("criado", $this->sql->date())));

			if(!!!$idpostagem)
			{
				$json->add("error", "Falha ao inserir postagem, tente novamente!");

				return false;
			}
			else
			{
				$tags = explode(",", $tags);

				if(is_array($tags))
				{
					foreach($tags as $tag)
					{
						if($tag != "")
						{
							$this->InsereTAG($idpostagem, $tag);
						}
					}
				}

				foreach($categorias as $categoria)
				{
					$this->InsereCategoria($idpostagem, $categoria);
				}

				$json->add("success", "Postagem inserida com sucesso!");
				$json->add("url", URL."Painel/Postagem/".$idpostagem);
				$json->add("id", $idpostagem);
				$json->add("files", true);

				return true;
			}
		}

		return false;
	}

	// Mostra a postagem
	public function Mostra($idpostagem)
	{
		$postagem = $this->sql->selectFirst(array("*"), "postagem", array(array("id", $idpostagem)));

		if(!!!$postagem)
		{
			return null;
		}
		else
		{
			if($postagem->capa == "")
				$postagem->capa = "none.png";

			$postagem->categorias = $this->sql->selectJoin(array("postagem_categoria.*"), "postagem_categorias", array(array("postagem_categoria", "postagem_categoria.id", "postagem_categorias.idcategoria")), array(array("postagem_categorias.idpostagem", $idpostagem)));

			$postagem->tags = $this->sql->selectJoin(array("postagem_tag.*"), "postagem_tags", array(array("postagem_tag", "postagem_tag.id", "postagem_tags.idtag")), array(array("postagem_tags.idpostagem", $idpostagem)));

			$postagem->fotos = $this->sql->select(array("*"), "postagem_fotos", array(array("idpostagem", $idpostagem)));

			$postagem->comentarios = $this->sql->select(array("*"), "postagem_comentario", array(array("idpostagem", $idpostagem), array("aprovado", 1), array("resposta", 0)), null, array(array("id", "DESC")));

			$postagem->arquivos = $this->sql->select(array("*"), "postagem_arquivo", array(array("idpostagem", $idpostagem)));

			$postagem->categorias = ($postagem->categorias == null ? array() : $postagem->categorias);
			$postagem->tags = ($postagem->tags == null ? array() : $postagem->tags);
			$postagem->fotos = ($postagem->fotos == null ? array() : $postagem->fotos);
			$postagem->comentarios = ($postagem->comentarios == null ? array() : $postagem->comentarios);

			return $postagem;
		}
	}

	// Edita uma postagem
	public function Editar($idpostagem, $post, $json)
	{
		$postagem = $this->Mostra($idpostagem);

		if(!!!$postagem)
		{
			$json->add("error", "Postagem não existe!");
			$json->add("redirect", URL."Painel/Postagem/Listar");
		}
		else
		{
			$autor = isset($post["autor"]) ? (int)($post["autor"]) : 0;
			$status = isset($post["status"]) ? (int)($post["status"]) : 0;
			$tags = isset($post["tags"]) ? $post["tags"] : "";
			$categorias = isset($post["categorias"]) ? (is_array($post["categorias"]) ? $post["categorias"] : array()) : array();
			$titulo = isset($post["titulo"]) ? $post["titulo"] : "";
			$conteudo = isset($post["conteudo"]) ? $post["conteudo"] : "";
			$youtube = isset($post["youtube"]) ? $post["youtube"] : "";
			$observacoes = isset($post["observacoes"]) ? $post["observacoes"] : "";
			$fotos = isset($post["fotos"]) ? $post["fotos"] : false;
			$arquivos = isset($post["arquivos"]) ? $post["arquivos"] : false;

			if($this->control->painel->conta->categoria != 1)
			{
				$status = 3;
			}

			if($this->sql->count("conta", array(array("id", $autor))) == 0)
			{
				$json->add("error", "Autor inválido!");
			}
			else if(!isset($this->Status()[$status]))
			{
				$json->add("error", "Habilitado inválido!");
			}
			else if($titulo == "")
			{
				$json->add("error", "Preencha o campo do título!");
			}
			else if($conteudo == "")
			{
				$json->add("error", "Preencha o campo do conteúdo!");
			}
			else
			{
				if($youtube != "")
				{
					if(strlen($youtube) != 11)
					{
						$json->add("error", "Link do <b>Youtube</b> deve conter apenas 11 caracteres!");
						return;
					}
				}

				$set = array(array("idconta", $autor), array("status", $status), array("titulo", $titulo), array("conteudo", $conteudo), array("youtube", $youtube), array("observacoes", $observacoes), array("alterado", $this->sql->date()));

				if($status != $postagem->status || $observacoes != $postagem->observacoes)
				{
					$historico = json_decode($postagem->historico);
					$historico[] = array($status, $observacoes, time());
					$historico = json_encode($historico);

					$set[] = array("historico", $historico);
				}

				$this->sql->update("postagem", $set, array(array("id", $idpostagem)));

				if(!!$fotos)
				{
					foreach($fotos as $idfoto)
					{
						$foto = $this->MostraFoto($idpostagem, $idfoto);

						if(!!$foto)
						{
							$this->ApagaFoto($foto->imagem, "galeria", $foto->id);
						}
					}
				}

				if(!!$arquivos)
				{
					foreach($arquivos as $val)
					{
						$arquivo = $this->sql->selectFirst(array("*"), "postagem_arquivo", array(array("id", $val), array("idpostagem", $idpostagem)));

						if(!!$arquivo)
						{
							$path = dirname(__FILE__)."/../../files/".$arquivo->arquivo;
							if(file_exists($path))
								if(is_file($path))
									unlink($path);

							$this->sql->delete("postagem_arquivo", array(array("id", $val)));
						}
					}
				}

				$this->DesvinculaTAGs($idpostagem);

				$tags = explode(",", $tags);

				if(is_array($tags))
				{
					foreach($tags as $tag)
					{
						if($tag != "")
						{
							$this->InsereTAG($idpostagem, $tag);
						}
					}
				}

				$this->DesvinculaCategorias($idpostagem);

				foreach($categorias as $categoria)
				{
					$this->InsereCategoria($idpostagem, $categoria);
				}

				if($this->control->painel->conta->categoria == 1)
				{
					if($this->control->painel->conta->id != $autor)
					{
						$this->control->loadClass("mailer");

						$mail = new Mailer($this->control);

						$mail->Send(array("nelson@quantik.com.br", "Nelson"), "Alteração realizada em sua postagem no FoodSense", "Olá! Sua postagem \"<b>".$titulo."</b>\" foi alterada, clique <a href=\"".URL."Painel/Postagem/".$idpostagem."\" target=\"_blank\">AQUI</a> para vê-la!");
					}
				}

				$json->add("success", "Postagem editada com sucesso!");
				$json->add("url", URL."Painel/Postagem/".$idpostagem);
				$json->add("files", true);

				return true;
			}

			return false;
		}
	}

	// Apaga uma postagem
	public function Apagar($idpostagem, $json)
	{
		$json->add("no-refresh", true);

		$postagem = $this->Mostra($idpostagem);

		if(!$postagem)
		{
			$json->add("error", "Postagem não existe!");
		}
		else
		{
			if($this->sql->delete("postagem", array(array("id", $postagem->id))))
			{
				if($postagem->capa != "")
				{
					$this->ApagaFoto($postagem->capa, "galeria");
				}

				if(!!$postagem->fotos)
				{
					foreach($postagem->fotos as $foto)
					{
						$this->ApagaFoto($foto->imagem, "galeria", $foto->id);
					}
				}

				if(!!$postagem->arquivos)
				{
					foreach($postagem->arquivos as $arquivo)
					{
						$path = dirname(__FILE__)."/../../files/".$arquivo->arquivo;
						if(file_exists($path))
							if(is_file($path))
								unlink($path);
					}
				}

				$json->add("success", "Postagem apagada com sucesso!");
			}
			else
			{
				$json->add("error", "Houve uma falha ao apagar a postagem, tente novamente!");
			}
		}
	}

	// Retorna as categorias da postagem
	public function PostagemCategorias($postagem)
	{
		$rtn = array();

		$categorias = $this->sql->select(array("id", "nome"), "postagem_categoria");

		if(!!$categorias)
		{
			foreach($categorias as $categoria)
			{
				$relacao = $this->sql->selectFirst(array("1"), "postagem_categorias", array(array("idpostagem", $postagem->id), array("idcategoria", $categoria->id)));
				$rtn[] = "<option value=\"".$categoria->id."\"".(!!$relacao ? " selected" : "").">".$categoria->nome."</option>";
			}
		}

		return implode("", $rtn);
	}

	// Retorna as tags da postagem
	public function PostagemTAGs($postagem)
	{
		$rtn = array();

		foreach($postagem->tags as $tag)
		{
			$rtn[] = $tag->nome;
		}

		return implode(",", $rtn);
	}





	// Listar categorias
	public function ListarCategorias()
	{
		$categorias = $this->sql->select(array("*"), "postagem_categoria");

		if(!!!$categorias)
		{
			return array();
		}
		else
		{
			$rtn = array();

			foreach($categorias as $categoria)
			{
				$rtn[] = array(
					$categoria->id,
					"<a href=\"".URL."Painel/Postagem/Categoria/".$categoria->id."\">".$categoria->nome."</a>",
					$this->ContaLigacaoCategoriaPostagens($categoria->id),
					"<div class=\"text-center\"><span class=\"btn btn-danger btn-minier btn-apagar\" data-url=\"".URL."Ajax/Postagem/Categoria/Apagar/".$categoria->id."\"><i class=\"fa fa-trash-o fa-fw\"></i></span></div>"
				);
			}

			return $rtn;
		}
	}

	// Conta quantas postagens uma categoria está ligada
	public function ContaLigacaoCategoriaPostagens($idcategoria)
	{
		return $this->sql->count("postagem_categorias", array(array("idcategoria", $idcategoria)));
	}

	// Lista as categoria pro select
	public function ListarCategoriasSelect()
	{
		$rtn = array();

		$categorias = $this->sql->select(array("id", "nome"), "postagem_categoria");

		if(!!$categorias)
		{
			foreach($categorias as $categoria)
			{
				$rtn[] = "<option value=\"".$categoria->id."\">".$categoria->nome."</option>";
			}
		}

		return implode("", $rtn);
	}





	// Controla a categoria a ser inserida
	public function InsereCategoria($idpostagem, $idcategoria)
	{
		$idcategoria = (int)($idcategoria);

		if(!!$this->MostraCategoria("id", $idcategoria))
		{
			return !!$this->sql->insert("postagem_categorias", array(array("idpostagem", $idpostagem), array("idcategoria", $idcategoria)));
		}

		return false;
	}

	// Mostra a categoria
	public function MostraCategoria($tipo, $valor, $id = null)
	{
		$where = array();
		$where[] = array($tipo, $valor);

		if($id != null)
		{
			$where[] = array("id", $id, "!=");
		}

		return $this->sql->selectFirst(array("*"), "postagem_categoria", $where);
	}

	// Adiciona a categoria
	public function NovaCategoria($post, $json)
	{
		$nome = isset($post["nome"]) ? $post["nome"] : "";

		if($nome == "")
		{
			$json->add("error", "Deve preencher o nome da categoria!");
		}
		else if(!!$this->MostraCategoria("nome", $nome))
		{
			$json->add("error", "Está categoria já existe!");
		}
		else
		{
			$idcategoria = $this->sql->insert("postagem_categoria", array(array("nome", $nome), array("criado", $this->sql->date())));

			if(!!!$idcategoria)
			{
				$json->add("error", "Falha ao inserir categoria, tente novamente!");
			}
			else
			{
				$json->add("success", "Categoria inserida com sucesso!");
				$json->add("url", URL."Painel/Postagem/Categoria/".$idcategoria);
			}
		}
	}

	// Edita a categoria
	public function EditarCategoria($idcategoria, $post, $json)
	{
		$nome = isset($post["nome"]) ? $post["nome"] : "";

		if($nome == "")
		{
			$json->add("error", "Deve preencher o nome da categoria!");
		}
		else if(!!$this->MostraCategoria("nome", $nome, $idcategoria))
		{
			$json->add("error", "Está categoria já existe!");
		}
		else
		{
			$this->sql->update("postagem_categoria", array(array("nome", $nome)), array(array("id", $idcategoria)));

			$json->add("success", "Categoria editada com sucesso!");
			$json->add("refresh", true);
		}
	}

	// Remove a categoria
	public function ApagarCategoria($idcategoria, $json)
	{
		$json->add("no-refresh", true);

		if(!!!$this->MostraCategoria("id", $idcategoria))
		{
			$json->add("error", "Categoria não existe!");
		}
		else
		{
			if(!!$this->sql->delete("postagem_categoria", array(array("id", $idcategoria))))
			{
				$json->add("success", "Categoria apagada com sucesso!");
			}
			else
			{
				$json->add("error", "Houve uma falha ao apagar a categoria, tente novamente!");
			}
		}
	}

	// Desvincula categorias da postagem
	public function DesvinculaCategorias($idpostagem)
	{
		return $this->sql->delete("postagem_categorias", array(array("idpostagem", $idpostagem)));
	}





	// Listar tags
	public function ListarTAGs()
	{
		$tags = $this->sql->select(array("*"), "postagem_tag");

		if(!!!$tags)
		{
			return array();
		}
		else
		{
			$rtn = array();

			foreach($tags as $tag)
			{
				$rtn[] = array(
					$tag->id,
					$tag->nome,
					$this->ContaLigacaoTAGPostagens($tag->id),
					"<div class=\"text-center\"><span class=\"btn btn-danger btn-minier btn-apagar\" data-url=\"".URL."Ajax/Postagem/TAG/Apagar/".$tag->id."\"><i class=\"fa fa-trash-o fa-fw\"></i></span></div>"
				);
			}

			return $rtn;
		}
	}

	// Conta quantas postagens uma tag está ligada
	public function ContaLigacaoTAGPostagens($idtag)
	{
		return $this->sql->count("postagem_tags", array(array("idtag", $idtag)));
	}





	// Controla a tag a ser inserida
	public function InsereTAG($idpostagem, $nome)
	{
		$idtag = 0;

		$tag = $this->MostraTAG("nome", $nome);

		if(!!$tag)
		{
			$idtag = $tag->id;
		}
		else
		{
			$idtag = $this->NovaTAG($nome);

			if(!!!$idtag)
			{
				return false;
			}
		}

		return !!$this->sql->insert("postagem_tags", array(array("idpostagem", $idpostagem), array("idtag", $idtag)));
	}

	// Mostra a tag
	public function MostraTAG($tipo, $valor)
	{
		return $this->sql->selectFirst(array("*"), "postagem_tag", array(array($tipo, $valor)));
	}

	// Adiciona a tag
	public function NovaTAG($nome)
	{
		return $this->sql->insert("postagem_tag", array(array("nome", $nome)));
	}

	// Remove a tag
	public function ApagarTAG($idtag, $json)
	{
		$json->add("no-refresh", true);

		if(!!!$this->MostraTAG("id", $idtag))
		{
			$json->add("error", "TAG não existe!");
		}
		else
		{
			if(!!$this->sql->delete("postagem_tag", array(array("id", $idtag))))
			{
				$json->add("success", "TAG apagada com sucesso!");
			}
			else
			{
				$json->add("error", "Houve uma falha ao apagar a tag, tente novamente!");
			}
		}
	}

	// Desvincula tags da postagem
	public function DesvinculaTAGs($idpostagem)
	{
		return $this->sql->delete("postagem_tags", array(array("idpostagem", $idpostagem)));
	}





	// Adiciona imagem
	public function AddImagem($postagem, $file, $json, $type)
	{
		$dir = false;

		if($type == "galeria")
		{
			$dir = "galeria";
		}
		else if($type == "capa")
		{
			$dir = "capa";
		}

		if(!!!$dir)
		{
			$json->add("error", "Tipo de imagem inválido! #2");
		}
		else
		{
			$upload = $this->_imagem->Upload($file, true, 1024, 1024, dirname(__FILE__)."/../../img/noticias/".$dir."/");

			if(!is_array($upload))
			{
				$json->add("error", "Falha no upload!");
			}
			else if(count($upload) < 2)
			{
				$json->add("error", "Falha no upload!");
			}
			else if($upload[0] == "error")
			{
				$json->add($upload[0], $upload[1]);
			}
			else if($upload[0] == "success")
			{
				$savename = $upload[1];
				$filename = $upload[2];

				$insert = false;

				if($type == "galeria")
				{
					$insert = $this->sql->insert("postagem_fotos", array(array("idpostagem", $postagem->id), array("imagem", $savename), array("criado", $this->sql->date())));
				}
				else if($type == "capa")
				{
					if($postagem->capa != "none.png")
					{
						$this->ApagaFoto($postagem->capa, "capa");
					}

					$insert = $this->sql->update("postagem", array(array("capa", $savename)), array(array("id", $postagem->id)));
				}

				if(!!!$insert)
				{
					$json->add("error", "Falha ao enviar a imagem <b>".$filename."</b>!");
					return false;
				}
				else
				{
					$json->add("success", "Imagem <b>".$filename."</b> enviada com sucesso!");
					return true;
				}
			}
		}

		return false;
	}

	// Adiciona imagem
	public function AddArquivo($postagem, $file, $json)
	{
		if($file["error"] != 0)
		{
			$json->add("error", "Falha ao enviar o arquivo <b>".$file["name"]."</b>!");
		}
		else
		{
			$name = strtoupper(time().uniqid());

			$src = $file["tmp_name"];
			$dst = dirname(__FILE__)."/../../files/".$name;

			if(!move_uploaded_file($src, $dst))
			{
				$json->add("error", "Houve uma falha ao mover o arquivo <b>".$file["name"]."</b>!");
			}
			else
			{
				$this->sql->insert("postagem_arquivo", array(array("idpostagem", $postagem->id), array("nome", $file["name"]), array("arquivo", $name), array("criado", $this->sql->date())));
				$json->add("success", "Arquivo <b>".$file["name"]."</b> enviado com sucesso!");
			}
		}
	}





	// Retorna as fotos da postagem
	public function ListarFotos($idpostagem)
	{
		$rtn = array();

		$fotos = $this->sql->select(array("*"), "postagem_fotos", array(array("idpostagem", $idpostagem)));

		if(!!$fotos)
		{
			foreach($fotos as $foto)
			{
				$rtn[] = $foto;
			}
		}

		return $rtn;
	}

	// Retorna fotos para exibição
	public function ExibeFotos($postagem)
	{
		$rtn = array();

		foreach($postagem->fotos as $foto)
		{
			$rtn[] = "<label class=\"postagem-foto\">".
				"<div class=\"postagem-foto-imagem\"><img src=\"".URL."img/noticias/galeria/".$foto->imagem."\"></div>".
				"<div class=\"postagem-foto-checkbox\">".
					"<div class=\"checkbox no-margin\">".
						"<input type=\"checkbox\" name=\"fotos[]\" value=\"".$foto->id."\" class=\"ace hidden\">".
						"<span class=\"lbl\"></span>".
					"</div>".
				"</div>".
			"</label>";
		}

		if(count($rtn) > 0)
		{
			$rtn = "<div class=\"well\"><div class=\"postagem-galeria\">".implode("", $rtn)."</div></div>";
		}
		else
		{
			$rtn = "<div class=\"alert alert-warning\">Nenhuma foto encontrada!</div>";
		}

		return $rtn;
	}

	// Retorna a foto da postagem
	public function MostraFoto($idpostagem, $idfoto)
	{
		return $this->sql->selectFirst(array("*"), "postagem_fotos", array(array("id", $idfoto), array("idpostagem", $idpostagem)));
	}

	// Apaga a foto
	public function ApagaFoto($foto, $path, $id = false)
	{
		$this->_imagem->Apagar(dirname(__FILE__)."/../../img/noticias/".$path."/".$foto);

		if($id)
		{
			$this->sql->delete("postagem_fotos", array(array("id", $id)));
		}
	}





	// Retorna as 5 últimas postagens
	public function GetLast5Postagens($search, $pagina)
	{
		$rtn = "";

		$postagens = $this->sql->select(array("id", "titulo"), "postagem", array(array("status", 6)), array(0, 5), array(array("id", "DESC")));

		if($postagens != null)
		{
			$rtn .= "<div class=\"postagens-last\">";

			$rtn .= "<div class=\"postagens-last-title\">Últimas 5 postagens</div>";

			foreach($postagens as $postagem)
			{
				$rtn .= "<a href=\"".URL."Blog/".$search."/".$pagina."/".$postagem->id."\" class=\"postagens-last-link on-click\"><span>".$postagem->titulo."</span></a>";
			}

			$rtn .= "</div>";
		}

		return $rtn;
	}
}

?>