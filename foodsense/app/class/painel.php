<?php

class Painel
{
	private $breadcrumb = array();

	private $control;
	private $sql;

	private $active = "";
	private $title = "";

	public $conta = false;

	private $menuActive = "";
	private $subMenuActive = "";
	private $menu = array();

	public function __construct($control, $page_termos = false)
	{
		$this->setControl($control);
		$this->setSQL($control->getSQL());

		// Inicia a sessão
		session_start();

		// Inicia o usuário
		$this->loadUsuario($page_termos);
	}

	// Breadcrumb
	public function addBreadcrumb($name, $link = "", $class = null)
	{
		$this->breadcrumb[] = array($name, $link, is_null($class) ? null : "<i class=\"".$class."\"></i>");
	}
	public function getBreadcrumbs()
	{
		$rtn = array();

		$last = array_pop($this->breadcrumb);

		foreach($this->breadcrumb as $id => $val)
			$rtn[] = "<li>".(is_null($val[2]) ? "" : $val[2]." ")."<a href=\"".URL.$val[1]."\">".$val[0]."</a></li>";

		$rtn[] = "<li class=\"active\">".(is_null($last[2]) ? "" : $last[2]." ")."".$last[0]."</li>";

		return implode("", $rtn);
	}

	// Control
	public function setControl($control)
	{
		$this->control = $control;
	}
	public function getControl()
	{
		return $this->control;
	}

	// SQL
	public function setSQL($sql)
	{
		$this->sql = $sql;
	}
	public function getSQL()
	{
		return $this->sql;
	}

	// Carrega o usuário logado
	public function loadUsuario($page_termos)
	{
		$login = $this->getSession("login");
		$senha = $this->getSession("senha");

		if(!!$login && !!$senha)
		{
			$conta = $this->sql->selectFirst(array("*"), "conta", array(array("login", $login), array("senha", $senha)));

			if(!!$conta)
			{
				$conta->aniversario = implode("/", array_reverse(explode("-", $conta->aniversario)));

				$termos = $this->sql->count("conta_termos", array(array("idconta", $conta->id)));

				if($page_termos || $termos > 0)
				{
					$this->conta = $conta;
				}
				else
				{
					$this->control->getRoute()->Redirect("Painel/Termos");
				}
			}
			else
			{
				$this->conta = false;
			}
		}
		else
		{
			$this->conta = false;
		}
	}

	// Active
	public function setActive($active)
	{
		$this->active = $active;
	}
	public function getActive()
	{
		return $this->active;
	}

	// Title
	public function setTitle($title)
	{
		$this->title = $title;
	}
	public function getTitle()
	{
		return $this->title;
	}

	// Menu Active
	public function setMenuActive($menuActive)
	{
		$this->menuActive = $menuActive;
	}
	public function getMenuActive()
	{
		return $this->menuActive;
	}

	// SbMenu Active
	public function setSubMenuActive($subMenuActive)
	{
		$this->subMenuActive = $this->menuActive."-".$subMenuActive;
	}
	public function getSubMenuActive()
	{
		return $this->subMenuActive;
	}

	// Menu
	public function addMenu($categorias, $text, $link, $icon, $active, $dropdown = null)
	{
		if(is_array($categorias))
		{
			if(count($categorias) > 0)
			{
				if(!in_array($this->conta->categoria, $categorias))
				{
					if($active == $this->menuActive)
					{
						$this->control->getRoute()->Redirect("Painel/Home");
					}

					return;
				}
			}
		}
		else
		{
			return;
		}

		if(!is_array($dropdown))
		{
			$this->menu[] = "<li class=\"".($active == $this->menuActive ? "active" : "")."\">".
				"<a href=\"".URL.$link."\"><i class=\"menu-icon ".$icon."\"></i><span class=\"menu-text\"> ".$text."</span></a>".
			"</li>";
		}
		else
		{
			$this->menu[] = "<li class=\"".($active == $this->menuActive ? "active open" : "")."\">".
				"<a href=\"#\" class=\"dropdown-toggle\"><i class=\"menu-icon ".$icon."\"></i><span class=\"menu-text\"> ".$text."</span><b class=\"arrow fa fa-angle-down\"></b></a>".
				"<b class=\"arrow\"></b>".
				"<ul class=\"submenu\">".implode("", array_map(function($el) use ($link, $active){
					$subMenu = $active."-".$el[2];
					return "<li".($subMenu == $this->subMenuActive ? " class=\"active\"" : "").">".
						"<a href=\"".URL.$link."/".$el[1]."\">".
							"<i class=\"menu-icon fa fa-caret-right\"></i> ".$el[0].
						"</a>".
						"<b class=\"arrow\"></b>".
					"</li>";
				}, $dropdown))."</ul>".
			"</li>";
		}
	}
	public function getMenu()
	{
		$this->addMenu(array(), "Página Inicial", "Painel/Home", "fa fa-tachometer fa-fw", "home");

		$this->addMenu(array(1), "Usuarios", "Painel/Usuarios", "fa fa-user-o fa-fw", "usuario", array(
			array("Novo usuário", "Novo", "novo"),
			array("Listar usuários", "Listar", "listar"),
		));

		$this->addMenu(array(), "Postagem", "Painel/Postagem", "fa fa-file-text-o fa-fw", "postagem", array(
			array("Nova postagem", "Nova", "nova"),
			array("Listar postagens", "Listar", "listar"),
			array("Nova categoria", "Categoria/Nova", "nova_categoria"),
			array("Listar categorias", "Categoria/Listar", "listar_categoria"),
			array("Listar TAGs", "TAGs/Listar", "listar_tags"),
		));

		return implode("", $this->menu);
	}

	// Sessão
	public function setSession($key, $val)
	{
		$_SESSION[$key] = $val;
	}
	public function getSession($key)
	{
		return isset($_SESSION[$key]) ? $_SESSION[$key] : false;
	}

	// Verifica se usuário está logado
	public function isLogged()
	{
		return !!$this->conta;
	}

	// Sai do usuário logada
	public function logout()
	{
		session_destroy();
		unset($_SESSION);
	}





	// Alerta de aniversáriante
	public function getAniversariantes()
	{
		$rtn = "";

		$aniversariantes = $this->sql->select(array("nome", "email", "telefone"), "conta", array(array("MONTH(aniversario)", date("m")), array("DAY(aniversario)", date("d"))));

		if($aniversariantes != null)
		{
			if(count($aniversariantes) > 0)
			{
				$rtn .= "<div class=\"widget-box widget-color-blue\">";
				$rtn .= "<div class=\"widget-header\"><h5 class=\"widget-title bigger lighter ui-sortable-handle\">Aniversáriantes de <b>".date("d/m")."</b></h5></div>";
				$rtn .= "<div class=\"widget-body\">";
				$rtn .= "<table class=\"table table-striped table-bordered table-hover table-middle no-margin-bottom\">";

				$rtn .= "<thead>
					<tr>
						<th>Nome</th>
						<th>E-mail</th>
						<th>Telefone</th>
					</tr>
				</thead>";

				$rtn .= "<tbody>";
				foreach($aniversariantes as $val)
				{
					$rtn .= "<tr>
						<td>".$val->nome."</td>
						<td>".$val->email."</td>
						<td>".$val->telefone."</td>
					</tr>";
				}
				$rtn .= "</tbody>";

				$rtn .= "</table>";
				$rtn .= "</div>";
				$rtn .= "</div>";

			}
		}

		if($rtn == "")
		{
			$rtn = "<div class=\"alert alert-info\">Nenhum aniversário para hoje, <b>".date("d/m")."</b>!</div>";
		}

		return $rtn;
	}
}

?>