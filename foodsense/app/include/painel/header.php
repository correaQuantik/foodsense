<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?= TITLE." | ".$this->painel->getTitle(); ?></title>
	<!-- Icon -->
	<link rel="shortcut icon" href="<?= URL."img/icon.png"; ?>">
	<!-- Font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?= URL."css/geral/font-awesome"; ?>">
	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?= URL."css/geral/bootstrap"; ?>">
	<!-- DatePicker -->
	<link rel="stylesheet" href="<?= URL."css/geral/datepicker"; ?>">
	<!-- DateTimePicker -->
	<link rel="stylesheet" href="<?= URL."css/geral/datetimepicker"; ?>">
	<!-- Full calendar -->
	<link rel="stylesheet" href="<?= URL."css/geral/fullcalendar"; ?>">
	<!-- Theme -->
	<link rel="stylesheet" href="<?= URL."css/geral/ace"; ?>">
	<link rel="stylesheet" href="<?= URL."css/geral/ace-skins"; ?>">
	<link rel="stylesheet" href="<?= URL."css/geral/ace-rtl"; ?>">
	<!-- Chosen -->
	<link rel="stylesheet" href="<?= URL."css/geral/chosen"; ?>">
	<!-- Fancybox -->
	<link rel="stylesheet" href="<?= URL."css/geral/jquery.fancybox"; ?>">
	<!-- Tag input -->
	<link rel="stylesheet" href="<?= URL."css/painel/tagsinput.jquery"; ?>">
	<!-- SweetAlert -->
	<link rel="stylesheet" href="<?= URL."css/geral/sweetalert"; ?>">
	<!-- WB -->
	<link rel="stylesheet" href="<?= URL."css/geral/style"; ?>">
	<link rel="stylesheet" href="<?= URL."css/painel/style"; ?>">
</head>
<body class="no-skin" data-url="<?= URL; ?>">
	<div id="navbar" class="navbar navbar-default ace-save-state">
		<div id="navbar-container" class="navbar-container ace-save-state container">
			<button type="button" id="menu-toggler" class="navbar-toggle menu-toggler pull-left" data-target="#sidebar">
				<span class="sr-only">Toggle sidebar</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>

			<div class="navbar-header pull-left">
				<a href="<?= URL."Painel/Home"; ?>" class="navbar-brand"><img src="<?= URL."img/quantik.png"; ?>"></a>
			</div>

			<div class="navbar-buttons navbar-header pull-right" role="navigation">
				<ul class="nav ace-nav">
					<li class="light-gray dropdown-modal">
						<a data-toggle="dropdown" href="#" class="dropdown-toggle">
							<img class="nav-user-photo" src="<?= URL."img/".($this->painel->conta->foto == "" ? "none.jpg" : "usuarios/".$this->painel->conta->foto); ?>">
							<span class="user-info">
								<small>Seja bem-vindo,</small><span> <?= $this->painel->conta->nome; ?></span>
							</span>
							<i class="ace-icon fa fa-caret-down"></i>
						</a>

						<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
							<li><a href="<?= URL."Painel/Perfil"; ?>"><i class="ace-icon fa fa-user fa-fw"></i> Perfil</a></li>
							<li><a href="<?= URL."Painel/Sair"; ?>"><i class="ace-icon fa fa-power-off fa-fw"></i> Sair</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div id="main-container" class="main-container ace-save-state container">
		<div id="sidebar" class="sidebar responsive ace-save-state">
			<ul class="nav nav-list"><?= $this->painel->getMenu(); ?></ul>
		</div>

		<div class="main-content">
			<div class="main-content-inner">
				<div id="breadcrumbs" class="breadcrumbs ace-save-state">
					<ul class="breadcrumb"><?= $this->painel->getBreadcrumbs(); ?></ul>
				</div>

				<div class="page-content">
					<div class="page-header">
						<h1><?= $this->painel->getTitle(); ?></h1>
					</div>