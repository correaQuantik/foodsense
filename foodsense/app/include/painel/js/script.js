$(function(){

	// Objetos mask

	$.loadMask();

	// Tooltip

	$(".toolT").tooltip();

	// Tags input

	$(".tags").tagsInput({
		defaultText: "Adicionar TAG"
	});

	// DataTable

	$.extend($.fn.dataTable.defaults, {
		oLanguage: {
			oAria: {
				sSortAscending: ": activate to sort column ascending",
				sSortDescending: ": activate to sort column descending"
			},
			oPaginate: {
				sFirst: "Início",
				sLast: "Último",
				sNext: "Próximo",
				sPrevious: "Anterior"
			},
			sEmptyTable: "Nenhum dado disponível",
			sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
			sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
			sInfoFiltered: "(filtrado de _MAX_ registros)",
			sInfoPostFix: "",
			sDecimal: "",
			sThousands: ",",
			sLengthMenu: "Mostrar _MENU_ registros por página",
			sLoadingRecords: "Carregando...",
			sProcessing: "Processando...",
			sSearch: "Pesquisar: ",
			sSearchPlaceholder: "",
			sUrl: "",
			sZeroRecords: "Nenhum registro encontrado"
		}
	});

	// Objetos DataTable

	var dataTableTimer = null;

	var dataTables = $(".datatable").DataTable({
		"columnDefs": [
			{
				targets: "no-sort",
				orderable: false,
			}
		]
	});

	function ReloadDataTables(){
		dataTables.ajax.reload(WaitTimeForDataTableReload, false);
	} ReloadDataTables();

	function WaitTimeForDataTableReload(){
		dataTableTimer = setTimeout(ReloadDataTables, 3000);
	}

	function StopDataTableReload(){
		if(dataTableTimer != null)
		{
			clearTimeout(dataTableTimer);
		}
	}

	// Objetos DatePicker

	$(".datepicker").datepicker({
		dayNames: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
		dayNamesMin: ["D", "S", "T", "Q", "Q", "S", "S", "D"],
		dayNamesShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb", "Dom"],
		monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
		monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
		nextText: "Próximo",
		prevText: "Anterior",
		format: "dd/mm/yyyy",
		todayHighlight: true,
		autoclose: true,
	});

	// Objetos DateTimePicker

	$(".datetimepicker").datetimepicker({
		format: "DD/MM/YYYY HH:mm:ss",
		locale: "pt-br",
		icons: {
			time: 'fa fa-clock-o',
			date: 'fa fa-calendar',
			up: 'fa fa-chevron-up',
			down: 'fa fa-chevron-down',
			previous: 'fa fa-chevron-left',
			next: 'fa fa-chevron-right',
			today: 'fa fa-arrows ',
			clear: 'fa fa-trash',
			close: 'fa fa-times'
		}
	});

	// Chosen

	$(".chosen").chosen({
		placeholder_text_multiple: "Selecione o que deseja",
		placeholder_text_single: "Selecione o que deseja",
		no_results_text: "Nenhum dado encontrado",
		width: "100%",
	});

	// Fancybox

	$(".fancybox").fancybox();

	// Fiz /n nas textarea

	$("textarea").each(function(){
		this.value = this.value.replace(/<br \/>/gi, "\n");
	});

	// Envia form

	$(document).on("submit", "form", function(e){
		e.preventDefault();

		var form = $(this);
		var btn = form.find("button");
		var msg1 = btn.find(".msg1");
		var msg2 = btn.find(".msg2");
		var sending = !!msg1.hasClass("hidden");

		if(sending)
			return;

		$.sendForm(form, {
			beforeSend: function(){
				btn.attr("disabled", true);
				msg1.addClass("hidden");
				msg2.removeClass("hidden");
			},
			success: function(ans){
				if(ans["modal"] != undefined && ans["success"] != undefined)
				{
					$.alert({
						type: "success",
						text: ans["success"]
					});

					form[0].reset();
					form.find(".chosen").val("").trigger("chosen:updated");

					modal.modal("hide");

					btn.attr("disabled", false);
					msg1.removeClass("hidden");
					msg2.addClass("hidden");
				}
				else if(ans["success"] != undefined)
				{
					if(ans["files"] != undefined)
					{
						var files = [];

						form.find("input[type='file']").each(function(){
							var input = this;
							for(var i = 0; i < this.files.length; i++)
							{
								files.push({
									file: this.files[i],
									type: input.name.split("[")[0]
								});
							}
						});

						$.alert({
							text: ans["success"],
							type: "success",
							timeout: 0
						});

						if(files.length > 0)
						{
							var action = form[0].action;

							if(ans["id"] != undefined)
							{
								action += "/" + ans["id"];
							}

							$.sendFormFiles(files, action, ans["url"]);
						}
						else
						{
							setTimeout(function(){
								location.href = ans["url"];
							}, 3000);
						}
					}
					else if(ans["refresh"] != undefined)
					{
						console.log("ALO");

						$.alert({
							type: "success",
							text: ans["success"],
							timeout: 0
						});

						setTimeout(function(){
							location.reload();
						}, 3000);
					}
					else if(ans["norefresh"] != undefined)
					{
						$.alert({
							type: "success",
							text: ans["success"]
						});

						btn.attr("disabled", false);
						msg1.removeClass("hidden");
						msg2.addClass("hidden");
					}
					else
					{
						$.alert({
							type: "success",
							text: ans["success"]
						});

						setTimeout(function(){
							location.href = ans["url"];
						}, 3000);
					}
				}
				else if(ans["warning"] != undefined)
				{
					$.alert({
						type: "warning",
						text: ans["warning"]
					});

					btn.attr("disabled", false);
					msg1.removeClass("hidden");
					msg2.addClass("hidden");
				}
				else if(ans["error"] != undefined)
				{
					$.alert({
						type: "danger",
						text: ans["error"]
					});

					btn.attr("disabled", false);
					msg1.removeClass("hidden");
					msg2.addClass("hidden");
				}
				else
				{
					btn.attr("disabled", false);
					msg1.removeClass("hidden");
					msg2.addClass("hidden");
				}
			}
		});
	});

	// Apaga dado

	$(document).on("click", ".btn-apagar", function(){
		var btn = $(this);

		StopDataTableReload();

		btn.attr("disabled", true);

		DeleteBeforeAjax("Deseja continuar?", null, btn.attr("data-url"), function(){
			btn.attr("disabled", false);
			ReloadDataTables();
		});
	});

	function DeleteBeforeAjax(title, text, url, onEnd){
		swal({
			title: title,
			text: text,
			type: "warning",
			confirmButtonColor: "#F8BB86",
			showCancelButton: true,
			closeOnConfirm: false,
			showLoaderOnConfirm: true,
			html: true,
		},
		function(button){
			if(!button)
			{
				ReloadDataTables();
			}
			else
			{
				$.sendAjax({
					url: url,
					success: function(ans){
						DeleteAfterAjax(ans, onEnd);
					}
				});
			}
		});
	}

	function DeleteAfterAjax(ans, onEnd){
		var end = false;

		if(ans["success"] != undefined)
		{
			$.alert({
				type: "success",
				text: ans["success"]
			});

			if(ans["norefresh"] != undefined)
			{
				end = true;
			}
			else if(ans["refresh"] != undefined)
			{
				setTimeout(function(){
					location.reload();
				}, 3000);
			}
			else if(ans["url"] != undefined)
			{
				setTimeout(function(){
					location.href = ans["url"];
				}, 3000);
			}
			else
			{
				end = true;
			}
		}
		else if(ans["error"] != undefined)
		{
			$.alert({
				type: "danger",
				text: ans["error"]
			});

			end = true;
		}
		else if(ans["confirm"] != undefined && ans["text"] != undefined && ans["url"] != undefined)
		{
			DeleteBeforeAjax(ans["confirm"], ans["text"], ans["url"], onEnd);
		}
		else
		{
			end = true;
		}

		if(end)
		{
			swal.close();

			if($.isFunction(onEnd))
			{
				onEnd();
			}
		}
	}

	// Gerencia comentários do Blog

	$(document).on("click", ".blog-comment", function(e){
		e.preventDefault();

		var btn = $(this);

		var id = btn.attr("data-id");
		var type = btn.attr("data-type");

		if(id != undefined && type != undefined)
		{
			id = Number(id);
			type = Number(type);

			if(!isNaN(id) && !isNaN(type))
			{
				if(type < 0 || type > 1)
					return;

				btn.attr("disabled", true);

				swal({
					title: (type == 1 ? "Deseja aprovar este comentário?" : "Deseja apagar este comentário?"),
					type: "warning",
					confirmButtonColor: "#F8BB86",
					showCancelButton: true,
					closeOnConfirm: false,
					showLoaderOnConfirm: true,
				}, function(button){
					if(button)
					{
						var url = $("body").attr("data-url");

						if(url == undefined)
						{
							swal.close();
						}
						else
						{
							$.ajax({
								url: url+"Ajax/Postagem/Comentario",
								data: {
									id: id,
									type: type
								},
								dataType: "JSON",
								type: "POST",
								success: function(ans){
									btn.attr("disabled", false);

									if(ans["error"] != undefined)
									{
										swal({
											title: ans["error"],
											type: "error",
											confirmButtonColor: "#F27474",
											closeOnConfirm: true,
										});
									}
									else if(ans["success"] != undefined)
									{
										swal({
											title: ans["success"],
											type: "success",
											confirmButtonColor: "#A5DC86",
											closeOnConfirm: true,
										}, function(){
											if(ans["reload"] != undefined)
											{
												location.reload();
											}
										});
									}
								}
							});
						}
					}
				});
			}
		}
	});

});