				</div>
			</div>
		</div>

		<div class="footer">
			<div class="footer-inner">
				<div class="footer-content">
					<span class="bigger-120">Painel administrativo criado e desenvolvido por <a href="http://quantik.com.br/" target="_blank">Agência Quantik</a>.</span>
				</div>
			</div>
		</div>

		<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
			<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
		</a>
	</div>

	<!-- GSAP -->
	<script src="<?= URL."js/geral/TweenMax"; ?>"></script>
	<script src="<?= URL."js/geral/TweenMax.scrollTo"; ?>"></script>
	<!-- jQuery -->
	<script src="<?= URL."js/geral/jquery"; ?>"></script>
	<!-- Moment -->
	<script src="<?= URL."js/geral/moment"; ?>"></script>
	<!-- Mask -->
	<script src="<?= URL."js/geral/mask.jquery"; ?>"></script>
	<!-- Bootstrap -->
	<script src="<?= URL."js/geral/bootstrap"; ?>"></script>
	<!-- Theme -->
	<script src="<?= URL."js/geral/ace"; ?>"></script>
	<script src="<?= URL."js/geral/ace-elements"; ?>"></script>
	<!-- DataTable -->
	<script src="<?= URL."js/geral/jquery.dataTables"; ?>"></script>
	<script src="<?= URL."js/geral/jquery.dataTables.bootstrap"; ?>"></script>
	<!-- DatePicker -->
	<script src="<?= URL."js/geral/datepicker"; ?>"></script>
	<!-- DateTimePicker -->
	<script src="<?= URL."js/geral/datetimepicker"; ?>"></script>
	<!-- Full calendar -->
	<script src="<?= URL."js/geral/fullcalendar"; ?>"></script>
	<script src="<?= URL."js/geral/fullcalendar-lang"; ?>"></script>
	<!-- Modal -->
	<script src="<?= URL."js/geral/bootbox"; ?>"></script>
	<!-- Chosen -->
	<script src="<?= URL."js/geral/chosen"; ?>"></script>
	<!-- Fancybox -->
	<script src="<?= URL."js/geral/jquery.fancybox"; ?>"></script>
	<!-- HTML2CANVAS -->
	<script src="<?= URL."js/painel/html2canvas"; ?>"></script>
	<!-- CKEditor -->
	<script src="<?= URL."ckeditor/ckeditor.js"; ?>"></script>
	<!-- Tag input -->
	<script src="<?= URL."js/painel/tagsinput.jquery"; ?>"></script>
	<!-- SweetAlert -->
	<script src="<?= URL."js/geral/sweetalert"; ?>"></script>
	<!-- WB -->
	<script src="<?= URL."js/geral/main"; ?>"></script>
	<script src="<?= URL."js/geral/script"; ?>"></script>
	<script src="<?= URL."js/painel/script"; ?>"></script>
</body>
</html>