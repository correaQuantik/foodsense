	<div class="preFooter">
		<div class="newsletter">
			<h3>INSCREVA-SE NA NOSSA <b>NEWSLETTER</b></h3>
			<form>
				<input type="text" name="nome">
				<input type="email" name="email">
				<input type="submit" value="Enviar" name="">
			</form>
		</div>
		<div class="postagensFooter">
			<div class="">
				<div>
					<p>Últimas postagens</p>
					<span>#<b>FOOD</b>SENSE<span>BRASIL</span></span>
				</div>
				<div>
					<div><a>Titulo do post - tal tal tal tal tal</a></div>
					<div><a>Titulo do post - tal tal tal tal tal</a></div>
					<div><a>Titulo do post - tal tal tal tal tal</a></div>
					<div><a>Titulo do post - tal tal tal tal tal</a></div>
					<div><a>Titulo do post - tal tal tal tal tal</a></div>
					<div><a>Titulo do post - tal tal tal tal tal</a></div>
					<div><a>Titulo do post - tal tal tal tal tal</a></div>
				</div>
			</div>
		</div>
		<div class="testeCal">
			<div>
				<h1>Quer saber quantas <b>calorias</b> tem sua <b>refeição?</b></h1>
				<a href="#" class="btn btn-warning">Clique aqui!</a>
			</div>
		</div>
	</div>
	<div class="footer">
		<div class="container">
			<div class="footer-cell menu">
				<div class="footer-menu-link"><a href="<?= URL."Home"; ?>" class="on-click">HOME</a></div>
				<div class="footer-menu-link"><a href="<?= URL."Sobre"; ?>" class="on-click">SOBRE</a></div>
				<div class="footer-menu-link"><a href="<?= URL."Produtos"; ?>" class="on-click">PRODUTOS</a></div>
				<div class="footer-menu-link"><a href="<?= URL."Blog/Todas/1"; ?>" class="on-click">BLOG</a></div>
				<div class="footer-menu-link"><a href="<?= URL."Parceiros"; ?>" class="on-click">PARCEIROS</a></div>
				<div class="footer-menu-link"><a href="<?= URL."Contato"; ?>" class="on-click">CONTATO</a></div>
			</div>

			<div class="footer-cell menu">
				<div class="footer-menu-link"><a href="<?= URL."QualSeuEstilo"; ?>" class="on-click">QUAL O SEU ESTILO?</a></div>
				<div class="footer-menu-link"><a href="<?= URL."Diferenciais"; ?>" class="on-click">DIFERENCIAIS</a></div>
				<div class="footer-menu-link"><a href="<?= URL.""; ?>" class="on-click">POLÍTICA DE PRIVACIDADE</a></div>
				<div class="footer-menu-link"><a href="<?= URL.""; ?>" class="on-click">SUGESTÕES E RECLAMAÇÕES</a></div>
				<div class="footer-menu-link"><a href="<?= URL.""; ?>" class="on-click">TRABALHE CONOSCO</a></div>
				<div class="footer-menu-link"><a href="<?= URL.""; ?>" class="on-click">OBTENHA VANTAGENS</a></div>
			</div>

			<div class="footer-cell social">
				<div>
					<a href="" class="footer-social"><img src="<?= URL."img/icon-facebook.png"; ?>"></a>
					<a href="" class="footer-social"><img src="<?= URL."img/icon-instagram.png"; ?>"></a>
				</div>
				
				<div>
					<a href="" class="footer-social"><img src="<?= URL."img/icon-google-plus.png"; ?>"></a>
					<a href="" class="footer-social"><img src="<?= URL."img/icon-tripadvisor.png"; ?>"></a>
				</div>
			</div>

			<div class="footer-cell facebook">
				<div class="facebook-plugin"></div>
			</div>
		</div>
	</div>

	<div id="scripts" class="hidden">
		<!-- TweenMax -->
		<script type="text/javascript" src="<?= URL."js/geral/TweenMax"; ?>"></script>
		<script type="text/javascript" src="<?= URL."js/geral/morphSVG"; ?>"></script>
		<!-- jQuery -->
		<script type="text/javascript" src="<?= URL."js/geral/jquery"; ?>"></script>
		<!-- Fancybox -->
		<script src="<?= URL."js/geral/jquery.fancybox"; ?>"></script>
		<!-- Owl Carousel -->
		<script type="text/javascript" src="<?= URL."js/home/owl.carousel"; ?>"></script>
		<!-- Mask -->
		<script type="text/javascript" src="<?= URL."js/geral/mask.jquery"; ?>"></script>
		<!-- Quantik -->
		<script type="text/javascript" src="<?= URL."js/home/animation"; ?>"></script>
		<script type="text/javascript" src="<?= URL."js/home/script"; ?>"></script>
		<!-- Tooltipster -->
		<script type="text/javascript" src="<?= URL."js/geral/tooltipster.bundle"; ?>"></script>
	</div>
</body>
</html>