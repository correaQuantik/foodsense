$(function(){

	//Calendario
	$(".calArrow").on("click",function(e) {
		if ($(this).child(0).hasClass("fa-arrow-left")) {
			alert("left");
		}
		else if ($(this).child(0).hasClass("fa-arrow-right")) {
			alert("right");
		}
	});

	//Tooltip
	$('.tooltipster').tooltipster({
	    theme: 'tooltipster-shadow'
	});

	var hiddenCalendar = false;

	$(".on-click.navbar-calendar").on("click",function() {

			$(".calendar").slideToggle("fast");

	});

	// Owl

	$(".header-slider").owlCarousel({
		dotsContainer: ".header-dots>.container",
		autoplaySpeed: 1500,
		dotsSpeed: 1500,
		autoplay: true,
		items: 1,
		loop: true
	});

	$(".item-slider").owlCarousel({
		dotsContainer: ".item-dots>.container",
		autoplaySpeed: 1500,
		dotsSpeed: 1500,
		autoplay: true,
		items: 1,
		loop: true
	});

	// Load page

	$(window).bind("load", function(){
		TweenMax.set("body > *", {
			autoAlpha: 0
		});

		var anim = [];

		$("body").removeClass("loading");

		$("body > *").each(function(){
			if(!$(this).hasClass("hidden") && !$(this).hasClass("responsive"))
			{
				anim.push(this);
			}
		});

		TweenMax.to(anim, 0.3, {
			autoAlpha: 1
		});
	});

	// OnClick

	var redirect = false;

	$(document).on("click", ".on-click", function(e){
		var el = $(this);

		var isButton = (el.is("a") || el.is("button"));

		if(isButton)
		{
			if(el.hasClass("clicked"))
			{
				if(el.is("a"))
					location.href = el.attr("href");

				return true;
			}
			else
			{
				redirect = true;
				el.addClass("clicked");
				e.preventDefault();
			}
		}
		else
		{
			if(redirect)
				return false;
		}

		var ball = $("<div class=\"ball\"></div>");

		var left = e.pageX - el.offset().left;
		var top = e.pageY - el.offset().top;

		TweenMax.set(ball, {
			height: 0,
			width: 0,
			left: left,
			top: top,
		});

		var height = el.outerHeight();
		var width = el.outerWidth();
		var size = (height > width ? height : width);
		size *= 3;

		el.append(ball);

		TweenMax.to(ball, 0.5, {
			autoAlpha: 0,
			height: size,
			width: size,
			onUpdate: function(){
				ball.css({
					"margin-left": 0 - (ball.width() / 2),
					"margin-top": 0 - (ball.height() / 2)
				});
			},
			onComplete: function(){
				ball.remove();

				if(isButton && el.hasClass("clicked"))
				{
					el.trigger("click");
				}
			}
		});
	});

	function RemoveClicked(){
		$(".clicked").removeClass("clicked");

		$("form button.on-click").find(".icon1").removeClass("hidden");
		$("form button.on-click").find(".icon2").addClass("hidden");

		redirect = false;
	}

	// Navbar logo

	if($(".navbar").length > 0)
	{
		if(!$(".navbar").hasClass("static"))
		{
			var isLogoVisible = false;
			var logoAnim = null;

			TweenMax.set(".navbar-logo", {
				autoAlpha: 0
			});

			$(window).scroll(function(){
				var scrollTop = $(window).scrollTop();
				var headerHeight = $(".header").outerHeight() / 2;

				if(!isLogoVisible && scrollTop >= headerHeight)
				{
					if(logoAnim != null)
					{
						logoAnim.kill();
						logoAnim = null;
					}

					isLogoVisible = true;

					logoAnim = TweenMax.to(".navbar-logo", 0.2, {
						className: "+=visible",
						onComplete: function(){
							logoAnim = TweenMax.to(".navbar-logo", 0.2, {
								autoAlpha: 1
							});
						}
					});
				}
				else if(isLogoVisible && scrollTop < headerHeight)
				{
					if(logoAnim != null)
					{
						logoAnim.kill();
						logoAnim = null;
					}

					isLogoVisible = false;

					logoAnim = TweenMax.to(".navbar-logo", 0.2, {
						autoAlpha: 0,
						onComplete: function(){
							logoAnim = TweenMax.to(".navbar-logo", 0.2, {
								className: "-=visible"
							});
						}
					});
				}
			});
		}
	}

	// Start responsive menu

	function StartResponsiveMenu(){
		var responsive = $(".responsive");
		var responsive_box = $(".responsive-box");

		TweenMax.set(responsive, {
			autoAlpha: 0
		});

		TweenMax.set(responsive_box, {
			autoAlpha: 0
		});

		responsive.toggleClass("hidden", false);

		responsive.toggleClass("static", $(".navbar").hasClass("static"));

		$(document).on("click", ".navbar-responsive svg", function(){
			var svg = $(this);

			if(!svg.hasClass("in_anim"))
			{
				svg.toggleClass("in_anim", true);

				var path1 = svg.find(".path-1");
				var path2 = svg.find(".path-2");

				if(!!svg.hasClass("active"))
				{
					TweenMax.to(path1, 0.25, {
						morphSVG: path1,
						onComplete: function(){
							svg.toggleClass("active", false);
							svg.toggleClass("in_anim", false);
						}
					});
					TweenMax.to(responsive, 0.25, {
						autoAlpha: 0,
						onComplete: function(){
							TweenMax.set(responsive_box, {
								autoAlpha: 0
							});
						}
					});
				}
				else
				{
					TweenMax.to(path1, 0.25, {
						morphSVG: path2,
						onComplete: function(){
							svg.toggleClass("active", true);
							svg.toggleClass("in_anim", false);
						}
					});
					TweenMax.to(responsive, 0.25, {
						autoAlpha: 1,
						onComplete: function(){
							TweenMax.to(responsive_box, 0.25, {
								autoAlpha: 1
							});
						}
					});
				}
			}
		});
	} StartResponsiveMenu();

	// Google Maps

	$(window).bind("load", function(){
		if($("#map").length > 0)
		{
			$.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyDnAw06l_BTuWefSCWKDKctgRDp0dMrkgA", function(){
				var map = new google.maps.Map(document.getElementById("map"), {
					scrollwheel: false,
					center: {
						lat: -23.037088,
						lng: -45.583863
					},
					zoom: 19
				});
			});
		}
	});

	// Forms

	var sending = false;

	// Submit Blog search

	$(document).on("click", "form button.on-click", function(){
		$(this).find(".icon1").addClass("hidden");
		$(this).find(".icon2").removeClass("hidden");
	});

	$(document).on("submit", ".postagens-search", function(e){
		e.preventDefault();

		var valor = $(".postagens-cell input").val();

		if(valor == "")
			valor = "Todas";

		location.href = $(this).attr("action").replace("Todas", valor);
	});

	// Submit Blog comment

	$(document).on("submit", ".postagem-view-comment-form", function(e){
		e.preventDefault();

		if(sending)
			return;
		else
			sending = true;

		var form = $(this);

		form.find("input[name='url']").val(location.href);

		$.ajax({
			url: form.attr("action"),
			data: form.serialize(),
			dataType: "JSON",
			type: "POST",
			success: function(ans){
				RemoveClicked();

				sending = false;

				if(ans["url"] != undefined)
				{
					location.href = ans["url"];
				}
				else if(ans["error"] != undefined)
				{
					$.alert({
						type: "error",
						text: ans["error"]
					});
				}
				else if(ans["success"] != undefined)
				{
					$.alert({
						type: "success",
						text: ans["success"]
					});

					form.find("textarea").val("");
				}
			}
		});
	});

	// Fix content height

	function FixContentHeight(){
		var navbar = $(".navbar");
		var content = $(".content.fill");
		var footer = $(".footer");

		content.css({
			"min-height": $(window).height() - navbar.outerHeight() - footer.outerHeight()
		});
	} FixContentHeight();

	$(window).bind("load resize", function(){
		FixContentHeight();
	});

	// Footer facebook plugin

	var fb_width = 0;

	$(window).bind("load resize", function(){
		var plugin = $(".facebook-plugin");
		var width = plugin.width();

		if(fb_width != width)
		{
			fb_width = width;
			plugin.html("<iframe src=\"https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Ffoodsensebrasil%2F&tabs&width="+width+"&height=154&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1646813122220455\" width=\""+width+"\" height=\"154\" style=\"border:none;overflow:hidden\" scrolling=\"no\" frameborder=\"0\" allowTransparency=\"true\"></iframe>")
		}
	});


	
		//Start method
		if ($("#categoriaSelect").length > 0) {
		UpdateShownCateg();

		$(":checkbox").prop("checked",false);
		jsonFile["categoria"] = $("#categoriaSelect").val();

		//binds
		$("#categoriaSelect").on("change",function () {
			UpdateShownCateg();
			$(":checkbox").prop("checked",false);
			ClearJson();
		});


		$(":checkbox").not("input[name='size'],input[name='insertCarrinho']").click(function (e){
			UpdateJson(e);
		});

		$("input[name='size']").click(function() {
			$(".guarnição:checked").each(function (e) {
				UpdateSize(this);
			});
		});

		$("input[value='Finalizar']").click(function (e) {
			if (totalCalories == 0) {
				e.preventDefault();
				alert("Seu prato está vazio!");
			}
		});

		$(":checkbox[name='insertCarrinho']").click(function(e){
			jsonFile["carrinho"] = $(":checkbox[name='insertCarrinho']").is(":checked");
		});

		function UpdateSize (e) {
			var title = $(e).val();

			totalCalories -= selectedCheckboxes["guarnição"][title];

			if ($("input[name='size']:checked").val() == "m") {
				var value = parseInt(allCheckboxes[title].split("/")[0]);
			}
			else {
				var value = parseInt(allCheckboxes[title].split("/")[1]);
			}

			totalCalories += value;
			selectedCheckboxes["guarnição"][title] = value;

			var jsonFile = {};

			jsonFile["categoria"] = $("#categoriaSelect").val();
			jsonFile["totalCaloria"] = totalCalories;
			jsonFile["items"] = selectedCheckboxes;

			$("#totalCalories").html(totalCalories);
			$("input[name='jsonData']").val(JSON.stringify(jsonFile,null,2));
		}

		function ClearJson() {
			totalCalories = 0;

			var jsonFile = {};

			selectedCheckboxes["guarnição"] = {};
			selectedCheckboxes["opcional"] = {};
			selectedCheckboxes["extra"] = {};
			selectedCheckboxes["papinhaS"] = {};
			selectedCheckboxes["papinhaF"] = {};
			selectedCheckboxes["igrediente"] = {};
			selectedCheckboxes["molho"] = {};

			jsonFile["categoria"] = $("#categoriaSelect").val();
			jsonFile["totalCaloria"] = totalCalories;
			jsonFile["items"] = selectedCheckboxes;
			jsonFile["carrinho"] = $(":checkbox[name='insertCarrinho']").is(":checked");

			$("#totalCalories").html(totalCalories);
			$("input[name='jsonData']").val(JSON.stringify(jsonFile,null,2));
		}

		function UpdateJson(e) {
			var title = $(e.target).val();
			var typeOfFood = e.target.className;
			var categoria = $("#categoriaSelect").val();

			var Searchquantity = "." + typeOfFood +".number." + categoria;
			var quantity = parseInt($( Searchquantity ).text());

			if (Object.keys(selectedCheckboxes[typeOfFood]).length + 1 > quantity && $(e.target).is(":checked")) {
				alert ("Por favor selecione no máximo " + quantity);
				$(e.target).prop("checked",false);
				return null;
			}
			else if (typeOfFood == "papinhaS" || typeOfFood == "papinhaF") {
				if ((Object.keys(selectedCheckboxes["papinhaS"]).length + Object.keys(selectedCheckboxes["papinhaF"]).length ) + 1 > 1 && $(e.target).is(":checked")) {
					alert ("Por favor selecione no máximo 1");
					$(e.target).prop("checked",false);
					return null;
				}
			}


			if (typeOfFood == "guarnição" && !Number.isInteger(allCheckboxes[title])) {
				if ($("input[name='size']:checked").val() == "m") {
					var value = parseInt(allCheckboxes[title].split("/")[0]);
				}
				else {
					var value = parseInt(allCheckboxes[title].split("/")[1]);
				}
			}
			else if (typeOfFood == "igrediente") {
				var value = allCheckboxes[title.substring(0,title.length-1)];
			}
			else {
				var value = allCheckboxes[title];				
			}

			if ($(".sum." + categoria).length > 0 && Object.keys(selectedCheckboxes[typeOfFood]).length > 0) {
				if ($(e.target).is(":checked") || Object.keys(selectedCheckboxes[typeOfFood]).length > 1){
					value = 0;
				}

				
			}

			if ($(e.target).is(":checked")) {
				totalCalories += value;
				selectedCheckboxes[typeOfFood][title] = value;
			}
			else {
				totalCalories -= value;
				delete selectedCheckboxes[typeOfFood][title];
			}

			//cria o arquivo json
			var jsonFile = {};

			jsonFile["categoria"] = categoria;
			jsonFile["totalCaloria"] = totalCalories;
			jsonFile["items"] = selectedCheckboxes;
			jsonFile["carrinho"] = $(":checkbox[name='insertCarrinho']").is(":checked");

			$("#totalCalories").html(totalCalories);
			$("input[name='jsonData']").val(JSON.stringify(jsonFile,null,2));
		}

		function UpdateShownCateg () {
			var ToShow = $("#categoriaSelect").val();

			var boxes = $(".customCheck");

			for (var count = 0; count < boxes.length; count++) {
				if (!$(boxes[count]).hasClass(ToShow)) {
					$(boxes[count]).fadeOut();
				}
				else {
					$(boxes[count]).fadeIn();
				}
			}

			$(".customCheck").parent().parent().fadeOut();
			$(".customCheck."+ToShow).parent().parent().fadeIn();
		}
	}
});