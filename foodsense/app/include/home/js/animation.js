$(function(){

	// Hover

	$(document).on("mouseenter", ".hover", function(){
		TweenMax.to(this, 0.2, {
			className: "+=on"
		});
	}).on("mouseleave", ".hover", function(){
		TweenMax.to(this, 0.2, {
			className: "-=on"
		});
	});

	// Hover all

	$(document).on("mouseenter", ".hover-all", function(){
		TweenMax.to([this, $(this).find("*")], 0.2, {
			className: "+=on"
		});
	}).on("mouseleave", ".hover-all", function(){
		TweenMax.to([this, $(this).find("*")], 0.2, {
			className: "-=on"
		});
	});

	// Focus

	$(document).on("focusin", ".focus, .focus *", function(){
		var parent = $(this).parents(".focus-all");
		var elem = parent.length > 0 ? [parent, parent.find("*")] : [this, $(this).find("*")];
		TweenMax.to(elem, 0.2, {
			className: "+=in"
		});
	}).on("focusout", ".focus, .focus *", function(){
		var parent = $(this).parents(".focus-all");
		var elem = parent.length > 0 ? [parent, parent.find("*")] : [this, $(this).find("*")];
		TweenMax.to(elem, 0.2, {
			className: "-=in"
		});
	});



	// Alertas

	$.alert = function(custom){
		var option = $.extend({
			timeout: 3000,
			type: "info",
			text: ""
		}, custom);

		var alerts;

		if(!!$(".alert-box").length)
		{
			alerts = $(".alert-box");
		}
		else
		{
			alerts = $("<div>");
			alerts.addClass("alert-box");
			$("body").append(alerts);
		}

		var fade = $("<div>");
		var msg = $("<div>");
		fade.addClass("alert-fade");
		fade.html(msg);
		msg.addClass("alert").addClass(option.type);
		msg.append(option.text);

		fade.animShow(alerts, msg, {
			position: true,
			complete: function(){
				if(option.timeout != 0)
				{
					setTimeout(function(){
						fade.animHide(msg, {
							complete: function(){
								fade.remove();

								if($(".alert-fade").length == 0)
								{
									alerts.remove();
								}
							}
						});
					}, option.timeout);
				}
			}
		});
	}

	// Animações para mostrar e esconder conteúdo

	$.fn.animShow = function(parent, content, custom){
		var option = $.extend({
			position: false, // true para no começo e false para no final
			speed: 0.15, // Tempo da animação
			complete: function(){}
		}, custom);

		var element = this;

		TweenMax.set(element, {
			height: 0
		});

		TweenMax.set(content, {
			autoAlpha: 0,
			scale: 0.85
		});

		if(option.position)
		{
			parent.prepend(element);
		}
		else
		{
			parent.append(element);
		}

		TweenMax.to(element, option.speed, {
			height: element.get(0).scrollHeight,
			onComplete: function(){
				element.css("height", "auto");
				TweenMax.to(content, option.speed, {
					autoAlpha: 1,
					scale: 1,
					onComplete: function(){
						option.complete();
					}
				});
			}
		});
	}

	$.fn.animHide = function(content, custom){
		var option = $.extend({
			speed: 0.15, // Tempo da animação
			complete: function(){}
		}, custom);

		var element = this;

		TweenMax.to(content, option.speed, {
			autoAlpha: 0,
			scale: 0.85,
			onComplete: function(){
				TweenMax.fromTo(element, option.speed, {
					height: element.get(0).scrollHeight
				},{
					height: 0,
					onComplete: function(){
						option.complete();
					}
				});
			}
		});
	}

});