<?php

	function clamp($current, $min, $max) {
	    return max($min, min($max, $current));
	}

	$mes_extenso = array(
        'Jan' => 'Janeiro',
        'Feb' => 'Fevereiro',
        'Mar' => 'Marco',
        'Apr' => 'Abril',
        'May' => 'Maio',
        'Jun' => 'Junho',
        'Jul' => 'Julho',
        'Aug' => 'Agosto',
        'Nov' => 'Novembro',
        'Sep' => 'Setembro',
        'Oct' => 'Outubro',
        'Dec' => 'Dezembro'
    );

	$today = date("j");
	$dayInMonth = date("t");
	$month = date("M");
	$daysFromLastMonth = date("N",mktime(0,0,0, date("m"),date("01-m-Y"),date("Y")));
	$TDFLM = cal_days_in_month(CAL_GREGORIAN, clamp(date("n") - 1,1,12), date("Y")) + 1;

	$events = $this->sql->select(array("*"),"event");

	$calendar = "
	<div><a class='calArrow'><i class=\"fa fa-arrow-left\" aria-hidden=\"true\"></i></a>    <span id='calMonth'>".$mes_extenso[$month]."</span>    <a class='calArrow'><i class=\"fa fa-arrow-right calArrow\" aria-hidden=\"true\"></i></a></div>
		<div class=\"calendar_inner\">
			<div class='calHeader'>Dom</div>
			<div class='calHeader'>Seg</div>
			<div class='calHeader'>Ter</div>
			<div class='calHeader'>Qua</div>
			<div class='calHeader'>Qui</div>
			<div class='calHeader'>Sex</div>
			<div class='calHeader'>Sab</div>";

	for ($i=clamp($daysFromLastMonth,1,6);$i > 0;$i--) {
		if ($i <= 6) {
			$calendar .= "<div class='day off'>".($TDFLM - $i)."</div>";
		}
	}

	for ($i=0; $i < $dayInMonth; $i++) {
		if ($i + 1 < $today) {
			$calendar .= "<div class='day off'>".($i + 1)."</div>";
		}
		else {
			foreach ($events as $key => $value) {
				if ($value->month == $month && $value->day == $i + 1) {
					$calendar .= "<div class='tooltipster day' title='".$value->eventTitle."' style='background-color:".$value->eventColor.";color:".$value->textColor.";'>".($i + 1)."</div>";
					continue 2;
				}
			}
			$calendar .= "<div class='day'>".($i + 1)."</div>";
		}
	}

?>

<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta charset="utf-8">
	<title><?= TITLE; ?></title>
	<!-- Icone -->
	<link rel="shortcut icon" href="<?= URL."img/icon.png"; ?>">
	<!-- Fonte -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i">
	<link rel="stylesheet" href="<?= URL."css/geral/font-awesome"; ?>">
	<!-- Fancybox -->
	<link rel="stylesheet" href="<?= URL."css/geral/jquery.fancybox"; ?>">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="<?= URL."css/home/owl.carousel"; ?>">
	<!-- Quantik -->
	<link rel="stylesheet" type="text/css" href="<?= URL."css/home/style"; ?>">
	<!-- Tooltipster -->
	<link rel="stylesheet" type="text/css" href="<?= URL."css/geral/tooltipster.bundle"; ?>">
	<link rel="stylesheet" type="text/css" href="<?= URL."css/home/tooltipster-sideTip-shadow"; ?>">
</head>
<?php

	if ($this->_front->GetActive() != "Home") {
		?>

			<style type="text/css">
				.header {
					height: 450px;
				}
				.header-action {
					display: none;
				}
			</style>

		<?php
	}

?>
<body class="loading">
	<div class="<?= $this->_front->GetNavbarClasses(); ?>">
		<div class="container">
			<div class="navbar-cell no-padding">
				<img src="<?= URL."img/navbar-logo.png"; ?>" class="navbar-logo">
			</div>

			<div class="navbar-cell navbar-menu"><?= $this->_front->GetMenu("navbar-link on-click"); ?></div>

			<div class="navbar-cell">
				<a href="" class="navbar-vantagens on-click">OBTENHA VANTAGENS</a>
			</div>

			<div class="navbar-cell">
				<a href="" class="navbar-social"><img src="<?= URL."img/icon-instagram.png"; ?>"></a>
				<a href="" class="navbar-social"><img src="<?= URL."img/icon-facebook.png"; ?>"></a>
				<a href="" class="navbar-social"><img src="<?= URL."img/icon-tripadvisor.png"; ?>"></a>
			</div>

			<div class="navbar-cell no-padding">
				<div class="navbar-calendar on-click">
					<div class="navbar-calendar-status">Estamos abertos agora!</div>
					<div class="navbar-calendar-text">Verifique nosso calendário & horários</div>
					<div class="navbar-calendar-arrow"></div>
				</div>
				<div class="calendar" style="display: none">
					
						<?php echo $calendar ?>
					</div>
				</div>
			</div>

			<div class="navbar-cell navbar-responsive">
				<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
					<path class="path-1" d="
					M417.4,224H94.6C77.7,224,64,238.3,64,256c0,17.7,13.7,32,30.6,32h322.8c16.9,0,30.6-14.3,30.6-32   C448,238.3,434.3,224,417.4,224z
					M417.4,96H94.6C77.7,96,64,110.3,64,128c0,17.7,13.7,32,30.6,32h322.8c16.9,0,30.6-14.3,30.6-32   C448,110.3,434.3,96,417.4,96z
					M417.4,352H94.6C77.7,352,64,366.3,64,384c0,17.7,13.7,32,30.6,32h322.8c16.9,0,30.6-14.3,30.6-32   C448,366.3,434.3,352,417.4,352z"/>

					<path class="path-2 hidden" d="M437.5,386.6L306.9,256l130.6-130.6c14.1-14.1,14.1-36.8,0-50.9c-14.1-14.1-36.8-14.1-50.9,0L256,205.1L125.4,74.5  c-14.1-14.1-36.8-14.1-50.9,0c-14.1,14.1-14.1,36.8,0,50.9L205.1,256L74.5,386.6c-14.1,14.1-14.1,36.8,0,50.9  c14.1,14.1,36.8,14.1,50.9,0L256,306.9l130.6,130.6c14.1,14.1,36.8,14.1,50.9,0C451.5,423.4,451.5,400.6,437.5,386.6z"/>
				</svg>
			</div>
		</div>
	</div>

	<div class="responsive hidden">
		<div class="responsive-box">
			<div class="responsive-logo">
				<img src="<?= URL."img/navbar-logo.png"; ?>">
			</div>

			<div class="responsive-menu"><?= $this->_front->GetMenu("responsive-link on-click"); ?></div>

			<div class="responsive-vantagens">
				<a href="" class="on-click">OBTENHA VANTAGENS</a>
			</div>

			<div class="responsive-socials">
				<a href=""><img src="<?= URL."img/icon-instagram.png"; ?>"></a>
				<a href=""><img src="<?= URL."img/icon-facebook.png"; ?>"></a>
				<a href=""><img src="<?= URL."img/icon-tripadvisor.png"; ?>"></a>
			</div>

			<div class="responsive-calendar">
				<div class="responsive-calendar-status">Estamos abertos agora!</div>
				<div class="responsive-calendar-text">Verifique nosso calendário & horários</div>
			</div>
		</div>
	</div>