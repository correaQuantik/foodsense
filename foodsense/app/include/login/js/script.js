$(function(){

	$(document).on("submit", "#login", function(e){
		e.preventDefault();

		var form = $(this);

		$.sendForm(form, {
			beforeSend: function(){
				form.find("button").attr("disabled", true);
				form.find("button > i.fa").removeClass("fa-sign-in").addClass("fa-spin fa-spinner");
			},
			success: function(ans){
				form.find("button > i.fa").removeClass("fa-spin fa-spinner").addClass("fa-sign-in");

				if(!!ans.success)
				{
					$.alert({
						type: "success",
						text: ans.success
					});

					setTimeout(function(){
						location.reload();
					}, 2000);
				}
				else if(!!ans.error)
				{
					$.alert({
						type: "danger",
						text: ans.error
					});

					form.find("button").attr("disabled", false);
				}
			}
		});
	});

});