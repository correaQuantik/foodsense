<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?= TITLE." | ".$this->painel->getTitle(); ?></title>
	<!-- Icon -->
	<link rel="shortcut icon" href="<?= URL."img/icon.png"; ?>">
	<!-- Font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?= URL."css/geral/font-awesome"; ?>">
	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?= URL."css/geral/bootstrap"; ?>">
	<!-- Theme -->
	<link rel="stylesheet" href="<?= URL."css/geral/ace"; ?>">
	<link rel="stylesheet" href="<?= URL."css/geral/ace-skins"; ?>">
	<link rel="stylesheet" href="<?= URL."css/geral/ace-rtl"; ?>">
	<!-- WB -->
	<link rel="stylesheet" href="<?= URL."css/geral/style"; ?>">
	<link rel="stylesheet" href="<?= URL."css/login/style"; ?>">
</head>
<body class="login-layout">
	<div class="main-container">
		<div class="main-content">