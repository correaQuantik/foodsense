$(function(){

	// Variáveis globais

	$.URL = "http://192.168.1.35/_dentista/";

	// Carrega o mask

	$.loadMask = function(){
		$(".mask").each(function(){
			$(this).mask($(this).attr("mask"));
		});
	}

	// Envio de formulário

	$.sendForm = function(form, custom){
		var option = $.extend({
			url: "",
			data: {},
			cache: false,
			contentType: false,
			processData: false,
			dataType: "JSON",
			type: "POST"
		}, custom);

		$("*[disabled='disabled']").addClass("toDisable").attr("disabled", false);
		form.find("input[type='file']").attr("disabled", true);

		if(option.url == "")
		{
			option.url = form.attr("action");
		}

		option.data = new FormData(form[0]);

		$.ajax(option);

		$(".toDisable").attr("disabled", true).removeClass("toDisable");
		form.find("input[type='file']").attr("disabled", false);
	}

	// Envia um Ajax

	$.sendAjax = function(custom){
		var option = $.extend({
			url: "",
			data: {},
			dataType: "JSON",
			type: "POST"
		}, custom);

		if(option.url != "")
		{
			$.ajax(option);
		}
	}

	// Envia um Ajax com arquivos

	$.sendFormFiles = function(files, url, redirect){
		$.alert({
			text: "Começando o envio de arquivos...",
			type: "warning",
			timeout: 0,
		});

		$.sendFormFile(files, 0, url, redirect);
	}

	$.sendFormFile = function(files, cur, url, redirect){
		if(files.length > cur)
		{
			var option = {
				url: url,
				cache: false,
				contentType: false,
				processData: false,
				dataType: "JSON",
				type: "POST",
				success: function(ans){
					if(!!ans.success)
					{
						$.alert({
							text: ans.success,
							type: "success",
							timeout: 0,
						});
					}
					else if(!!ans.warning)
					{
						$.alert({
							text: ans.warning,
							type: "warning",
							timeout: 0,
						});
					}
					else if(!!ans.error)
					{
						$.alert({
							text: ans.error,
							type: "danger",
							timeout: 0,
						});
					}

					$.sendFormFile(files, ++cur, url, redirect);
				}
			};

			option.data = new FormData();
			option.data.append("type", files[cur].type);
			option.data.append("file", files[cur].file);

			$.ajax(option);
		}
		else
		{
			$.alert({
				text: "Arquivos enviados com sucesso!<br><br><b>Redirecionando...</b>",
				type: "success",
				timeout: 0,
			});

			setTimeout(function(){
				location.href = redirect;
			}, 3000);
		}
	}

	// Alertas

	$.alert = function(custom){
		var option = $.extend({
			timeout: 3000,
			type: "info",
			text: ""
		}, custom);

		var alerts;

		if(!!$(".alert-box").length)
		{
			alerts = $(".alert-box");
		}
		else
		{
			alerts = $("<div>");
			alerts.addClass("alert-box");
			$("body").append(alerts);
		}

		var fade = $("<div>");
		var msg = $("<div>");
		fade.addClass("alert-fade");
		fade.html(msg);
		msg.addClass("alert").addClass("alert-"+option.type);
		msg.append(option.text);

		fade.animShow(alerts, msg, {
			position: true,
			complete: function(){
				if(option.timeout != 0)
				{
					setTimeout(function(){
						fade.animHide(msg, {
							complete: function(){
								fade.remove();

								if($(".alert-fade").length == 0)
								{
									alerts.remove();
								}
							}
						});
					}, option.timeout);
				}
			}
		});
	}

	// Animações para mostrar e esconder conteúdo

	$.fn.animShow = function(parent, content, custom){
		var option = $.extend({
			position: false, // true para no começo e false para no final
			speed: 0.15, // Tempo da animação
			complete: function(){}
		}, custom);

		var element = this;

		TweenMax.set(element, {
			height: 0
		});

		TweenMax.set(content, {
			autoAlpha: 0,
			scale: 0.85
		});

		if(option.position)
		{
			parent.prepend(element);
		}
		else
		{
			parent.append(element);
		}

		TweenMax.to(element, option.speed, {
			height: element.get(0).scrollHeight,
			onComplete: function(){
				element.css("height", "auto");
				TweenMax.to(content, option.speed, {
					autoAlpha: 1,
					scale: 1,
					onComplete: function(){
						option.complete();
					}
				});
			}
		});
	}

	$.fn.animHide = function(content, custom){
		var option = $.extend({
			speed: 0.15, // Tempo da animação
			complete: function(){}
		}, custom);

		var element = this;

		TweenMax.to(content, option.speed, {
			autoAlpha: 0,
			scale: 0.85,
			onComplete: function(){
				TweenMax.fromTo(element, option.speed, {
					height: element.get(0).scrollHeight
				},{
					height: 0,
					onComplete: function(){
						option.complete();
					}
				});
			}
		});
	}

	// Campo do tipo arquivo

	$(document).on("change", "input[type='file']", function(){
		var parent = $(this).parents(".input-group");
		parent.find("input[type='text']").val(this.files.length == 0 ? "" : (this.files.length == 1 ? "1 arquivo selecionado" : this.files.length + " arquivos selecionados"));
	});

});