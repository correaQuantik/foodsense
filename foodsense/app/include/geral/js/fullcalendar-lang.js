! function(a) {
	"function" == typeof define && define.amd ? define(["jquery", "moment"], a) : "object" == typeof exports ? module.exports = a(require("jquery"), require("moment")) : a(jQuery, moment)
}(function(a, b) {
	! function() {
		"use strict";
		var a = (b.defineLocale || b.lang).call(b, "pt-br", {
			months: "Janeiro_Fevereiro_Março_Abril_Maio_Junho_Julho_Agosto_Setembro_Outubro_Novembro_Dezembro".split("_"),
			monthsShort: "Jan_Fev_Mar_Abr_Mai_Jun_Jul_Ago_Set_Out_Nov_Dez".split("_"),
			weekdays: "Domingo_Segunda-feira_Terça-feira_Quarta-feira_Quinta-feira_Sexta-feira_Sábado".split("_"),
			weekdaysShort: "Dom_Seg_Ter_Qua_Qui_Sex_Sáb".split("_"),
			weekdaysMin: "Dom_2º_3º_4º_5º_6º_Sáb".split("_"),
			weekdaysParseExact: !0,
			longDateFormat: {
				LT: "HH:mm",
				LTS: "HH:mm:ss",
				L: "DD/MM/YYYY",
				LL: "D [de] MMMM [de] YYYY",
				LLL: "D [de] MMMM [de] YYYY [Ã s] HH:mm",
				LLLL: "dddd, D [de] MMMM [de] YYYY [Ã s] HH:mm"
			},
			calendar: {
				sameDay: "[Hoje Ã s] LT",
				nextDay: "[Amanhã Ã s] LT",
				nextWeek: "dddd [Ã s] LT",
				lastDay: "[Ontem Ã s] LT",
				lastWeek: function() {
					return 0 === this.day() || 6 === this.day() ? "[Último] dddd [Ã s] LT" : "[Última] dddd [Ã s] LT"
				},
				sameElse: "L"
			},
			relativeTime: {
				future: "em %s",
				past: "%s atrás",
				s: "poucos segundos",
				m: "um minuto",
				mm: "%d minutos",
				h: "uma hora",
				hh: "%d horas",
				d: "um dia",
				dd: "%d dias",
				M: "um mês",
				MM: "%d meses",
				y: "um ano",
				yy: "%d anos"
			},
			ordinalParse: /\d{1,2}Âº/,
			ordinal: "%dÂº"
		});
		return a
	}(), a.fullCalendar.datepickerLang("pt-br", "pt-BR", {
		closeText: "Fechar",
		prevText: "&#x3C;Anterior",
		nextText: "Próximo&#x3E;",
		currentText: "Hoje",
		monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
		monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
		dayNames: ["Domingo", "Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "Sábado"],
		dayNamesShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"],
		dayNamesMin: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"],
		weekHeader: "Sm",
		dateFormat: "dd/mm/yy",
		firstDay: 0,
		isRTL: !1,
		showMonthAfterYear: !1,
		yearSuffix: ""
	}), a.fullCalendar.lang("pt-br", {
		buttonText: {
			month: "Mês",
			week: "Semana",
			day: "Dia",
			list: "Compromissos"
		},
		allDayText: "dia inteiro",
		eventLimitText: function(a) {
			return "mais +" + a
		}
	})
});