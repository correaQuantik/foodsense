$(function(){

	// Hover

	$(document).on("mouseenter", ".hover, .hover *", function(){
		var parent = $(this).parents(".hover-all");
		var elem = parent.length > 0 ? [parent, parent.find("*")] : [this, $(this).find("*")];
		TweenMax.to(elem, 0.2, {
			className: "+=on"
		});
	}).on("mouseleave", ".hover, .hover *", function(){
		var parent = $(this).parents(".hover-all");
		var elem = parent.length > 0 ? [parent, parent.find("*")] : [this, $(this).find("*")];
		TweenMax.to(elem, 0.2, {
			className: "-=on"
		});
	});

	// Focus

	$(document).on("focusin", ".focus, .focus *", function(){
		var parent = $(this).parents(".focus-all");
		var elem = parent.length > 0 ? [parent, parent.find("*")] : [this, $(this).find("*")];
		TweenMax.to(elem, 0.2, {
			className: "+=in"
		});
	}).on("focusout", ".focus, .focus *", function(){
		var parent = $(this).parents(".focus-all");
		var elem = parent.length > 0 ? [parent, parent.find("*")] : [this, $(this).find("*")];
		TweenMax.to(elem, 0.2, {
			className: "-=in"
		});
	});

});